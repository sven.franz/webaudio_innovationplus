from enum import Enum
import markdown
import os
import io
import numpy as np
from matplotlib.figure import Figure as matplotlibFigure

class PluginType(Enum):
    persistent  = 1
    loopthrough = 2
    insert      = 3
    
class Domain(Enum):
    Time  = 1
    Frequency = 2

class TimePoint():
    def __init__(self, yValue, channel, color) -> None:
        self.value = yValue
        self.channel = channel
        self.color = color

class FrequencyPoint():
    def __init__(self, frequency, channel, color) -> None:
        self.value = frequency
        self.channel = channel
        self.color = color

class Annotation:
    def __init__(self, value, start, end) -> None:
        self.value = value
        self.start = start
        self.end = end

class Figure(matplotlibFigure):
    def __init__(self, size = (10, 7.5)) -> None:
        matplotlibFigure.__init__(self, figsize = size)

    def toBytes(self, format = "svg"):
        buffer = io.BytesIO()
        self.savefig(buffer, format = format, transparent = True)
        buffer.seek(0)
        return buffer.read()

class AudioPlugin:
    name = None
    isModal = False
    type = PluginType.loopthrough
    domain = Domain.Time
    blockLen = 1024
    fftSize = 1024
    timeDomainWithoutOverlap = False
    blockTime = {"start": 0, "end": 0}

    def bytesToArray(self, data):
        if type(data) is str:
            data = [data]
        array = None
        for channel in range(len(data)):
            values = np.fromstring(data[channel], np.float32)
            if array is None:
                array = np.empty([0, values.shape[0]])
            values.shape = [1, values.shape[0]]
            array = np.vstack([array, values])
        return array

    def __init__(self, *args, **kwargs) -> None:
        self.temp = {}
        self.params = {}
        if len(args):
            self.temp = args[0].copy()
            self.params = args[0]
        for attr in dir(AudioPlugin):
            if not callable(getattr(AudioPlugin, attr)) and not attr.startswith("_"):
                if hasattr(self, attr):
                    self.params.update({attr: getattr(self, attr)})
                else:
                    self.params.update({attr: getattr(AudioPlugin, attr)})
        self.controls = []

    def appendOldParamsAfterInit(self):
        self.params.update(self.temp)
        delattr(self, "temp")

    def updateParams(self):
        pass

    def getPluginInfo(self):
        if os.path.isfile(os.path.join("Plugins", self.__class__.__name__ + ".md")):
            infoText = ""
            with open(os.path.join("Plugins", self.__class__.__name__ + ".md"), 'r') as file:
               infoText = file.read()
            return markdown.markdown(infoText)
        elif hasattr(self, "info"):
            return markdown.markdown(self.info())
        else:
            return False

# implement for Server-Side!
#    def process(self, data):
#        return data