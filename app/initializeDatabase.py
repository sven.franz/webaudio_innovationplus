from glob import glob
import os
import re
import soundfile as sf

from app import db
from app.models.AudioFiles import AudioFiles
from app.models.Folders import Folders
from app.models.Tags import Tags
from app.models.Users import Users
import getpass

def initializeDatabase():
    print ("\nInitializing Database")
    print ("=====================")
    print ("Create Admin Account:")
    while True:
        email = input(" eMail (used as login): ")
        if email and re.search(r'[\w.-]+@[\w.-]+.\w+', email): break
    while True:
        lastname = input(" Name: ")
        if lastname: break
    while True:
        password = getpass.getpass(" Password: ")
        passwordCheck = getpass.getpass(" Password-Check: ")
        if password and password == passwordCheck: break

    print ("\nIMPORTANT: edit SMTP_CONFIG.ini for sending confirmation mails!!!\n")
    print ("\nPlease wait...")

    filenames = glob(os.path.join("flask_session", "*"), recursive=True)
    for filename in filenames:
        os.remove(filename)
    filenames = glob(os.path.join("AudioFiles", "*.audiofile"), recursive=True)
    for filename in filenames:
        os.remove(filename)

    db.drop_all()
    db.create_all()
    user = Users(name = lastname, email = email, password = password, isDeveloper = True, isAdmin = True)
    db.session.add(user)    
    user.getPassphrase(password)
    
    t1 = Tags(name = "Sprache", color = "255, 0, 0")
    db.session.add(t1)
    db.session.add(Tags(name = "Flüstern", color = "255, 0, 100", parent = t1))
    db.session.add(Tags(name = "Normal", color = "255, 100, 0", parent = t1))
    db.session.add(Tags(name = "Brüllen", color = "255, 100, 100", parent = t1))
    db.session.add(Tags(name = "Schreien", color = "255, 200, 200", parent = t1))
    t1 = Tags(name = "Straßenlärm", color = "0, 255, 0")
    db.session.add(t1)
    db.session.add(Tags(name = "PKW", color = "0, 255, 100", parent = t1))
    db.session.add(Tags(name = "Motorrad", color = "100, 255, 100", parent = t1))
    t2 = Tags(name = "Martinshorn", color = "200, 255, 200", parent = t1)
    db.session.add(t2)
    db.session.add(Tags(name = "Polizei", color = "0, 100, 200", parent = t2))
    db.session.add(Tags(name = "Feuerwehr", color = "200, 100 , 200", parent = t2))
    db.session.add(Tags(name = "Krankenwagen", color = "200, 200, 200", parent = t2))

    folder1 = Folders(name = "SpeechFiles")
    db.session.add(Folders(name = "low", parent = folder1))
    db.session.add(Folders(name = "mid", parent = folder1))
    db.session.add(Folders(name = "high", parent = folder1))
    db.session.add(folder1)

    folder1 = Folders(name = "NoiseFiles")
    db.session.add(Folders(name = "low", parent = folder1))
    db.session.add(Folders(name = "mid", parent = folder1))
    db.session.add(Folders(name = "high", parent = folder1))
    db.session.add(folder1)
    db.session.commit()

    filenames = glob(os.path.join("AudioFiles", "*.wav"), recursive=True)
    for filename in filenames:
        db.session.add(AudioFiles(name = filename, user = user, password = password))

    db.session.commit()
    

    # x = Tag.getTagList()
    # print(x)


        
        
        