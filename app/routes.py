import hashlib
import io
import os
import secrets
import sys
import json
import copy
import threading
import wave
import markdown
import importlib
import inspect
import zipfile
import smtplib
import base64
import traceback
import mimetypes
from urllib.parse import urlparse
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import numpy as np
import numpy.fft as nf
from glob import glob
from datetime import datetime
from sqlalchemy import func
from Crypto.Cipher import AES
from app.AudioPlugin import AudioPlugin
from matplotlib import cm, pyplot
from app import app, auth, socketio, db
from flask import abort, Response, flash, stream_with_context, request, render_template, jsonify, session, redirect, url_for, send_file
from flask_socketio import emit
from flask_login import login_required, login_user, logout_user
from app.AudioPlugin import Annotation, Domain, FrequencyPoint, PluginType, TimePoint
from app.config import Config
from app.helper import FeatureFile
from app.models.Annotations import Annotations, AnnotationsHistory
from app.models.AudioFiles import AudioFileParts, AudioFiles
from app.models.Folders import Folders
from app.models.Tags import Tags
from app.models.Users import Usergroups, Users, UsergroupsPermissions
from expiringdict import ExpiringDict
from flask_wtf import FlaskForm
from wtforms import StringField,PasswordField,SubmitField
from wtforms.validators import DataRequired,Email,EqualTo

PluginList = ExpiringDict(max_len = 1000, max_age_seconds = 60 * 60) # 1 hour
Windows = {"Domain.Time": {}, "Domain.Frequency": {}}

class RegistrationForm(FlaskForm):
    name = StringField('Name', validators =[DataRequired()])
    email = StringField('Email', validators=[DataRequired(),Email()])
    password1 = PasswordField('Password', validators = [DataRequired()])
    password2 = PasswordField('Confirm Password', validators = [DataRequired(),EqualTo('password1')])
    submit = SubmitField('Register')

class LoginForm(FlaskForm):
    email = StringField('Email',validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Login')

def executeMethodInSanbox(obj, method, *args, **kwargs):
    class KillableThread(threading.Thread):
        def __init__(self, obj, method, *args, **kwargs):
            self.obj = obj
            self.method = method
            self.args = args
            self.killed = False
            self.returnValue = {}
            threading.Thread.__init__(self)

        def start(self):
            self.__run_backup = self.run
            self.run = self.__run	
            threading.Thread.start(self)

        def run(self):
            try:
                methodName = self.method.__name__
                globalsParameter = {'__builtins__' : {'np': np}}
                localsParameter = {'args': args, "returnValue": {}}
                if hasattr(self.method, "__init__")  and not isinstance(self.obj, AudioPlugin):
                    self.obj = AudioPlugin()
                    self.obj.__class__ = self.method
                    methodName = "obj.__init__"
                    localsParameter.update({"obj" : self.obj})
                    self.method = self.obj.__init__
                ProcessCode = inspect.getsource(self.method).strip()
                exec(ProcessCode + "\nreturnValue = " + methodName + "(*args)", globalsParameter, localsParameter)
                if "obj" in localsParameter.keys():
                    self.returnValue = localsParameter["obj"]
                else:
                    self.returnValue = localsParameter["returnValue"]
            except Exception as ex:
                raise ex            
            finally:
                pass

        def __run(self):
            sys.settrace(self.globaltrace)
            self.__run_backup()
            self.run = self.__run_backup

        def globaltrace(self, frame, event, arg):
            if event == 'call':
                return self.localtrace
            else:
                return None

        def localtrace(self, frame, event, arg):
            if self.killed:
                if event == 'line':
                    raise SystemExit()
            return self.localtrace

        def kill(self):
            self.killed = True

    try:
        if (type(obj) is dict and "isTemp" in obj.keys() and obj["isTemp"]) or (hasattr(obj, "isTemp") and obj.isTemp):
            if hasattr(method, "__self__"):
                args = (obj, ) + args
            sandbox = KillableThread(obj, method, *args)
            sandbox.start()
            sandbox.join(timeout = 2)
            if sandbox.is_alive(): 
                sandbox.kill()
                sandbox.join()
                raise TimeoutError
            else:
                return sandbox.returnValue
        else:
            return method(*args)
    except Exception as ex:
        raise ex

"""
@auth.verify_password
def verify_password(login, password):
    user = Users.query.filter(Users.name.is_(login), Users.deleted.is_(None)).first()
    if user is not None and user.check_password(password):
        try:
            session.update({"user": {"id": user.id, "isAdmin": user.isAdmin, "isDeveloper": user.isDeveloper, "PluginFolder": os.path.join("Plugins", "users", hashlib.sha224(user.id.encode('utf-8')).hexdigest())}})
            return True
        except:
            if session and "user" in session.keys():
                session.pop("user", None)
            pass
    return False
"""
@auth.user_loader
def load_user(user_id):
    if "user" in session.keys() and "serverSecret" in session["user"] and session["user"]["serverSecret"] == hashlib.sha224(Config.SECRET_KEY[:32].encode('utf-8')).hexdigest():
        return Users.query.filter(Users.id.is_(user_id), Users.deleted.is_(None)).first()
    else:
        return None

@app.route('/register', methods = ['POST','GET'])
def register():
    aes = AES.new(Config.SECRET_KEY[:32].encode(), AES.MODE_CBC, bytes(Config.SECRET_KEY, 'utf-8')[:16])
    if request.method == "GET" and len(list(request.args.keys())) == 1:
        try:
            LoginInfo = json.loads(aes.decrypt(base64.b32decode(list(request.args.keys())[0])).decode("utf-8").strip())
            if "name" in LoginInfo.keys() and "email" in LoginInfo.keys() and "password" in LoginInfo.keys():
                user = Users.query.filter(Users.email.is_(LoginInfo["email"]), Users.deleted.is_(None)).first()
                if not user:
                    password = LoginInfo["password"]
                    if type(password) is str:
                        password = str.encode(password)
                    user = Users(name = LoginInfo["name"], email = LoginInfo["email"], password = password, isDeveloper = False, isAdmin = False)
                    for item in Config.AUTOMATIC_ACTIVATIONS.keys():
                        if LoginInfo["email"].endswith(item):
                            group = Usergroups.query.filter(Usergroups.name, Usergroups.deleted.is_(None)).first()
                            if not group:
                                group = Usergroups(name = Config.AUTOMATIC_ACTIVATIONS[item])
                                db.session.add(group)
                                db.session.commit()
                            user.Usergroups_id = group.id
                    db.session.add(user)
                    db.session.commit()
                    msg = "User registrated"
                    if not user.Usergroups_id:
                       msg += "... Please wait for activation"
                    flash(msg + "!")
                    return redirect(url_for('login'))
                else:
                    msg = "User with email '" + LoginInfo["email"] + "' already successfully registered"
                    if user.Usergroups_id is None:
                        msg += " but needs to wait for activation"
                    flash(msg + "!")
                    return redirect(url_for('login'))
            else:
                flash('Confirmation Link outdated... Please register again!')
                return redirect(url_for('login'))
        except Exception as ex:
            flash('Confirmation Link outdated... Please register again!')
            return redirect(url_for('login'))
    else:
        form = RegistrationForm()
        if form.validate_on_submit():
            if Users.query.filter(Users.email.is_(form.email.data), Users.deleted.is_(None)).first():
                flash("User with email '" + form.email.data + "' already exixts!")
            else:
                s = smtplib.SMTP(host = Config.SMTP_HOST, port = int(Config.SMTP_PORT))
                s.starttls()
                s.login(Config.SMTP_LOGIN, Config.SMTP_PASSWORD)
                msg = MIMEMultipart()
                msg['From']= Config.SMTP_EMAIL
                msg['To'] = form.email.data
                msg['Subject'] = "WebAudio Registration Confirmation"
                url = urlparse(request.base_url)
                password = form.password1.data
                if type(password) is str:
                    password = str.encode(password)
                LoginInfo = json.dumps({"name": form.name.data, "email": form.email.data, "password": password, "salt": secrets.token_urlsafe(32)})
                LoginInfo = base64.b32encode(aes.encrypt(LoginInfo + ' ' * (16 - len(LoginInfo) % 16))).decode('utf-8')
                message = "Dear " + (form.name.data).strip() + "\nplease visit following URL to confirm your registration:\n"
                message += url.scheme + "://" + url.hostname + ":" + str(url.port) + url.path + "?" + LoginInfo
                msg.attach(MIMEText(message, 'plain'))
                s.send_message(msg)
                s.quit()
                flash("Confirmation Link sent to '" + form.email.data +"'... Please confirm registration!")
                return redirect(url_for('login'))
        return render_template('registration.html', form=form)

@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = Users.query.filter(Users.email.is_(form.email.data), Users.deleted.is_(None), db.or_(Users.Usergroups_id.isnot(None), Users.isAdmin.is_(True))).first()
        password = form.password.data
        if type(password) is bytes:
            password = password.decode('utf-8')
        if user is not None and user.check_password(password):
            login_user(user)
            session.pop("user", None)
            session.update({"user": {"id": user.id, "isAdmin": user.isAdmin, "isDeveloper": user.isDeveloper, "Usergroup": user.Usergroups_id, "PluginFolder": os.path.join("Plugins", "users", hashlib.sha224(user.id.encode('utf-8')).hexdigest())}})
            session["user"]["iv"] = secrets.token_bytes(16)
            session["user"]["serverSecret"] = hashlib.sha224(Config.SECRET_KEY[:32].encode('utf-8')).hexdigest()
            aes = AES.new(Config.SECRET_KEY[:32].encode('utf-8'), AES.MODE_CBC, session["user"]["iv"])
            session["user"]["cypherPwd"] = aes.encrypt((password + ' ' * (16 - len(password) % 16)).encode('utf-8'))
            next = request.args.get("next")
            return redirect(next or url_for('index'))
        flash('Invalid email address or Password.')
    return render_template('login.html', form=form)

@app.route("/forbidden",methods=['GET', 'POST'])
@login_required
def protected():
    return redirect(url_for('forbidden.html'))

@app.route("/logout")
def logout():
    if session and "user" in session.keys():
        session.pop("user", None)
    logout_user()
    return redirect(url_for('login'))
    
@app.route('/')
@login_required
def index():
    if "Plugins" in session.keys():
        del session["Plugins"]
    return render_template('index.html')

@app.route('/config')
@login_required
def config():
    return render_template('admin.html')

@app.route('/admin')
@login_required
def admin():
    if session["user"]["isAdmin"]:
        return render_template('admin.html')
    else:
        abort(404)

@app.route('/docs')
@app.route('/docs/<file_name>')
@login_required
def docs(file_name = None):
    if file_name and os.path.isfile(os.path.join("docs", file_name)):
        return send_file(os.path.join("..", "docs", file_name), mimetype = mimetypes.MimeTypes().guess_type(os.path.join("docs", file_name))[0])
    else:
        title = "Plugins"
        filename = os.path.join("docs", title.lower() + ".md")
        content = ""
        with open(filename) as file:
            content = file.read()
        content = "[TOC]\n" + content
        return render_template('docs/index.html', title = title, content = markdown.markdown(content, extensions=['fenced_code', 'codehilite', 'toc']))

@app.route('/getUserSettings', methods=['POST'])
@login_required
def getUserSettings():
    user = Users.query.filter(Users.id.is_(session["user"]["id"]), Users.deleted.is_(None)).first().asDict()
    if (user):
        user.pop("id")
        user.pop("password")
        user.pop("privatekey")
        user.pop("publickey")

        permission = None
        if "file" in session.keys():
            file = db.session.query(AudioFiles).join(AudioFileParts, AudioFiles.id == AudioFileParts.audioFile_id).filter(AudioFileParts.id.is_(session["file"])).first()
            if file:
                permission = UsergroupsPermissions.query.filter(UsergroupsPermissions.usergroup_id.is_(user["Usergroups_id"]), UsergroupsPermissions.folder_id.is_(file.folder_id)).first()
        for column in UsergroupsPermissions.__table__.columns:
            if str(column.type) == "BOOLEAN":
                user[column.name] = user["isAdmin"] or (permission and getattr(permission, column.name))
        user["adminMails"] = []
        for admin in Users.query.filter(Users.isAdmin.is_(True), Users.deleted.is_(None)).all():
            user["adminMails"].append(admin.email)
        return jsonify(json.dumps(user, default=str))
    return 'bad request!', 400

@app.route('/loadPluginList')
@login_required
def loadPluginList():
    if "Plugins" in session.keys():
        del session["Plugins"]
    sortList = []
    plugins = []
    permission = None
    file = None
    if "file" in session.keys():
        file = db.session.query(AudioFiles).join(AudioFileParts, AudioFiles.id == AudioFileParts.audioFile_id).filter(AudioFileParts.id.is_(session["file"])).first()
    if file:
        permission = UsergroupsPermissions.query.filter(UsergroupsPermissions.usergroup_id.is_(session["user"]["Usergroup"]), UsergroupsPermissions.folder_id.is_(file.folder_id)).first()
    for file in glob(os.path.join("Plugins", "*.py")) + glob(os.path.join(session["user"]["PluginFolder"], "*.py")):
        path, pluginName = os.path.split(file)
        pluginName = pluginName.replace(".py", "")
        spec = importlib.util.spec_from_file_location(pluginName, file)
        module = importlib.util.module_from_spec(spec)
        if module.__name__ == spec.name:
            spec.loader.exec_module(module)
            if hasattr(module, pluginName) and AudioPlugin in inspect.getmro(getattr(module, pluginName)) and not (path.startswith(os.path.join("Plugins", "users")) and session["user"]["isDeveloper"] == False):
                data = {"id": module.__name__, "name": getattr(module, module.__name__).name, "modal": getattr(module, module.__name__).isModal, "type": getattr(module, module.__name__).type, "isTemp": path.startswith(os.path.join("Plugins", "users"))}
                if not data["name"]:
                    data["name"] = module.__name__
                allowModify = session["user"]["isAdmin"]
                if permission:
                    allowModify = allowModify or permission.allowModify
                #if module.type == PluginType.loopthrough or allowModify:
                if not pluginName.lower() in sortList:
                    plugins.append(data)
                    sortList.append(pluginName.lower())
                else:
                    plugins[sortList.index(pluginName.lower())] = data
    plugins = [plugins[i] for i in [i[0] for i in sorted(enumerate(sortList), key=lambda x:x[1])]]
    return jsonify(json.dumps(plugins, default=str))

@app.route('/importPlugin', methods=['POST'])
@login_required
def importPlugin():
    if hasattr(request, "files") and "plugin" in request.files.to_dict().keys():
        file = request.files['plugin']
        if file.content_type == 'text/x-python':
            os.makedirs(session["user"]["PluginFolder"], exist_ok=True)
            file.save(os.path.join(session["user"]["PluginFolder"], file.name))
            return {"status": "success"}, 200
        elif file.content_type == 'application/zip':
            with zipfile.ZipFile(file, 'r') as zip_ref:
                pluginName = None
                for zippedFile in zip_ref.infolist():
                    if zippedFile.filename.lower().endswith(".py"):
                        pluginName = zippedFile.filename.lower()[: -3]
                for zippedFile in zip_ref.infolist():
                    if not zippedFile.filename.lower() in [pluginName + ".py", pluginName + ".md", pluginName + ".js"]:
                        return 'bad request!', 400
                zip_ref.extractall(os.path.join(session["user"]["PluginFolder"]))         
            return {"status": "success"}, 200   
    return 'bad request!', 400

@app.route('/loadPlugin', methods=['POST'])
@login_required
def loadPlugin():
    global PluginList
    selectedPlugin = ""
    data = []
    if request.method == "POST" and request.is_json and len(request.data) and "id" in request.get_json():
        data = request.get_json()
        selectedPlugin = data["id"]
        del data["id"]
    if not "Plugins" in session.keys():
        session["Plugins"] = []
    PluginList = {plug: PluginList[plug] for plug in PluginList if len(PluginList[plug]["user_ids"])}
    filename = os.path.join(session["user"]["PluginFolder"], selectedPlugin + ".py") if os.path.isfile(os.path.join(session["user"]["PluginFolder"], selectedPlugin + ".py")) else os.path.join("Plugins", selectedPlugin + ".py")
    if not (filename.startswith(os.path.join("Plugins", "users")) and session["user"]["isDeveloper"] == False):
        if  (not selectedPlugin in PluginList.keys() or PluginList[selectedPlugin]["isTemp"]) and os.path.isfile(filename):
            spec = importlib.util.spec_from_file_location(selectedPlugin, filename)
            module = importlib.util.module_from_spec(spec)        
            spec.loader.exec_module(module)
            sys.modules[selectedPlugin] = module
            PluginList[selectedPlugin] = {"user_ids" : [], "isTemp" : filename.startswith(os.path.join("Plugins", "users"))}
        if selectedPlugin in PluginList.keys():
            try:
                plugin = executeMethodInSanbox(PluginList[selectedPlugin], getattr(sys.modules[selectedPlugin], selectedPlugin), data)
            except Exception as ex:
                abort(408, description = "Plugin Initialization failed: " + repr(ex))
            plugin.isTemp = PluginList[selectedPlugin]["isTemp"]
            AudioPlugin.appendOldParamsAfterInit(plugin)
            data = {"controls": None, "params": None, "script": None, "isServerSide": False}
            plugin.isTemp = PluginList[selectedPlugin]["isTemp"]
            try:
                executeMethodInSanbox(plugin, plugin.updateParams)
            except Exception as ex:
                abort(408, description = "Plugin Method updateParams() failed: " + repr(ex))
            data["params"] = plugin.params
            data["controls"] = plugin.controls
            data["info"] = AudioPlugin.getPluginInfo(plugin)
            if hasattr(plugin, "process"):
                data["isServerSide"] = True
            session["Plugins"].append({"object": plugin, "params": data["params"], "isTemp": PluginList[selectedPlugin]["isTemp"]})
            filename = os.path.join(session["user"]["PluginFolder"], plugin.__class__.__name__ + ".js") if os.path.isfile(os.path.join(session["user"]["PluginFolder"], plugin.__class__.__name__ + ".js")) else os.path.join("Plugins", plugin.__class__.__name__ + ".js")
            if filename and os.path.isfile(filename):
                with open(filename, 'r') as file:
                    data["script"] = file.read()
            if not session["user"]["id"] in PluginList[selectedPlugin]["user_ids"]:
                PluginList[selectedPlugin]["user_ids"].append(session["user"]["id"])
    return jsonify(json.dumps(data, default=str))

@app.route('/unloadPlugin', methods=['POST'])
@login_required
def unloadPlugin():
    if "Plugins" in session.keys():
        selectedPlugin = ""
        if request.method == "POST" and request.is_json and len(request.data) and "id" in request.get_json():
            selectedPlugin = request.get_json()["id"]
        idx = [idx for idx in range(len(session["Plugins"])) if session["Plugins"][idx]["object"].__class__.__name__ == selectedPlugin]
        if len(idx):
            del session["Plugins"][idx[0]]
            if session["user"]["id"] in PluginList[selectedPlugin]["user_ids"]:
                PluginList[selectedPlugin]["user_ids"].remove(session["user"]["id"])
            if len(PluginList[selectedPlugin]["user_ids"]) == 0:
                del PluginList[selectedPlugin]
            return jsonify(True)
    return jsonify(False)

@app.route('/setCurrentFile', methods=['POST'])
@login_required
def setCurrentFile():
    if request.is_json and "file" in request.json.keys():
        session["file"] = request.json["file"]
    return jsonify(True)

@app.route('/download', methods=['GET'])
@login_required
def download():
    if "Plugins" in session.keys():
        del session["Plugins"]
    myAudiofile = AudioFileParts.query.filter(AudioFileParts.id.is_(session["file"])).first()
    fileData = None
    if myAudiofile:
        file = db.session.query(AudioFiles).join(AudioFileParts, AudioFiles.id == AudioFileParts.audioFile_id).filter(AudioFileParts.id.is_(myAudiofile.id)).first()
        permission = UsergroupsPermissions.query.filter(UsergroupsPermissions.usergroup_id.is_(session["user"]["Usergroup"]), UsergroupsPermissions.folder_id.is_(file.folder_id)).first()
        if session["user"]["isAdmin"] or (permission and permission.allowRead):
            #response = make_response(stream_with_context(generate(fileData), mimetype="application/octet-stream")
            password = AES.new(Config.SECRET_KEY[:32].encode('utf-8'), AES.MODE_CBC, session["user"]["iv"]).decrypt(session["user"]["cypherPwd"]).strip()
            if type(password) is str:
                password = str.encode(password)
            response = Response(stream_with_context(myAudiofile.getData(session["user"], password)))
            response.headers.set('Content-Type', 'audio/mpeg')
            response.headers.set('Cache-Control', 'no-cache')
            response.headers.set('Content-Length', myAudiofile.fileSize)
            response.headers.set('Content-Disposition', 'attachment', filename=myAudiofile.AudioFile.name + " [" + str(myAudiofile.part + 1) + "]")
            return response
    return jsonify(fileData)

@app.route('/upload', methods=['GET', 'POST'])
@login_required
def upload():
    file = None
    FilePreferences = None
    if (hasattr(request, "files") and "audiofile" in request.files.to_dict().keys()) or ("FilePreferences" in request.form.keys()):
        if hasattr(request, "files") and "audiofile" in request.files.to_dict().keys():
            file = request.files['audiofile']
        if "FilePreferences" in request.form.keys():
            FilePreferences = json.loads(request.form["FilePreferences"])
        password = AES.new(Config.SECRET_KEY[:32].encode(), AES.MODE_CBC, session["user"]["iv"]).decrypt(session["user"]["cypherPwd"]).strip()
        if type(password) is str:
            password = str.encode(password)
        if "file" in session.keys() and session["file"] and FilePreferences and FilePreferences["id"] and FilePreferences["id"] == session["file"]:
            tmpFile = db.session.query(AudioFiles).join(AudioFileParts, AudioFiles.id == AudioFileParts.audioFile_id).filter(AudioFileParts.id.is_(session["file"])).first()
            if (tmpFile):
                permission = UsergroupsPermissions.query.filter(UsergroupsPermissions.usergroup_id.is_(session["user"]["Usergroup"]), UsergroupsPermissions.folder_id.is_(tmpFile.folder_id)).first()
                if session["user"]["isAdmin"] or (permission and permission.allowModify):
                    AudioFilePart = AudioFileParts.query.filter(AudioFileParts.id.is_(session["file"])).first()
                    if AudioFilePart.isPublic != FilePreferences["isPublic"] and (session["user"]["isAdmin"] or (tmpFile.owner_id == session["user"]["id"]) or (permission and permission.allowChangeProtection)):
                        if not file:
                            file = AudioFilePart.getData(session["user"], password)
                        AudioFilePart.isPublic = FilePreferences["isPublic"]
                        if AudioFilePart.AudioFile.name != FilePreferences["filename"]:
                            AudioFilePart.AudioFile.name = FilePreferences["filename"]
                    if file:                
                        AudioFilePart.setData(file, session["user"], password)
                    db.session.commit()
        elif file:
            filename = file.filename
            if not filename: 
                filename = "File " + datetime.now().strftime("%Y.%m.%d %H:%M:%S")
            if FilePreferences and "filename" in FilePreferences.keys() and FilePreferences["filename"]:
                filename = FilePreferences["filename"]
            newFile = AudioFiles(data = file, name = filename, user = session["user"], password = password)
            db.session.add(newFile)
            db.session.commit()
            db.session.flush()
            db.session.refresh(newFile)
            session["file"] = newFile.audioFileParts[0].id
        else:
             return 'bad request!', 400
        return {"status": "success", "FileId": session["file"]}, 200
    else:
        return render_template('upload.html')

@app.route('/completeTagList')
@login_required
def completeTagList():
    tags = []
    for tag in Tags.query.filter(Tags.deleted.is_(None), Tags.manual.is_(False)).order_by(Tags.name).all():
        tags.append(tag.asDict())
    return jsonify(tags)

@app.route('/hierarchicalTagList', methods=['GET', 'POST'])
@login_required
def hierarchicalTagList():
    def getTagsFromParent(id):
        tags = []
        for tag in Tags.query.filter(Tags.parent_id.is_(id), Tags.deleted.is_(None), Tags.manual.is_(False)).all():
            tag.value = tag.id
            tag.children = getTagsFromParent(tag.id)
            tags.append(tag.asDict())
        return tags
    return jsonify(getTagsFromParent(None))

@app.route('/storeAdministrationData', methods=['POST'])
@login_required
def storeAdministrationData():
    if session["user"]["isAdmin"] and request.method == "POST" and request.is_json:
        try:
            if list(request.json.keys())[0] == "Usergroups":
                baseClasses = [Usergroups, Users, UsergroupsPermissions]
            elif list(request.json.keys())[0] == "Folders":
                baseClasses = [Folders, AudioFiles]
            else:
                baseClasses = [eval(list(request.json.keys())[0])]
            data = request.json[list(request.json.keys())[0]]
            for row in data:
                currentClass = baseClasses[0]
                if "level" in row.keys():
                    currentClass = baseClasses[int(row["level"])]
                if "deleted" in row.keys() and row["deleted"]:
                    rowToDelete = currentClass.query.filter_by(id=row["id"]).first()
                    if rowToDelete:
                        row["deleted"] = datetime.now()
                else:
                    del row["deleted"]
                if row["parent_id"] == "temp":
                    row["parent_id"] = None
                if currentClass is Usergroups:
                    del row["parent_id"]
                elif currentClass is Users:
                    row["Usergroups_id"] = row["parent_id"]
                    del row["parent_id"]
                elif currentClass is AudioFiles:
                    row["folder_id"] = row["parent_id"]
                    del row["parent_id"]
                if hasattr(currentClass, "readonly"):
                    for key in currentClass.readonly:
                        if key.lower() in [x.lower() for x in row.keys()]:
                            del row[list(row.keys())[[x.lower() for x in row.keys()].index(key.lower())]]
                for key in list(row.keys()):
                    keys = list(currentClass.__dict__)
                    if key.lower() in [x.lower() for x in keys]:
                        idx = [x.lower() for x in keys].index(key.lower())
                        if str(getattr(currentClass, keys[idx]).type) == "BOOLEAN":
                            row[key] = (row[key].lower() == "true")
                        if key != keys[idx]:
                            row[keys[idx]] = row[key]
                            del row[key]
                    elif key == "PERMISSIONS":
                        permissions = json.loads(str(row[key]))
                        if type(permissions) is list or type(permissions) is dict:
                            for permission in permissions:
                                dataToStore = {key: permission[key] for key in [column.name for column in UsergroupsPermissions.__table__.columns._all_columns]}
                                db.session.merge(UsergroupsPermissions(**dataToStore))
                        del row[key]
                    else:
                        del row[key]
                if row["id"] != "temp":
                    db.session.merge(currentClass(**row))
            db.session.commit()
            db.session.flush()
            return jsonify(json.dumps({"succsess": True}, default=str))
        except Exception as e:
            print(traceback.format_exc())
    return jsonify(json.dumps({"succsess": False}, default=str))

@app.route('/getFilePreferences', methods=['POST'])
@login_required
def getFilePreferences():
    filePreferences = {}
    if request.method == "POST" and request.is_json and "fileId" in request.get_json() and request.get_json()["fileId"] == session["file"]:
        myAudiofile = AudioFileParts.query.filter(AudioFileParts.id.is_(request.get_json()["fileId"])).first()
        filePreferences = myAudiofile.asDict(session["user"]["id"])
        filePreferences.pop('audioFile_id')
    return jsonify(json.dumps(filePreferences, default=str))
    
@app.route('/getAnnotations', methods=['POST'])
@login_required
def getAnnotations():
    annotatioList = []
    if request.method == "POST" and request.is_json and len(request.data):
        loop = True
        id = None
        if request.method == "POST" and request.is_json and len(request.data) and "id" in request.get_json():
            id = request.get_json()["id"]
        while loop and "file" in session.keys() and session["file"]:
            if id:
                query = Annotations.query.join(AudioFileParts).order_by(Annotations.id).filter(Annotations.id > id, AudioFileParts.id.is_(session["file"]), Annotations.deleted.is_(None))
            else:
                query = Annotations.query.join(AudioFileParts).order_by(Annotations.id).filter(AudioFileParts.id.is_(session["file"]), Annotations.deleted.is_(None))
            data = query.first()
            if data and len(annotatioList) < 100:
                if len(data.history) and str(data.history[-1].action and data.history[-1].action) != "D":
                    annotatioList.append(data.asDict())
                id = data.id
            else:
                loop = False
    return jsonify(json.dumps(annotatioList, default=str))

@app.route('/saveAnnotations', methods=['POST'])
@login_required
def saveAnnotations():
    try:
        if request.method == "POST" and request.is_json and len(request.data):
            data = request.get_json()
            for annotation in data:
                origAnotation = None
                file = db.session.query(AudioFiles).join(AudioFileParts, AudioFiles.id == AudioFileParts.audioFile_id).filter(AudioFileParts.id.is_(session["file"])).first()
                allowAnnotationTags = session["user"]["isAdmin"]
                allowManualAnnotationTags = session["user"]["isAdmin"]
                if file:
                    permission = UsergroupsPermissions.query.filter(UsergroupsPermissions.usergroup_id.is_(session["user"]["Usergroup"]), UsergroupsPermissions.folder_id.is_(file.folder_id)).first()
                    if permission:
                        allowAnnotationTags = allowAnnotationTags or permission.allowAnnotationTags
                        allowManualAnnotationTags = allowManualAnnotationTags or permission.allowManualAnnotationTags
                if "id" in annotation.keys():
                    origAnotation = Annotations.query.filter(Annotations.id.is_(annotation["id"]), Annotations.deleted.is_(None)).first()
                if "action" in annotation.keys() and annotation["action"] == "deleted":
                    if (origAnotation) and allowAnnotationTags:
                        db.session.add(AnnotationsHistory(user_id=session["user"]["id"], date=datetime.now(), annotation=origAnotation, start=origAnotation.history[-1].start, end=origAnotation.history[-1].end, action="D"))
                else:
                    annotation["start"] = datetime.utcfromtimestamp(annotation["start"]).time()
                    annotation["end"] = datetime.utcfromtimestamp(annotation["end"]).time()
                    if allowAnnotationTags:
                        if origAnotation and origAnotation.tag_id != annotation["tag_id"]:
                            db.session.add(AnnotationsHistory(user_id=session["user"]["id"], date=datetime.now(), annotation=origAnotation, start=annotation["start"], end=annotation["end"], action="D"))
                            tag = Tags.query.filter(Tags.id.is_(annotation["tag_id"]), Tags.deleted.is_(None)).first()
                            if tag.manual == True and tag.name != annotation["value"]:
                                tag.name = annotation["value"]
                                db.session.merge(tag)
                            db.session.add(Annotations(AudioFileParts=origAnotation.AudioFileParts, tag=tag, history=[AnnotationsHistory(user=session["user"], date=datetime.now(), start=annotation["start"], end=annotation["end"])]))
                        elif origAnotation:
                            tag = Tags.query.filter(Tags.id.is_(annotation["tag_id"]), Tags.deleted.is_(None)).first()
                            if tag.manual == True and tag.name != annotation["value"]:
                                tag.name = annotation["value"]
                                db.session.merge(tag)
                            origAnotation.history.append(AnnotationsHistory(user_id=session["user"]["id"], date=datetime.now(), start=annotation["start"], end=annotation["end"], action="M"))
                            db.session.merge(origAnotation)
                        else:
                            tag = Tags.query.filter(Tags.id.is_(annotation["tag_id"]), Tags.deleted.is_(None)).first()
                            if tag == None:
                                tag = Tags(name = annotation["value"], color = "255, 255, 255", manual = True)
                                db.session.add(tag)
                            db.session.add(Annotations(AudioFileParts_id=session["file"], tag=tag, history=[AnnotationsHistory(user_id=session["user"]["id"], date=datetime.now(), start=annotation["start"], end=annotation["end"])]))
            db.session.commit()
            db.session.flush()
        return jsonify(json.dumps({"succsess": True}, default=str))
    except Exception as e:
        print(traceback.format_exc())
        return jsonify(json.dumps({"succsess": False}, default=str))

@app.route('/loadFilesAndFolders')
@login_required
def loadFilesAndFolders():
    forAdmin = session["user"]["isAdmin"] and request.method == "GET" and "admin" in list(request.args.keys())
    def loadFolders(root = None):
        tree = []
        for folder in Folders.query.filter(Folders.parent_id.is_(root), Folders.deleted.is_(None)).order_by(func.lower(Folders.name)).all():
            node = {"name": folder.name}
            if forAdmin:
                node["id"] = folder.id
            subfolders = loadFolders(folder.id)
            if subfolders:
                node.update({"folders": subfolders})
            files = []
            permission = UsergroupsPermissions.query.filter(UsergroupsPermissions.usergroup_id.is_(session["user"]["Usergroup"]), UsergroupsPermissions.folder_id.is_(folder.id)).first()
            if session["user"]["isAdmin"] or (permission and permission.allowRead):
                for file in folder.audiofiles:
                    files.append(file.asDict())
                if files:
                    node.update({"files": files})
            if subfolders or files or forAdmin:
                tree.append(node)
        return tree
    tree = {}
    tree.update({"folders": loadFolders()})
    files = []
    permission = UsergroupsPermissions.query.filter(UsergroupsPermissions.usergroup_id.is_(session["user"]["Usergroup"]), UsergroupsPermissions.folder_id.is_(None)).first()
    if session["user"]["isAdmin"] or (permission and permission.allowRead):
        for file in AudioFiles.query.filter(AudioFiles.folder_id.is_(None), AudioFiles.deleted.is_(None)).order_by(func.lower(AudioFiles.name)).all():
            files.append(file.asDict())
            if not forAdmin:
                files[-1].pop("id")
                files[-1].pop("folder_id")
    tree.update({"files": files})
    return jsonify(json.dumps(tree, default=str))
    
@app.route('/getColormap', methods=['POST'])
@login_required
def getColormap():
    colors = []
    if request.method == "POST" and request.is_json and len(request.data) and "colormap" in request.get_json():
        data = request.get_json()
        colormaps = pyplot.colormaps()
        idx = next(i for i,v in enumerate(colormaps) if v.lower() == data["colormap"].lower())
        colormap = cm.get_cmap(colormaps[idx])
        colors = []
        if type(colormap) is cm.colors.LinearSegmentedColormap:
            temp = colormap(range(256))
            for idx in range(len(temp)):
                colors.append(list(temp[idx]))
        else:
            for idx in range(len(colormap.colors)):
                colors.append(colormap.colors[idx] + [1])
    return jsonify(json.dumps(colors, default=str))

@app.route('/UsergroupsAndUsersList')
@login_required
def UsergroupsAndUsersList():
    def loadFolders(root = None):
        tree = []
        for folder in Folders.query.filter(Folders.parent_id.is_(root), Folders.deleted.is_(None)).order_by(func.lower(Folders.name)).all():
            node = {"name": folder.name}
            node["id"] = folder.id
            tree.append(node)
            [tree.append(subfolder) for subfolder in loadFolders(folder.id)]
        return tree

    usergroups = []
    if session["user"]["isAdmin"]:
        for group in Usergroups.query.filter(Usergroups.deleted.is_(None)).order_by(Usergroups.name).all():
            usergroups.append({"id": group.id, "name": group.name, "users" : [], "permissions": []})
        usergroups.append({"id": None, "name": "None", "users" : [], "permissions": []})
        for group in usergroups:
            for user in Users.query.filter(Users.deleted.is_(None), Users.Usergroups_id.is_(group["id"])).order_by(Users.name).all():
                currentUser = user.asDict()
                del currentUser['privatekey']
                group["users"].append(currentUser)

            if group["id"]:
                for folder in loadFolders():
                    permission = Folders.query.join(UsergroupsPermissions, UsergroupsPermissions.folder_id == Folders.id, isouter=True).with_entities(Folders.name, Folders.parent_id, UsergroupsPermissions).filter(Folders.deleted.is_(None), UsergroupsPermissions.usergroup_id.is_(group["id"]), UsergroupsPermissions.folder_id.is_(folder["id"])).first()
                    if not permission:
                        permission = UsergroupsPermissions(folder_id = folder["id"], usergroup_id = group["id"])
                        db.session.add(permission)
                        permission = Folders.query.join(UsergroupsPermissions, UsergroupsPermissions.folder_id == Folders.id, isouter=True).with_entities(Folders.name, Folders.parent_id, UsergroupsPermissions).filter(Folders.deleted.is_(None), UsergroupsPermissions.usergroup_id.is_(group["id"]), UsergroupsPermissions.folder_id.is_(folder["id"])).first()
                    value = permission.UsergroupsPermissions.__dict__.copy()
                    if "_sa_instance_state" in value.keys():
                        value.pop('_sa_instance_state')
                    value["name"] = permission._data[0]
                    value["parentFolder_id"] = permission._data[1]
                    group["permissions"].append(value)
            db.session.commit()
        return jsonify(json.dumps(usergroups, default=str))
    return 'bad request!', 400

@app.route('/convertFeatureFileToWav', methods=['POST'])
@login_required
def convertFeatureFileToWav():
    file = None
    if (hasattr(request, "files") and "featureFile" in request.files.to_dict().keys()):
        files = request.files.to_dict(flat=False)["featureFile"]
        audio = None
        hasCalibrationInDb = False
        for file in files:
            featureFile = FeatureFile.load(bytearray(file.read()))
            hasCalibrationInDb = hasCalibrationInDb or (featureFile.calibrationInDb[0] != 0 and featureFile.calibrationInDb[1] != 0)
            if audio is None:
                audio = featureFile.wavSignal()
                channels = 1
                blocklen = 512
                if len(audio.shape) > 1:
                    channels = audio.shape[1]
                audio = audio.reshape(int(audio.shape[0] / channels), channels)
                win = np.transpose(np.tile(np.hanning(blocklen), (1, channels)))
            else:
                newAudio = featureFile.wavSignal()
                newAudio = newAudio.reshape(int(newAudio.shape[0] / channels), channels)
                audio[-int(blocklen / 2) :, :] = np.multiply(audio[-int(blocklen / 2) :, :], win[-int(blocklen / 2) :, :]) + np.multiply(newAudio[ : int(blocklen / 2), :], win[ : int(blocklen / 2), :])
                audio = np.concatenate((audio, newAudio[int(blocklen / 2) : , :]))
        if hasCalibrationInDb == False:
            audio = audio / np.max(audio)
        audio = (audio* (2 ** 15 - 1)).astype("<h")
        outFile = io.BytesIO()
        channels = 1
        if len(audio.shape) > 1:
            channels = audio.shape[1]
        with wave.open(outFile, "w") as f:
            f.setnchannels(channels)
            f.setsampwidth(2)
            f.setframerate(featureFile.fs)
            f.writeframes(audio.tobytes())
        outFile.seek(0)
        response = Response(stream_with_context(outFile))
        response.headers.set('Content-Type', 'audio/mpeg')
        response.headers.set('Cache-Control', 'no-cache')
        response.headers.set('Content-Length', outFile.getbuffer().nbytes)
        response.headers.set('Content-Disposition', 'attachment', filename=file.filename.replace(".feat", ".wav"))
        return response
    else:
        abort(404)

def wola(plugin, data, plugIdx):
    plugIdx += 1
    Overlays_2D = {}
    Overlays_3D = {}
    annotations = []
    returnAudioData = False
    tempBlocklenQuotient = 2
    if plugin.params["domain"] == Domain.Frequency:
       plugin.params["timeDomainWithoutOverlap"] = False 
    if plugin.params["timeDomainWithoutOverlap"]:
        tempBlocklenQuotient = 1
    if (int(plugin.params["fftSize"]) < int(plugin.params["blockLen"])):
        plugin.params["fftSize"] = int(plugin.params["blockLen"])
    # create hann-windows (windowIn and windowOut) at initialization
    if not str(plugin.params["blockLen"]) + "_" + str(data.shape[0]) in Windows[str(plugin.params["domain"])].keys():
        Windows[str(plugin.params["domain"])][str(plugin.params["blockLen"]) + "_" + str(data.shape[0])] = {"windowIn": None, "windowOut": None}
        if plugin.params["domain"] == Domain.Frequency:
            Windows[str(plugin.params["domain"])][str(plugin.params["blockLen"]) + "_" + str(data.shape[0])]["windowIn"] = np.tile(np.sqrt(np.hanning(int(plugin.params["blockLen"]))), (data.shape[0], 1)).astype(np.float32)
            Windows[str(plugin.params["domain"])][str(plugin.params["blockLen"]) + "_" + str(data.shape[0])]["windowOut"] = np.tile(np.sqrt(np.hanning(int(plugin.params["blockLen"]))), (data.shape[0], 1)).astype(np.float32)
        else:
            Windows[str(plugin.params["domain"])][str(plugin.params["blockLen"]) + "_" + str(data.shape[0])]["windowIn"] = np.ones((data.shape[0], int(plugin.params["blockLen"])), dtype=np.float32)
            if plugin.params["timeDomainWithoutOverlap"] == False:
                Windows[str(plugin.params["domain"])][str(plugin.params["blockLen"]) + "_" + str(data.shape[0])]["windowOut"] = np.tile(np.hanning(int(plugin.params["blockLen"])), (data.shape[0], 1)).astype(np.float32)
            else:
                Windows[str(plugin.params["domain"])][str(plugin.params["blockLen"]) + "_" + str(data.shape[0])]["windowOut"] = Windows[str(plugin.params["domain"])][str(plugin.params["blockLen"]) + "_" + str(data.shape[0])]["windowIn"]
    # create and fill buffers at initialization
    if data.shape[1] < int(plugin.params["blockLen"]):
        data = np.concatenate([np.zeros([data.shape[0], plugin.params["blockLen"] - data.shape[1]]), data], 1)
    dataOut = np.zeros(data.shape, dtype=np.float32)
    if not hasattr(plugin, "lastBlockIn") or plugin.lastBlockIn is None or type(plugin.lastBlockIn) is dict:
        plugin.lastBlockIn = np.zeros([data.shape[0], int(plugin.params["blockLen"] / tempBlocklenQuotient)])
        if plugin.params["timeDomainWithoutOverlap"] == False:
            plugin.lastBlockOut = np.zeros([data.shape[0], int(plugin.params["blockLen"] / 2)])
    if type(plugin.lastBlockIn) is bytes:
        plugin.lastBlockIn = np.fromstring(plugin.lastBlockIn, np.float32)
        plugin.lastBlockIn.shape = [dataOut.shape[0], int(plugin.lastBlockIn.shape[0] / dataOut.shape[0])]
    else:
        plugin.lastBlockIn = np.asarray(plugin.lastBlockIn, np.float32)
    if hasattr(plugin, "lastBlockOut"):
        if type(plugin.lastBlockOut) is dict:
            plugin.lastBlockOut = []
        if type(plugin.lastBlockOut) is bytes:
            plugin.lastBlockOut = np.fromstring(plugin.lastBlockOut, np.float32)
            plugin.lastBlockOut.shape = [dataOut.shape[0], int(plugin.lastBlockOut.shape[0] / dataOut.shape[0])]
        else:
            plugin.lastBlockOut = np.asarray(plugin.lastBlockOut, np.float32)
        try:
            dataOut[:, : plugin.lastBlockOut.shape[1]] = plugin.lastBlockOut
        except:
            pass
    plugins = [plugin]
    if not hasattr(plugin, "executeWithoutSubPlugins"):
        while plugIdx < len(session["Plugins"]) and (not hasattr(session["Plugins"][plugIdx]["object"], "process") or (session["Plugins"][plugIdx]["object"].domain == plugin.params["domain"] and int(session["Plugins"][plugIdx]["object"].fftSize) == int(plugin.params["fftSize"]) and int(session["Plugins"][plugIdx]["object"].blockLen) == int(plugin.params["blockLen"]) and session["Plugins"][plugIdx]["object"].timeDomainWithoutOverlap == plugin.params["timeDomainWithoutOverlap"])):
            if hasattr(session["Plugins"][plugIdx]["object"], "process"):
                plugins.append(session["Plugins"][plugIdx]["object"])
            plugIdx += 1
    idx = 0
    winIn = Windows[str(plugin.params["domain"])][str(int(plugin.params["blockLen"])) + "_" + str(data.shape[0])]["windowIn"]
    while idx < data.shape[1]:
        if plugin.params["timeDomainWithoutOverlap"] == False:
            temp = np.concatenate((plugin.lastBlockIn, data[:, idx : (idx + int(plugin.params["blockLen"] / 2))]), 1)
            if temp.shape[1] < winIn.shape[1]:
                temp = np.concatenate((temp, np.zeros([temp.shape[0], winIn.shape[1] - temp.shape[1]])), 1)
            block = winIn * temp
            plugin.lastBlockIn = data[:, idx : (idx + int(plugin.params["blockLen"] / 2))]
        else:
            block = winIn * data[:, idx: (idx + int(plugin.params["blockLen"]))]
        if plugin.params["domain"] == Domain.Frequency:
            block = nf.rfft(block, int(plugin.params["fftSize"]))
        for tmpPlug in plugins:
            tmpPlug.blockTime = plugin.blockTime
            processedData = executeMethodInSanbox(tmpPlug, tmpPlug.process, block)                
            if not type(processedData) is tuple: 
                processedData = (processedData, )
            for item in processedData:
                if type(item) is TimePoint or type(item) is FrequencyPoint or type(item) is Annotation:
                    item = [item]
                if type(item) is list and len(item):
                    if type(item[0]) is TimePoint:
                        if not tmpPlug.__class__.__name__ in Overlays_2D:
                            Overlays_2D[tmpPlug.__class__.__name__] = []
                        values = [0] * tmpPlug.params["channels"]
                        for point in item:
                            values[point.channel] = [point.value, point.color]
                        Overlays_2D[tmpPlug.__class__.__name__].append(values)
                    elif type(item[0]) is FrequencyPoint:
                        if not tmpPlug.__class__.__name__ in Overlays_3D:
                            Overlays_3D[tmpPlug.__class__.__name__] = []
                        values = [0] * tmpPlug.params["channels"]
                        for point in item:
                            values[point.channel] = [point.value, point.color]
                        Overlays_3D[tmpPlug.__class__.__name__].append(values)
                    elif type(item[0]) is Annotation:
                        for annotation in item:
                            annotations.append(json.dumps(annotation, default=vars))
                elif type(item) is np.ndarray:
                    block = item
                    returnAudioData = True            
        if plugin.params["domain"] == Domain.Frequency:
            block = nf.irfft(block)
        lastBlockOut = Windows[str(plugin.params["domain"])][str(int(plugin.params["blockLen"])) + "_" + str(data.shape[0])]["windowOut"] * block
        lastIdx = 0
        if (idx + int(plugin.params["blockLen"])) > dataOut.shape[1]:
            lastIdx = (idx + int(plugin.params["blockLen"])) - dataOut.shape[1]
        dataOut[:, idx: (idx + int(plugin.params["blockLen"]))] += lastBlockOut[:, : (lastBlockOut.shape[1] - lastIdx)]
        if hasattr(plugin, "lastBlockOut"):
            plugin.lastBlockOut = lastBlockOut[:, int(plugin.params["blockLen"] / 2) :]
        idx += int(int(plugin.params["blockLen"]) / tempBlocklenQuotient)
        plugin.blockTime["start"] = idx / plugin.params["sampleRate"]
        plugin.blockTime["end"] = (idx + plugin.params["blockLen"] / tempBlocklenQuotient) / plugin.params["sampleRate"]

    plugin.lastBlockIn = plugin.lastBlockIn.astype(np.float32).tobytes()
    if hasattr(plugin, "lastBlockOut"):
        plugin.lastBlockOut = plugin.lastBlockOut.astype(np.float32).tobytes()
    return dataOut, Overlays_2D, Overlays_3D, annotations, plugIdx, returnAudioData

@socketio.on('message')
@app.route('/socketMessage', methods=['POST'])
def handle_message(transferData):
    transferData, currentPlugins = processAudiodata(transferData)
    try:
        try:
            emit('message', transferData)
        except Exception as ex:
            print(traceback.format_exc())
            emit('message', json.loads(json.dumps(transferData, separators=(',', ':'), cls=NumpyEncoder)))
    except Exception as ex:
        traceback.format_exc()
        emit('error', {"target": currentPlugins[0]["plugin"].__class__.__name__, "message": traceback.format_exc()})

def processAudiodata(transferData):
    oldPluginVars = {}
    if not "settings" in transferData.keys() or transferData["settings"] == None:
        transferData["settings"] = {}
    hasChanges = False
    transferData["Overlays_2D"] = {}
    transferData["Overlays_3D"] = {}
    transferData["annotations"] = []
    returnAudioData = False
    currentPlugins = []
    for plugin in session["Plugins"]:
        plugin["object"].executeWithoutSubPlugins = False
        if "PluginId" in transferData.keys() and ((plugin["object"].__class__.__name__ == transferData["PluginId"] or plugin["object"].type == PluginType.insert) and (("Init" in transferData.keys() and hasattr(plugin["object"], "preprocess")) or hasattr(plugin["object"], "process") or "postProcess" in  transferData.keys())):
            plugin["object"].executeWithoutSubPlugins = True
        currentPlugins.append({"plugin": plugin["object"], "isTemp": plugin["isTemp"]})
    for idx in range(len(currentPlugins)):
        oldPluginVars[type(currentPlugins[idx]["plugin"]).__name__] = copy.deepcopy(vars(currentPlugins[idx]["plugin"]))
        if type(currentPlugins[idx]["plugin"]).__name__ in transferData["settings"].keys():
            if "Init" in transferData["settings"][type(currentPlugins[idx]["plugin"]).__name__ ].keys():
                transferData["settings"][type(currentPlugins[idx]["plugin"]).__name__ ]["Init"].update(transferData["settings"][type(currentPlugins[idx]["plugin"]).__name__]["params"])
                try:
                    currentPlugins[idx]["plugin"] = executeMethodInSanbox(currentPlugins[idx], currentPlugins[idx]["plugin"].__class__, transferData["settings"][type(currentPlugins[idx]["plugin"]).__name__ ]["Init"])
                except Exception as ex:
                    print(traceback.format_exc())
                    emit('error', {"target": currentPlugins[idx]["plugin"].__class__.__name__, "message": "Plugin Initialization failed: " + repr(ex)})
                    return None
                AudioPlugin.appendOldParamsAfterInit(currentPlugins[idx]["plugin"])
                if hasattr(plugin["object"], "preprocess"):
                    executeMethodInSanbox(currentPlugins[idx], currentPlugins[idx]["plugin"].preprocess)
                transferData["settings"][type(currentPlugins[idx]["plugin"]).__name__] = {}
                transferData["Init"] = True
            if "params" in transferData["settings"][type(currentPlugins[idx]["plugin"]).__name__].keys():
                currentPlugins[idx]["plugin"].params.update(transferData["settings"][type(currentPlugins[idx]["plugin"]).__name__]["params"])
            for item in transferData["settings"][type(currentPlugins[idx]["plugin"]).__name__]:
                if item != "params" and not item.endswith("__shape__"):
                    if item + "__shape__" in transferData["settings"][type(currentPlugins[idx]["plugin"]).__name__].keys():
                        transferData["settings"][type(currentPlugins[idx]["plugin"]).__name__][item] = np.fromstring(transferData["settings"][type(currentPlugins[idx]["plugin"]).__name__][item], np.float32)
                        transferData["settings"][type(currentPlugins[idx]["plugin"]).__name__][item].shape = transferData["settings"][type(currentPlugins[idx]["plugin"]).__name__][item + "__shape__"]
                    setattr(currentPlugins[idx]["plugin"], item, transferData["settings"][type(currentPlugins[idx]["plugin"]).__name__][item])
            try:
                currentPlugins[idx]["plugin"].isTemp = currentPlugins[idx]["isTemp"]
                executeMethodInSanbox(currentPlugins[idx]["plugin"], currentPlugins[idx]["plugin"].updateParams)
            except Exception as ex:
                print(traceback.format_exc())
                emit('error', {"target": currentPlugins[idx]["plugin"].__class__.__name__, "message": "Plugin Method updateParams() failed: " + repr(ex)})
    if "Init" in transferData.keys():
        if "data" in transferData.keys():
            del transferData["data"]
    elif "data" in transferData.keys() and type(transferData["data"]) is list and len(transferData["data"]):
        if type(transferData["data"][0]) is bytes:
            data = np.fromstring(b''.join(transferData["data"]), np.float32)
            data.shape = [len(transferData["data"]), int(data.shape[0] / len(transferData["data"]))]
        else:
            data = np.asarray(transferData["data"], np.float32)
        transferData["data"] = False
        idx = 0
        while idx < len(currentPlugins):
            if hasattr(currentPlugins[idx]["plugin"], "process"):
                currentPlugins[idx]["plugin"].isTemp = currentPlugins[idx]["isTemp"]
                try:
                    data, Overlays_2D, Overlays_3D, annotations, idx, returnAudioDataTemp = wola(currentPlugins[idx]["plugin"], data, idx)
                except Exception as ex:
                    print(traceback.format_exc())
                    emit('error', {"target": currentPlugins[idx]["plugin"].__class__.__name__, "message": repr(ex) + " in serverside process() method!"})
                    return None
                returnAudioData = returnAudioData or returnAudioDataTemp
                transferData["annotations"].append(annotations)
                transferData["Overlays_2D"].update(Overlays_2D)
                transferData["Overlays_3D"].update(Overlays_3D)
            else:
                idx += 1
        if returnAudioData:
            transferData["data"] = data.astype(np.float32).tobytes()
        if transferData["data"] == False:
            if len(data.shape) == 1:
                data.shape = [1,data.shape[0]]
            transferData["dataLen"] = data.shape[1]
    elif "postProcess" in  transferData.keys():
        transferData["data"] = {}
        for idx in range(len(currentPlugins)):
            if hasattr(currentPlugins[idx]["plugin"], "postprocess"):
                try:
                    currentPlugins[idx]["isTemp"] = currentPlugins[idx]["isTemp"]
                    data = executeMethodInSanbox(currentPlugins[idx], currentPlugins[idx]["plugin"].postprocess)
                except Exception as ex:
                    print(traceback.format_exc())
                    emit('error', {"target": currentPlugins[idx]["plugin"].__class__.__name__, "message": "Plugin Method postprocess() failed: " + repr(ex)})
                    return None
                transferData["data"][type(currentPlugins[idx]["plugin"]).__name__] = data
    for idx in range(len(currentPlugins)):
        newPluginVars = copy.deepcopy(vars(currentPlugins[idx]["plugin"]))
        if "controls" in newPluginVars.keys():
            newPluginVars.pop("controls")
        if "executeWithoutSubPlugins" in newPluginVars.keys():
            newPluginVars.pop("executeWithoutSubPlugins")
        if not type(currentPlugins[idx]["plugin"]).__name__ in transferData["settings"].keys():
            transferData["settings"][type(currentPlugins[idx]["plugin"]).__name__] = {}
        for var in vars(currentPlugins[idx]["plugin"]):
            if not var in oldPluginVars[type(currentPlugins[idx]["plugin"]).__name__].keys() or (var in newPluginVars.keys() and str(newPluginVars[var]) != str(oldPluginVars[type(currentPlugins[idx]["plugin"]).__name__][var])):
                hasChanges = True
            if var in newPluginVars.keys() and type(newPluginVars[var]) is np.ndarray:
                newPluginVars[var + "__shape__"] = newPluginVars[var].shape
                newPluginVars[var] = newPluginVars[var].astype(np.float32).tobytes()
        if hasChanges:
            transferData["settings"][type(currentPlugins[idx]["plugin"]).__name__].update(newPluginVars)
    if len(transferData["annotations"]) == 0:
        del transferData["annotations"]
    if len(transferData["Overlays_2D"]) == 0:
        del transferData["Overlays_2D"]
    if len(transferData["Overlays_3D"]) == 0:
        del transferData["Overlays_3D"]
    return transferData, currentPlugins

class NumpyEncoder(json.JSONEncoder):
    """ Special json encoder for numpy types """
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)