from glob import glob
import importlib
import os
import re
import sys
from flask import Flask
from flask_login import LoginManager
from app.AudioPlugin import AudioPlugin
from app.config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_socketio import SocketIO
from flask_session import Session
import inspect

auth = LoginManager()
auth.login_view = 'login'
app = Flask(__name__, static_url_path='/static')
app.config.from_object(Config)
app.config['SESSION_TYPE'] = 'filesystem'
app.session_interface = Session()

for file in glob(os.path.join("Plugins", "**", "*.py"), recursive=True):
    path, pluginName = os.path.split(file)
    pluginName = pluginName.replace(".py", "")
    spec = importlib.util.spec_from_file_location(pluginName, file)
    module = importlib.util.module_from_spec(spec)        
    if module.__name__ == spec.name:
        spec.loader.exec_module(module)
        if hasattr(module, pluginName) and AudioPlugin in inspect.getmro(getattr(module, pluginName)):
            sys.modules[pluginName] = module

db = SQLAlchemy(app)
Session(app)
migrate = Migrate(app, db)
socketio = SocketIO(app, manage_session = False)
auth.init_app(app)

from app import routes, models

if Config.SQLALCHEMY_DATABASE_URI.startswith("sqlite:") and not os.path.isfile(re.sub("sqlite.*:///", "", Config.SQLALCHEMY_DATABASE_URI)):
    from app import initializeDatabase
    with app.app_context():
        initializeDatabase.initializeDatabase()
