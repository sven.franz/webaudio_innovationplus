from flask_apscheduler import APScheduler

scheduler = APScheduler()

def cronJob():
    print("This test runs every 60 minutes")

def cronJobInit():
    scheduler.add_job(id = 'Scheduled Task', func = cronJob, trigger = "interval", seconds = 60 * 60)
    scheduler.start()
