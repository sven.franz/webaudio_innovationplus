from app import db
from app.models import UUIDstr

class Folders(db.Model):
    id = db.Column(db.String(36), primary_key=True, unique=True, default=UUIDstr)
    name = db.Column(db.String(1024))
    parent_id = db.Column(db.String(36), db.ForeignKey('folders.id'), index=True)
    parent = db.relationship('Folders', remote_side=id, backref='sub_folders')
    #audiofiles = db.relationship('AudioFiles', backref='folder', order_by="AudioFiles.name")
    audiofiles = db.relationship('AudioFiles', primaryjoin="and_(Folders.id==AudioFiles.folder_id, AudioFiles.deleted==None)", backref='folder', order_by="AudioFiles.name")
    deleted = db.Column(db.DateTime, nullable = True)