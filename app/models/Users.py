from flask_login import UserMixin
from app import db
from Crypto.PublicKey import RSA
from werkzeug.security import generate_password_hash, check_password_hash
import hashlib

from app.models import UUIDstr
from app.models.Folders import Folders

class Usergroups(db.Model):
    id = db.Column(db.String(36), primary_key=True, unique=True, default=UUIDstr)
    name = db.Column(db.String(128))
    deleted = db.Column(db.DateTime, nullable = True)

class UsergroupsPermissions(db.Model):   
    id = db.Column(db.String(36), primary_key=True, unique=True, default=UUIDstr)
    usergroup_id = db.Column(db.String(36), db.ForeignKey(Usergroups.id), index=True)
    folder_id = db.Column(db.String(36), db.ForeignKey(Folders.id), index=True)
    allowRead = db.Column(db.Boolean, default = False)
    allowModify = db.Column(db.Boolean, default = False)
    allowAnnotationTags = db.Column(db.Boolean, default = False)
    allowManualAnnotationTags = db.Column(db.Boolean, default = False)
    allowChangeProtection = db.Column(db.Boolean, default = False)
    
    def asDict(self):
        value = self.__dict__.copy()
        if "_sa_instance_state" in value.keys():
            value.pop('_sa_instance_state')
        return value

class Users(UserMixin, db.Model):
    id = db.Column(db.String(36), primary_key=True, unique=True, default=UUIDstr)
    name = db.Column(db.String(128))
    email = db.Column(db.String(128))
    password = db.Column(db.String(128))
    privatekey = db.Column(db.LargeBinary(2048))
    publickey = db.Column(db.LargeBinary(1024))
    isDeveloper = db.Column(db.Boolean, default = False)
    isAdmin = db.Column(db.Boolean, default = False)
    deleted = db.Column(db.DateTime, nullable = True)
    Usergroups_id = db.Column(db.String(36), db.ForeignKey(Usergroups.id), index=True)
    readonly = ["email", "password", "privatekey", "publickey"]

    def __init__(self, **kwargs):
        if "password" in kwargs.keys():
            kwargs["password"] = generate_password_hash(kwargs["password"])
        super(self.__class__, self).__init__(**{key:kwargs[key] for key in set(dir(self)).intersection(list(kwargs.keys())) if key in kwargs})
    
    def getPassphrase(self, password):
        passphrase = None
        try:
            if self.check_password(password):
                if not self.id:
                    db.session.commit()
                if type(password) is bytes:
                    password = password.decode("utf-8")
                passphrase = hashlib.sha224((password + self.id).encode('utf-8')).hexdigest()
                if not self.privatekey:
                    key = RSA.generate(2048)
                    self.privatekey = key.exportKey(passphrase = passphrase, pkcs = 8)
                    self.publickey = key.publickey().exportKey()
                    db.session.commit()
        except:
            pass
        return passphrase
    
    def __repr__(self):
        return '<User {}>'.format(self.name)
            
    def check_password(self, password):
        try:
            if type(password) is bytes:
                password = password.decode("utf-8")
            if check_password_hash(self.password, password):
                return True
        except:
            pass
        return False

    def asDict(self):
        value = self.__dict__.copy()
        if "_sa_instance_state" in value.keys():
            value.pop('_sa_instance_state')
        return value
