from app import db
from app.models import UUIDstr

class Tags(db.Model):
    id = db.Column(db.String(36), primary_key=True, unique=True, default=UUIDstr)
    name = db.Column(db.String(128))
    color = db.Column(db.String(128))
    parent_id = db.Column(db.String(36), db.ForeignKey('tags.id'), index=True)
    parent = db.relationship('Tags', remote_side=id, backref='sub_tags')
    annotations = db.relationship("Annotations", backref="tag")
    manual = db.Column(db.Boolean, default = False)
    deleted = db.Column(db.DateTime, nullable = True)

    def asDict(self):
        value = self.__dict__
        if "_sa_instance_state" in value.keys():
            value.pop('_sa_instance_state')
        return value
