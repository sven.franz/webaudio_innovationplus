import os
import soundfile as sf
import hashlib
import io
import math
import numpy as np
import secrets
import werkzeug.datastructures
from sqlalchemy import inspect

from app import db
from os import path
from app.helper.divideData import divide, min_len, search_interval_start
from app.models import UUIDstr
from Crypto.PublicKey import RSA
from Crypto.Cipher import AES, PKCS1_OAEP

from app.models.Users import Users
from app.models.Folders import Folders

import struct

class AudioFiles(db.Model):
    id = db.Column(db.String(36), primary_key=True, unique=True, default=UUIDstr)
    name = db.Column(db.String(1024))
    folder_id = db.Column(db.String(36), db.ForeignKey(Folders.id), index=True)
    owner_id = db.Column(db.String(36), db.ForeignKey(Users.id), index=True)
    samplingRate = db.Column(db.Integer, default = 0)
    channels = db.Column(db.Integer, default = 0)
    audioFileParts = db.relationship('AudioFileParts', backref='AudioFile', order_by='AudioFileParts.part')
    deleted = db.Column(db.DateTime, nullable = True)
    readonly = ["name", "samplingRate", "channels", "audioFileParts"]

    def __init__(self, **kwargs):
        if not "id" in kwargs.keys():
            self.id = UUIDstr()
            if "data" in kwargs.keys() and type(kwargs["data"]) is werkzeug.datastructures.FileStorage:
                data, kwargs["samplingRate"] = sf.read(kwargs["data"])
            else:
                data, kwargs["samplingRate"] = sf.read(kwargs["name"])
            kwargs["name"] = path.split(kwargs["name"])[-1]
            if len(data.shape) == 1:
                data = np.asmatrix(data)
            if data.shape[1] > data.shape[0]:
                data = np.transpose(data)
            data = (data * 2 ** (np.iinfo("int16").bits - 1)).astype(np.int16)
            kwargs["channels"] = data.shape[1]
            if type(kwargs["user"]) is Users:
                kwargs["owner_id"] = kwargs["user"].id
            else:
                kwargs["owner_id"] = kwargs["user"]["id"]
            super(self.__class__, self).__init__(**{key:kwargs[key] for key in set(dir(self)).intersection(list(kwargs.keys())) if key in kwargs})

            dataIO = io.BytesIO()
            dataIO.write(write_header(len(data) * self.channels * 2, self.channels, 2, self.samplingRate))
            dataIO.write(data.tobytes())
            dataIO.seek(0)
            parts, fs = divide({"data" : dataIO, "NoHighpass" : False})
            if len(parts) < 2 and data.shape[0] / self.samplingRate > min_len * 60:
                parts = np.split(data, [math.ceil(1 / math.ceil((data.shape[0] / self.samplingRate) / (search_interval_start * 60)) * data.shape[0]), ], 0)
            for idx in range(len(parts)):
                db.session.add(AudioFileParts(data = parts[idx], audioFile = self, part = idx, isPublic = False, samplingRate = self.samplingRate, user = kwargs["user"], password = kwargs["password"]))
        else:
            super(self.__class__, self).__init__(**{key:kwargs[key] for key in set(dir(self)).intersection(list(kwargs.keys())) if key in kwargs})
        
    def asDict(self):
        value = self.__dict__.copy()
        if "_sa_instance_state" in value.keys():
            value.pop('_sa_instance_state')
        value.update({"duration" : sum(filepart.duration for filepart in self.audioFileParts)})
        fileParts = []
        for filepart in self.audioFileParts:
            fileParts.append(filepart.asDict())
            fileParts[-1].pop("audioFile_id")
            fileParts[-1].update({"samplingRate" : self.samplingRate, "channels": self.channels, "filename": self.name + " [" + str(fileParts[-1]["part"] + 1) + "]" })
        value.update({"fileParts": fileParts})
        return value

class AudioFileParts(db.Model):
    id = db.Column(db.String(36), primary_key=True, unique=True, default=UUIDstr)
    part = db.Column(db.Integer, default = 0)
    audioFile_id = db.Column(db.String(36), db.ForeignKey(AudioFiles.id), index=True)
    isPublic = db.Column(db.Boolean, default = False)
    fileSize = db.Column(db.BigInteger(), default = 0)
    duration = db.Column(db.Float, default = 0)
    annotations = db.relationship("Annotations", backref="annotations")
    encryptions = db.relationship("AudioFilePartsEncryption", backref="encryption", lazy="dynamic")

    def __init__(self, **kwargs):
        self.id = UUIDstr()
        self.audioFile_id = kwargs["audioFile"].id
        super(self.__class__, self).__init__(**{key:kwargs[key] for key in set(dir(self)).intersection(list(kwargs.keys())) if key in kwargs})
        self.samplingRate = kwargs["audioFile"].samplingRate
        if not self.AudioFile and "audioFile" in kwargs.keys():
            self.AudioFile = kwargs["audioFile"]
        self.setData(kwargs["data"], kwargs["user"], kwargs["password"])

    def filename(self):
        return path.join("AudioFiles", hashlib.sha224((self.audioFile_id + "." + self.id).encode('utf-8')).hexdigest() + ".audiofile")

    def setData(self, data, user, password):        
        def readGenerator(data):
            if type(data) is werkzeug.datastructures.FileStorage:
                for block in sf.blocks(data, 64 * 1024, dtype = 'int16'):
                    yield block.tobytes(order='C')
            else:
                while True:
                    chunk = data.read(64 * 1024)
                    if len(chunk) == 0: break
                    yield chunk

        if not os.path.exists("AudioFiles"):
            os.makedirs("AudioFiles")
        if type(password) is str:
            password = str.encode(password)
        if not type(user) is Users:
            if "user" in user:
                id = user["user"]["id"]
            elif "id" in user:
                id = user["id"]
            user = Users.query.filter(Users.id.is_(id), Users.deleted.is_(None)).first()
        if user and user.check_password(password):
            if type(data) is werkzeug.datastructures.FileStorage:
                data.seek(0, os.SEEK_END)
                self.fileSize = data.tell()
                data.seek(0) 
                dataGenerator = readGenerator(data)
            elif type(data) is bytes:
                self.fileSize = len(data) 
                dataGenerator = readGenerator(io.BytesIO(data))
            elif type(data) is np.matrix:
                if not data.dtype is np.dtype('int16'):
                    data = (data * 2 ** (np.iinfo("int16").bits - 1)).astype(np.int16)
                self.fileSize = data.nbytes
                self.duration = round(data.shape[0] / self.samplingRate * 1000) / 1000
                dataGenerator = readGenerator(io.BytesIO(data.tobytes()))
            else:
                dataGenerator = data
            self.samplingRate = self.AudioFile.samplingRate
            self.channels = self.AudioFile.channels
            #if type(transferData["data"][0]) is bytes:
            #    data = np.fromstring(b''.join(transferData["data"]), np.float32)
            #    data.shape = [len(transferData["data"]), int(data.shape[0] / len(transferData["data"]))]
            if self.isPublic == False:
                if inspect(self).persistent == False or not self.encryptions.filter(AudioFilePartsEncryption.user_id.is_(user.id)).first():
                    encryption = AudioFilePartsEncryption(audiofilePart = self, user = user)
                    db.session.add(encryption)
                else:
                    encryption = self.encryptions.filter(AudioFilePartsEncryption.user_id.is_(user.id)).first()
                RSAencryptor = PKCS1_OAEP.new(RSA.importKey(encryption.user.privatekey, passphrase = user.getPassphrase(password)))
                AESencryptor = AES.new(RSAencryptor.decrypt(encryption.key), AES.MODE_CBC, encryption.initializationVector)
            with open(self.filename() + ".bak", 'wb') as outfile:
                if self.isPublic:
                    outfile.write(write_header(self.fileSize, self.channels, 2,  self.samplingRate))
                for chunk in dataGenerator:
                    if self.isPublic == False:
                        if len(chunk) % 16 != 0:
                            chunk += b'0' * (16 - len(chunk) % 16)
                        chunk = AESencryptor.encrypt(chunk)
                    outfile.write(chunk)
            os.replace(self.filename() + ".bak", self.filename())

    def getData(self, user, password):
        if type(password) is str:
            password = str.encode(password)
        encryption = None
        if not type(user) is Users and type(user) is dict:
            if "user" in user:
                id = user["user"]["id"]
            elif "id" in user:
                id = user["id"]
            user = Users.query.filter(Users.id.is_(id), Users.deleted.is_(None)).first()
        if user and user.check_password(password):
            blobLen = 0
            with open(self.filename(), 'rb') as infile:
                while True:
                    chunk = infile.read(24 * 1024)
                    if len(chunk) == 0:
                        return
                    if blobLen == 0:
                        if chunk[0:4] != b'RIFF' and chunk[12:16] != b'fmt ':
                            encryption = self.encryptions.filter(AudioFilePartsEncryption.user_id.is_(user.id)).first()
                            if encryption:
                                encryptor = PKCS1_OAEP.new(RSA.importKey(encryption.user.privatekey, passphrase = user.getPassphrase(password))) # hashlib.sha224(user.password.encode('utf-8')).hexdigest()
                                decryptor = AES.new(encryptor.decrypt(encryption.key), AES.MODE_CBC, encryption.initializationVector)
                                yield write_header(self.fileSize, self.AudioFile.channels, 2, self.AudioFile.samplingRate)
                            else:
                                return
                    if encryption:
                        chunk = decryptor.decrypt(chunk)
                    if blobLen + len(chunk) > self.fileSize:
                        chunk = chunk[ : self.fileSize - blobLen]
                    blobLen += len(chunk)
                    yield chunk
        return None

    def asDict(self, userId = None):
        value = self.__dict__.copy()
        if "_sa_instance_state" in value.keys():
            value.pop('_sa_instance_state')
        value["filename"] = self.AudioFile.name
        value["samplingRate"] = self.AudioFile.samplingRate
        value["channels"] = self.AudioFile.channels
        value["isOwner"] = self.AudioFile.owner_id == userId
        return value

class AudioFilePartsEncryption(db.Model):
    id = db.Column(db.String(36), primary_key=True, unique=True, default=UUIDstr)
    audiofilePart_id = db.Column(db.String(36), db.ForeignKey(AudioFileParts.id), index=True)
    user = db.relationship(Users, backref='user', order_by=Users.name)
    user_id = db.Column(db.String(36), db.ForeignKey(Users.id), index=True)
    key = db.Column(db.LargeBinary(32))
    initializationVector = db.Column(db.LargeBinary(16))

    def __init__(self, **kwargs):
        encryptor = PKCS1_OAEP.new(RSA.importKey(kwargs["user"].publickey))
        kwargs["key"] = encryptor.encrypt(secrets.token_bytes(32))
        kwargs["initializationVector"] = secrets.token_bytes(16)
        kwargs["audiofilePart_id"] = kwargs["audiofilePart"].id
        super(self.__class__, self).__init__(**{key:kwargs[key] for key in set(dir(self)).intersection(list(kwargs.keys())) if key in kwargs})

def write_header(_byteLen, _nchannels, _sampwidth, _framerate):
    WAVE_FORMAT_PCM = 0x0001
    initlength = _byteLen
    bytes_to_add = b'RIFF'
    _nframes = initlength // (_nchannels * _sampwidth)
    _datalength = _nframes * _nchannels * _sampwidth
    bytes_to_add += struct.pack('<L4s4sLHHLLHH4s',
        36 + _datalength, b'WAVE', b'fmt ', 16,
        WAVE_FORMAT_PCM, _nchannels, _framerate,
        _nchannels * _framerate * _sampwidth,
        _nchannels * _sampwidth,
        _sampwidth * 8, b'data')
    bytes_to_add += struct.pack('<L', _datalength)
    return bytes_to_add