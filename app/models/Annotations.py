from app import db
from app.models import UUIDstr
from app.models.AudioFiles import AudioFileParts
from app.models.Tags import Tags
from app.models.Users import Users

class Annotations(db.Model):
    id = db.Column(db.String(36), primary_key=True, unique=True, default=UUIDstr)
    AudioFileParts_id = db.Column(db.String(36), db.ForeignKey(AudioFileParts.id), index=True)
    tag_id = db.Column(db.String(36), db.ForeignKey(Tags.id), index=True)
    history = db.relationship("AnnotationsHistory", backref="annotation", order_by="AnnotationsHistory.date")
    deleted = db.Column(db.DateTime, nullable = True)

    def asDict(self):
        myTag = {"name": None, "color": "255, 255, 255", "manual": False}
        if self.tag:
            myTag = self.tag.asDict()
            if "sub_tags" in myTag.keys():
                myTag.pop("sub_tags")
        value = self.__dict__
        value.update({"color": myTag["color"], "value" : myTag["name"], "name" : myTag["name"], "manual": myTag["manual"]})
        history = None
        if len(self.history):
            history = self.history[-1].asDict()
            history.pop("id")
            history.pop("annotation_id")
        value.update(history)
        value.pop('tag')
        value.pop('history')
        value.pop('_sa_instance_state')
        return value

class AnnotationsHistory(db.Model):
    id = db.Column(db.String(36), primary_key=True, unique=True, default=UUIDstr)
    annotation_id = db.Column(db.String(36), db.ForeignKey(Annotations.id), index=True)
    user_id = db.Column(db.String(36), db.ForeignKey(Users.id), index=True)
    date = db.Column(db.DateTime)
    start = db.Column(db.Time)
    end = db.Column(db.Time)
    action = db.Column(db.String(1))

    def asDict(self):
        value = self.__dict__
        if "_sa_instance_state" in value.keys():
            value.pop('_sa_instance_state')
        return value
