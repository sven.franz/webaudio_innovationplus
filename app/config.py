import os
import secrets
import configparser

basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    SECRET_KEY = secrets.token_urlsafe(32) # os.environ.get('SECRET_KEY') or secrets.token_urlsafe(32)
    BASIC_AUTH_FORCE = os.environ.get('BASIC_AUTH_FORCE') or True
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or 'sqlite:///' + os.path.join(basedir, "..", 'db.sqlite3')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    TEMPLATES_AUTO_RELOAD = True
    SMTP_EMAIL = "EMAIL@DOMAIN.COM"
    SMTP_HOST = "HOST"
    SMTP_PORT = 587
    SMTP_LOGIN = "LOGIN"
    SMTP_PASSWORD = "PASSWORD"
    AUTOMATIC_ACTIVATIONS = {"@jade-hs.de": "Employee", "@stud.jade-hs.de": "Student"}

file = os.path.join(basedir, "..", 'SMTP_CONFIG.ini')
config = configparser.ConfigParser()
if not os.path.exists(file):
    config['SMTP'] = {'Email': Config.SMTP_EMAIL, 'Host': Config.SMTP_HOST, 'Port': Config.SMTP_PORT, "Login": Config.SMTP_LOGIN, "Password": Config.SMTP_PASSWORD}
    with open(file, 'w') as configfile:
        config.write(configfile)
config.read(file)
Config.SMTP_EMAIL = config['SMTP']['Email']
Config.SMTP_HOST = config['SMTP']['Host']
Config.SMTP_PORT = config['SMTP']['Port']
Config.SMTP_LOGIN = config['SMTP']['Login']
Config.SMTP_PASSWORD = config['SMTP']['Password']
