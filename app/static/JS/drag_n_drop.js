
export function dragElement(elmnt) {
  var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
  if (document.getElementById(elmnt.id + "header")) {
    document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
  } else {
    elmnt.onmousedown = dragMouseDown;
  }

  function dragMouseDown(e) {
    e = e || window.event;
    e.preventDefault();
    pos3 = e.clientX;
    pos4 = e.clientY;
    document.onmouseup = closeDragElement;
    document.onmousemove = elementDrag;
  }

  function elementDrag(e) {
    if (!elmnt.hasOwnProperty("dock") || elmnt.dock  == null) {
      e = e || window.event;
      e.preventDefault();
      pos1 = pos3 - e.clientX;
      pos2 = pos4 - e.clientY;
      pos3 = e.clientX;
      pos4 = e.clientY;
      elmnt.style.top = Math.max(Math.min(elmnt.offsetTop - pos2, window.innerHeight - elmnt.offsetHeight), 0) + "px";
      elmnt.style.left = Math.max(Math.min(elmnt.offsetLeft - pos1, window.innerWidth - elmnt.offsetWidth), 0) + "px";
      }
  }

  function closeDragElement() {
    document.onmouseup = null;
    document.onmousemove = null;
  }
}