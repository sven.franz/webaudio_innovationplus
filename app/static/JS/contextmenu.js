import { closeDropdowns, showWaveform } from "./dropdown.js";
import { buttonZoomIn, buttonZoomOut, buttonZoomVerticalIn, buttonZoomVerticalOut, DataSet, setZoomLevel, setZoomLevelVertical, showListAnnotations, showSpec, showWave, wavesurfer } from "./script.js";
import { hideSpectrogram, showSpectrogram } from "./spectrogram.js";
import { selectRegion, addAnnotation, deleteAnnotation } from "./annotation.js";
import { updateOverlays } from "./processing.js";

// Source: https://www.sitepoint.com/building-custom-right-click-context-menu-javascript/
export var toggleMenuOffHandler = null;

(function () {

  "use strict";

  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  //
  // H E L P E R    F U N C T I O N S
  //
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Function to check if we clicked inside an element with a particular class
   * name.
   * 
   * @param {Object} e The event
   * @param {String} className The class name to check against
   * @return {Boolean}
   */
  function clickInsideElement(e, className) {
    var el = e.srcElement || e.target;

    if (el.classList.contains(className)) {
      return el;
    } else {
      while (el = el.parentNode) {
        if (el.classList && el.classList.contains(className)) {
          return el;
        }
      }
    }

    return false;
  }

  /**
   * Get's exact position of event.
   * 
   * @param {Object} e The event passed in
   * @return {Object} Returns the x and y position
   */
  function getPosition(e) {
    var posx = 0;
    var posy = 0;

    if (!e) var e = window.event;

    if (e.pageX || e.pageY) {
      posx = e.pageX;
      posy = e.pageY;
    } else if (e.clientX || e.clientY) {
      posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
      posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
    }

    return {
      x: posx,
      y: posy
    }
  }

  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  //
  // C O R E    F U N C T I O N S
  //
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Variables.
   */
  var contextMenuClassName = "context-menu";
  var contextMenuItemClassName = "context-menu__item";
  var contextMenuLinkClassName = "context-menu__link";
  var contextMenuActive = "context-menu--active";

  var taskItemClassName = "task";
  var taskItemInContext;

  var clickCoords;
  var clickCoordsX;
  var clickCoordsY;

  var menu = document.querySelector("#context-menu");
  if (menu) {
    var menuItems = menu.querySelectorAll(".context-menu__item");  
  }
  var menuState = 0;
  var menuWidth;
  var menuHeight;
  var menuPosition;
  var menuPositionX;
  var menuPositionY;

  var windowWidth;
  var windowHeight;

  /**
   * Initialise our application's code.
   */
  function init() {
    contextListener();
    clickListener();
    keyupListener();
    resizeListener();
  }

  /**
   * Listens for contextmenu events.
   */
  function contextListener() {
    document.addEventListener("contextmenu", function (e) {
      closeDropdowns();
      taskItemInContext = clickInsideElement(e, taskItemClassName);
      if (taskItemInContext) {
        var nav = document.getElementById("context-menu");
        nav.innerHTML = "";
        var a = null;
        var hr = null;
        if (taskItemInContext.tagName == "REGION" || (taskItemInContext.tagName == "A" && taskItemInContext.region)) {
          a = window.document.createElement('a');
          a.classList.add("context-menu__link");
          a.setAttribute("data-action", "annotation");
          a.innerHTML = "Annotation<span style='float:right'>CTRL+B</span>";
          nav.appendChild(a);
          a = window.document.createElement('a');
          a.classList.add("context-menu__link");
          a.setAttribute("data-action", "play");
          a.text = "Play";
          nav.appendChild(a);
          a = window.document.createElement('a');
          a.classList.add("context-menu__link");
          a.setAttribute("data-action", "playLoop");
          a.text = "Play Loop";
          nav.appendChild(a);
          a = window.document.createElement('a');
          a.classList.add("context-menu__link");
          a.setAttribute("data-action", "delete");
          a.innerHTML = "Delete<span style='float:right'>DEL</span>";
          nav.appendChild(a);
        }
        else if (taskItemInContext.tagName == "DIV" && taskItemInContext.id == "waveform") {
          a = window.document.createElement('a');
          a.classList.add("context-menu__link");
          a.setAttribute("data-action", "zoomin");
          a.innerHTML = "Zoom In<span style='float:right'>+</span>";
          nav.appendChild(a);
          a = window.document.createElement('a');
          a.classList.add("context-menu__link");
          a.setAttribute("data-action", "zoomout");
          a.innerHTML = "Zoom Out<span style='float:right'>-</span>";
          nav.appendChild(a);
          a = window.document.createElement('a');
          a.classList.add("context-menu__link");
          a.setAttribute("data-action", "verticalzoomin");
          a.innerHTML = "Vertical Zoom In<span style='float:right'>SHIFT++</span>";
          nav.appendChild(a);
          a = window.document.createElement('a');
          a.classList.add("context-menu__link");
          a.setAttribute("data-action", "verticalzoomout");
          a.innerHTML = "Vertical Zoom Out<span style='float:right'>SHIFT+-</span>";
          nav.appendChild(a);
          a = window.document.createElement('a');
          a.classList.add("context-menu__link");
          a.setAttribute("data-action", "zoomreset");
          a.text = "Reset Zoom";
          nav.appendChild(a);
          hr = window.document.createElement('hr');
          hr.style.cssText = "margin:5px; border: 1px solid #8F9092;"
          nav.appendChild(hr);
          a = window.document.createElement('a');
          a.classList.add("context-menu__link");
          a.setAttribute("data-action", "showAnnotationList");
          a.text = document.getElementById("showAnnotationList").text;
          nav.appendChild(a);
          hr = window.document.createElement('hr');
          hr.style.cssText = "margin:5px; border: 1px solid #8F9092;"
          nav.appendChild(hr);
          a = window.document.createElement('a');
          a.classList.add("context-menu__link");
          a.setAttribute("data-action", "showWaveform");
          a.text = document.getElementById("showWaveform").text;
          nav.appendChild(a);
          a = window.document.createElement('a');
          a.classList.add("context-menu__link");
          a.setAttribute("data-action", "showSpectrogram");
          a.text = document.getElementById("showSpectrogram").text;
          nav.appendChild(a);
        }
        else if (taskItemInContext.tagName == "A" && taskItemInContext.id == "overlay") {
          a = window.document.createElement('a');
          a.classList.add("context-menu__link");
          a.setAttribute("data-action", "toggleVisibility");
          a.innerHTML = "Toggle Visibility";
          nav.appendChild(a);
          a = window.document.createElement('a');
          a.classList.add("context-menu__link");
          a.setAttribute("data-action", "delete");
          a.innerHTML = "Delete";
          nav.appendChild(a);
        }
        e.preventDefault();
        toggleMenuOn();
        positionMenu(e);
      } else {
        taskItemInContext = null;
        toggleMenuOff();
      }
    });
  }

  /**
   * Listens for click events.
   */
  function clickListener() {
    document.addEventListener("click", function (e) {
      var clickeElIsLink = clickInsideElement(e, contextMenuLinkClassName);

      if (clickeElIsLink) {
        e.stopPropagation();
        e.preventDefault();
        menuItemListener(clickeElIsLink);
      } else {
        var button = e.which || e.button;
        if (button === 1) {
          toggleMenuOff();
        }
      }
      return false;
    });
  }

  /**
   * Listens for keyup events.
   */
  function keyupListener() {
    window.onkeyup = function (e) {
      if (e.keyCode === 27) {
        toggleMenuOff();
      }
    }
  }

  /**
   * Window resize event listener
   */
  function resizeListener() {
    window.onresize = function (e) {
      toggleMenuOff();
    };
  }

  /**
   * Turns the custom context menu on.
   */
  function toggleMenuOn() {
    if (menuState !== 1) {
      menuState = 1;
      menu.classList.add(contextMenuActive);
    }
  }

  /**
   * Turns the custom context menu off.
   */
  function toggleMenuOff() {
    if (menuState !== 0) {
      menuState = 0;
      menu.classList.remove(contextMenuActive);
    }
  }

  /**
   * Positions the menu properly.
   * 
   * @param {Object} e The event
   */
  function positionMenu(e) {
    clickCoords = getPosition(e);
    clickCoordsX = clickCoords.x;
    clickCoordsY = clickCoords.y;

    menuWidth = menu.offsetWidth + 4;
    menuHeight = menu.offsetHeight + 4;

    windowWidth = window.innerWidth;
    windowHeight = window.innerHeight;

    if ((windowWidth - clickCoordsX) < menuWidth) {
      menu.style.left = windowWidth - menuWidth + "px";
    } else {
      menu.style.left = clickCoordsX + "px";
    }

    if ((windowHeight - clickCoordsY) < menuHeight) {
      menu.style.top = windowHeight - menuHeight + "px";
    } else {
      menu.style.top = clickCoordsY + "px";
    }
  }

  /**
   * Dummy action function that logs an action when a menu item link is clicked
   * 
   * @param {HTMLElement} link The link that was clicked
   */
  async function menuItemListener(link) {
    if (taskItemInContext.tagName == "REGION" || (taskItemInContext.tagName == "A" && taskItemInContext.region)) {
      var region = null;
      if (taskItemInContext.tagName == "A") {
        region = taskItemInContext.region;
        selectRegion(region);
      }
      else
        region = wavesurfer.regions.list[taskItemInContext.getAttribute("data-id")];
      if (link.getAttribute("data-action") == "annotation")
        addAnnotation(region);
      else if (link.getAttribute("data-action") == "play")
        region.play();
      else if (link.getAttribute("data-action") == "playLoop")
        region.playLoop();
      else if (link.getAttribute("data-action") == "delete")
        await deleteAnnotation(region);
    }
    else if (taskItemInContext.tagName == "DIV" && taskItemInContext.id == "waveform") {
      if (link.getAttribute("data-action") == "zoomin")
        buttonZoomIn.click();
      else if (link.getAttribute("data-action") == "zoomout")
        buttonZoomOut.click();
      else if (link.getAttribute("data-action") == "verticalzoomin")
        buttonZoomVerticalIn.click();
      else if (link.getAttribute("data-action") == "verticalzoomout")
        buttonZoomVerticalOut.click();
      else if (link.getAttribute("data-action") == "zoomreset") {
        setZoomLevel(1);
        setZoomLevelVertical(1);
        wavesurfer.zoom(1);
        buttonZoomVerticalOut.click();
      }
      else if (link.getAttribute("data-action") == "showAnnotationList") {
        document.getElementById("showAnnotationList").onclick();
      }
      else if (link.getAttribute("data-action") == "showWaveform")
        showWaveform(!showWave);
      else if (link.getAttribute("data-action") == "showSpectrogram") {
        if (showSpec)
          hideSpectrogram();
        else
          showSpectrogram();
      }
    }
    else if (taskItemInContext.tagName == "A" && taskItemInContext.id == "overlay") {
      if (link.getAttribute("data-action") == "toggleVisibility")
        taskItemInContext.dispatchEvent(new MouseEvent("dblclick"));
      else if (link.getAttribute("data-action") == "delete") {
        if (DataSet.Overlays.hasOwnProperty(taskItemInContext.innerHTML)) {
          delete DataSet.Overlays[taskItemInContext.innerHTML];
          wavesurfer.drawBuffer();
          await updateOverlays();
        }
      }
    }
    toggleMenuOff();
  }

  /**
   * Run the app.
   */
  init();
  toggleMenuOffHandler = toggleMenuOff;

})();