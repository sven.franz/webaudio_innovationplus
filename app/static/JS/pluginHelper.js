import { createPopUpWindow } from "./plugins.js";

(function () {
    'use strict';

    function popUp(title, modal = true, backgroundColor = "#ffffff") {
        var id = "PopUpWindow_" + title
        var popup = createPopUpWindow(id, title, modal, false);
        popup.setEnable(true);
        var content = document.getElementById(id + "_content");
        content.style.backgroundColor = backgroundColor;
        var controls = document.getElementById(id + "_controls");
        controls.style.display = "none";
        return content;
    };

    function blobToImage(data, width = "100%", height = "100%", type = "image/svg+xml") {
        var arrayBufferView = new Uint8Array(data)
        var blob = new Blob( [ arrayBufferView ], { type: type } );
        var urlCreator = window.URL || window.webkitURL;
        var imageUrl = urlCreator.createObjectURL( blob );
        var img = window.document.createElement('img');
        img.style.width = width;
        img.style.height =height;
        img.src = imageUrl;
        return img;
    }

    function arrayToDygraph(data, xValues = null) {
        var returnData = []
        for (var idx = 0; idx < data[0].length; idx++){
            var row = [idx];
            if (xValues && Array.isArray(xValues) && xValues.length > idx)
                row[0] = xValues[idx]
            for (var column = 0; column < data.length; column++)
                row.push(data[column][idx]);
            returnData.push(row)
        }
        return returnData;
    }

    class TimePoint {
        constructor(yValue, channel, color) {
            this.value = yValue;
            this.channel = channel;
            this.color = color;
        }
    }

    class FrequencyPoint {
        constructor(frequency, channel, color) {
            this.value = frequency;
            this.channel = channel;
            this.color = color;
        }
    }

    class Annotation {
        constructor(value, start, end) {
            this.id = "";
            this.value = value;
            this.start = start;
            this.end = end;
        }
    }

    if (typeof exports === 'object' && typeof module !== 'undefined') {
        module.exports = popUp;
        module.exports = blobToImage;
        module.exports = arrayToDygraph;
        module.exports = TimePoint;
        module.exports = FrequencyPoint;
        module.exports = Annotation;
    }
    else if (typeof define === 'function' && define.amd) {
        define(popUp);
        define(blobToImage);
        define(arrayToDygraph);
        define(TimePoint);
        define(FrequencyPoint);
        define(Annotation);
    }
    else {
        window.popUp = popUp;
        window.blobToImage = blobToImage;
        window.arrayToDygraph = arrayToDygraph;
        window.TimePoint = TimePoint;
        window.FrequencyPoint = FrequencyPoint;
        window.Annotation = Annotation;
    }
}());