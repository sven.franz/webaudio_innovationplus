class StreamProcessor extends AudioWorkletProcessor {

  constructor(options) {
    super()
    this.alpha = .01;
    this.Gains = [1, 1];
    this.RMS = [0, 0];
    this.OutBuffer = [];
    this.InBuffer = [];
    this.blockLen = 1024;
    this.transferBlocklen = 5 * this.blockLen;
    this.lastTime = Date.now();

    this.port.onmessage = async (message) => {
      if (message.data.hasOwnProperty("Init")) {
        this.RMS = [0, 0];
        this.OutBuffer = [];
        this.InBuffer = [];
      }
      if (message.data.hasOwnProperty("LeftGain"))
        this.Gains[0] = message.data["LeftGain"];
      if (message.data.hasOwnProperty("RightGain"))
        this.Gains[1] = message.data["RightGain"];
      // process received data
      if (message.data.hasOwnProperty("data") && message.data["data"] && message.data["data"].length && message.data["data"][0]) {
        var blockLen = 0;
        // get blocklen from incomming data
        if (message.data["data"] instanceof ArrayBuffer)
          blockLen = (message.data["data"].byteLength / 4) / this.InBuffer.length;
        else
          blockLen = message.data["data"][0].length;
        //console.log("R", blockLen)
        this.lastTime = Date.now();
        // Fallback if data can't be process correctly (???)
        // copy incomming data to InBuffer
        for (var channel = 0; channel < Math.min(2, this.InBuffer.length); channel++) {
          if (message.data["data"] instanceof ArrayBuffer)
            this.InBuffer[channel] = this.Float32Concat(this.InBuffer[channel], new Float32Array(message.data["data"].slice(channel * blockLen * 4, (channel + 1) * blockLen * 4)));
          else
            this.InBuffer[channel] = this.Float32Concat(this.InBuffer[channel], message.data["data"][channel]);
        }
      }
    }
  }

  mod(n, m) {
    return ((n % m) + m) % m;
  }

  Float32Concat(first, second) {
    if (second) {
      var firstLength = first.length, result = new Float32Array(firstLength + second.length);
      result.set(first);
      result.set(second, firstLength);
    }
    else if (first)
      return first;
    else 
      return new Float32Array(0);
    return result;
  }

  process(inputs, outputs) {
    if (inputs[0].length && inputs[0][0].length) {
      var sendBuffer = [];
      // initialize OutBuffer and InBuffer
      if (this.OutBuffer.length == 0 || this.InBuffer.length == 0) {
        this.OutBuffer = [];
        this.InBuffer = [];
        for (var channel = 0; channel < Math.min(2, inputs[0].length); channel++) {
          this.OutBuffer.push([]);
          this.InBuffer.push(new Float32Array(this.transferBlocklen));
        }
      }
      // store input samples in this.OutBuffer
      for (var channel = 0; channel < Math.min(2, inputs[0].length); channel++) {
        this.OutBuffer[channel] = this.Float32Concat(this.OutBuffer[channel], inputs[0][channel]);
        // push outbuffer into sendBuffer
        if (this.OutBuffer[channel].length >= this.transferBlocklen) {
          sendBuffer.push(this.OutBuffer[channel].slice(0, this.transferBlocklen));
          this.OutBuffer[channel] = this.OutBuffer[channel].slice(this.transferBlocklen)
        }
        // copy InBuffer to outputs (will be played)
        // calculate RMS and mute channel if necessary
        //console.log(this.InBuffer[channel].length, outputs[0][channel].length)
        var RMS = 0;
        if (this.InBuffer[channel].length >= this.blockLen) {
          if (this.Gains[channel]) {
            outputs[0][channel].set(this.InBuffer[channel].slice(0, outputs[0][channel].length), 0);
            for (var idx = 0; idx < outputs[0][channel].length; idx++)
              RMS += outputs[0][channel][idx] ** 2;
          }
          this.InBuffer[channel] = this.InBuffer[channel].slice(outputs[0][channel].length)
        }
        else {
          //console.log(this.InBuffer[channel].length)
        }
        // smooth RMS
        this.RMS[channel] = +((1 - this.alpha) * this.RMS[channel] + this.alpha * Math.sqrt(RMS / outputs[0][channel].length)) || 0;
      }
      // send data, if sendBuffer is filled
      if (sendBuffer.length) {
        //console.log("S", sendBuffer[0].length)
        this.port.postMessage({ data: sendBuffer, delayInSamples: parseInt(this.transferBlocklen)});
      }
      this.port.postMessage({ RMS: this.RMS });
    }
    return true;
  }
}

registerProcessor('StreamProcessor', StreamProcessor)