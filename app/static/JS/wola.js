import * as FFT from "./fft/fft.js"
import { DataSet, wavesurfer } from "./script.js";

export var executePreprocess = false;
export function setExecutePreprocess(value) {
    executePreprocess = value;
}

export async function wola(plugins, data, plugIdx, PluginParams, withoutSubPlugins = false) {

    function multvector(a, b) {
        return a.map((e, i) => e * b[i]);
    }
    function sumvector(a, b) {
        return a.map((e, i) => e + b[i]);
    }
    function Float32Concat(first, second) {
        var firstLength = first.length, result = new Float32Array(firstLength + second.length);
        result.set(first);
        result.set(second, firstLength);
        return result;
    }
    function flattenDeep(arr1) {
        if (Array.isArray(arr1))
            return arr1.reduce((acc, val) => Array.isArray(val) ? acc.concat(flattenDeep(val)) : acc.concat(val), []);
        else
            return [arr1];
    }
    var dataOut = [];
    var plugIdxStart = plugIdx.val;
    var channel = 0;
    var blocklen = Number(PluginParams[plugins[plugIdxStart].id]["params"].blockLen);
    if (PluginParams[plugins[plugIdx.val].id]["params"].domain == "Domain.Frequency")
        PluginParams[plugins[plugIdxStart].id]["params"].timeDomainWithoutOverlap = false;
    // create hann-windows (windowIn and windowOut) at initialization
    if (!plugins[plugIdxStart].hasOwnProperty("windowIn") || plugins[plugIdxStart].windowIn.length != blocklen) {
        plugins[plugIdxStart].windowIn = new Float32Array(blocklen);
        plugins[plugIdxStart].windowOut = new Float32Array(blocklen);
        for (var idx = 0; idx < blocklen; idx++) {
            plugins[plugIdxStart].windowOut[idx] = 0.5 * (1 - Math.cos(Math.PI * 2 * idx / (blocklen - 1)));
            if (PluginParams[plugins[plugIdx.val].id]["params"].domain == "Domain.Frequency") {
                plugins[plugIdxStart].windowIn[idx] = Math.sqrt(plugins[plugIdxStart].windowOut[idx]);
                plugins[plugIdxStart].windowOut[idx] = plugins[plugIdxStart].windowIn[idx];
            }
            else {
                plugins[plugIdxStart].windowIn[idx] = 1;
                if (PluginParams[plugins[plugIdx.val].id]["params"].timeDomainWithoutOverlap == true)
                    plugins[plugIdxStart].windowOut[idx] = 1;
            }
        }
    }
    // create buffers as empty arrays at initialization
    if (!plugins[plugIdxStart].hasOwnProperty("lastBlockIn")) {
        plugins[plugIdxStart].lastBlockIn = [];
        plugins[plugIdxStart].lastBlockOut = [];
    }
    if (!plugins[plugIdxStart].hasOwnProperty("block") || (plugins[plugIdxStart].block.length && plugins[plugIdxStart].block[0].length != blocklen))
        plugins[plugIdxStart].block = [];
    // fill buffers for each channel
    for (channel = 0; channel < data.length; channel++) {
        if (plugins[plugIdxStart].lastBlockIn.length <= channel) {
            plugins[plugIdxStart].lastBlockIn.push(new Float32Array(blocklen / (PluginParams[plugins[plugIdxStart].id]["params"].timeDomainWithoutOverlap ? 1 : 2)))
            if (!PluginParams[plugins[plugIdxStart].id]["params"].timeDomainWithoutOverlap)
                plugins[plugIdxStart].lastBlockOut.push(new Float32Array(blocklen / 2))
        }
        if (plugins[plugIdxStart].block.length <= channel)
            plugins[plugIdxStart].block.push(new Float32Array(blocklen));
        if (data[channel].length < blocklen)
            data[channel] = Float32Concat(data[channel], new Float32Array(blocklen - data[channel].length));
        dataOut.push(new Float32Array(data[channel].length));
        if (!PluginParams[plugins[plugIdxStart].id]["params"].timeDomainWithoutOverlap)
            dataOut[channel].set(plugins[plugIdxStart].lastBlockOut[channel], 0)
    }
    // create fft buffers if Domain == Frequency
    var fft = null;
    if (Number(PluginParams[plugins[plugIdxStart].id]["params"].fftSize) != blocklen)
    PluginParams[plugins[plugIdxStart].id]["params"].fftSize = blocklen
    if (PluginParams[plugins[plugIdxStart].id]["params"].domain == "Domain.Frequency") {
        fft = new FFT.FFT(Number(PluginParams[plugins[plugIdxStart].id]["params"].fftSize));
        if (!plugins[plugIdxStart].hasOwnProperty("fftBlock")) {
            plugins[plugIdxStart].spec = [];
            plugins[plugIdxStart].fftBlock = [];
            plugins[plugIdxStart].fftOut = fft.createComplexArray();
            for (channel = 0; channel < data.length; channel++)
                plugins[plugIdxStart].fftBlock[channel] = fft.createComplexArray()
        }
    }
    // process all blocks
    var idx = 0;
    if (data.length) {
        while (idx < data[0].length) {
            for (channel = 0; channel < data.length; channel++) {
                if (!PluginParams[plugins[plugIdxStart].id]["params"].timeDomainWithoutOverlap) {
                    var inBlock = new Float32Array(blocklen / 2);
                    inBlock.set(data[channel].slice(idx, idx + blocklen / 2), 0)
                    plugins[plugIdxStart].block[channel].set(multvector(plugins[plugIdxStart].windowIn, Float32Concat(plugins[plugIdxStart].lastBlockIn[channel], inBlock)), 0);
                    plugins[plugIdxStart].lastBlockIn[channel].set(data[channel].slice(idx, idx + blocklen / 2), 0)
                }
                else
                    plugins[plugIdxStart].block[channel].set(multvector(plugins[plugIdxStart].windowIn, data[channel].slice(idx, idx + blocklen)), 0);
                if (PluginParams[plugins[plugIdxStart].id]["params"].domain == "Domain.Frequency") {
                    fft.realTransform(plugins[plugIdxStart].fftBlock[channel], plugins[plugIdxStart].block[channel])
                }
            }
            plugIdx.val = plugIdxStart + 1;
            plugIdx.val = plugIdxStart;
            if (PluginParams[plugins[plugIdxStart].id]["params"].domain == "Domain.Frequency")
                Interleaved2Complex(plugins[plugIdxStart].fftBlock, plugins[plugIdxStart].spec);
            do {
                if (plugins[plugIdx.val].hasOwnProperty("pluginJSObject") && plugins[plugIdx.val].pluginJSObject) {
                    var returnValue = {};                
                    if (executePreprocess && plugins[plugIdx.val].pluginJSObject.preprocess) {
                        plugins[plugIdx.val].pluginJSObject.preprocess(PluginParams[plugins[plugIdx.val].id]["params"], document.getElementById(plugins[plugIdx.val].id + "_content"));
                    }
                    if (plugins[plugIdx.val].pluginJSObject.process) {
                        if (PluginParams[plugins[plugIdxStart].id]["params"].domain == "Domain.Frequency")
                            returnValue = plugins[plugIdx.val].pluginJSObject.process(plugins[plugIdxStart].spec, PluginParams[plugins[plugIdx.val].id]["params"], document.getElementById(plugins[plugIdx.val].id + "_content"));
                        else
                            returnValue = plugins[plugIdx.val].pluginJSObject.process(plugins[plugIdxStart].block, PluginParams[plugins[plugIdx.val].id]["params"], document.getElementById(plugins[plugIdx.val].id + "_content"));
                    }
                    if (typeof (returnValue) === "object" && returnValue.length > 0) {
                        var init = true;
                        for (const itemList of returnValue) {
                            if (returnValue.length && returnValue[0].constructor.name != "TimePoint" && returnValue[0].constructor.name != "FrequencyPoint" && returnValue[0].constructor.name != "Annotation") {
                                if (init) {
                                    if (PluginParams[plugins[plugIdxStart].id]["params"].domain == "Domain.Frequency")
                                        plugins[plugIdxStart].spec = [];
                                    else
                                        plugins[plugIdxStart].block = [];
                                    init = false;
                                }
                                if (PluginParams[plugins[plugIdxStart].id]["params"].domain == "Domain.Frequency")
                                    plugins[plugIdxStart].spec.push(itemList);
                                else
                                    plugins[plugIdxStart].block.push(itemList);
                            }
                            else {
                                for (var item of flattenDeep(itemList)) {
                                    if (typeof (item) === "object" && (item.constructor.name == "TimePoint" || item.constructor.name == "FrequencyPoint")) {
                                        if (!DataSet.Overlays.hasOwnProperty(plugins[plugIdx.val].id))
                                            DataSet.Overlays[plugins[plugIdx.val].id] = { values: [[]], visible: true, domain: (item.constructor.name == "TimePoint" ? "time" : "frequency") };
                                        if (DataSet.Overlays[plugins[plugIdx.val].id].values.slice(-1)[0].length == wavesurfer.backend.buffer.numberOfChannels)
                                            DataSet.Overlays[plugins[plugIdx.val].id].values.push([]);
                                        DataSet.Overlays[plugins[plugIdx.val].id].values.slice(-1)[0].push([item.value, item.color]);
                                    }
                                    else if (typeof (item) === "object" && item.constructor.name == "Annotation")
                                        await addImportedAnnotation(item);
                                }
                            }
                        }
                    }
                }
                plugIdx.val++;
            }
            while (!withoutSubPlugins && (plugIdx.val < plugins.length && PluginParams[plugins[plugIdx.val].id] && PluginParams[plugins[plugIdx.val].id]["params"].domain == PluginParams[plugins[plugIdx.val - 1].id]["params"].domain && Number(PluginParams[plugins[plugIdx.val].id]["params"].fftSize) == Number(PluginParams[plugins[plugIdx.val - 1].id]["params"].fftSize) && Number(PluginParams[plugins[plugIdx.val].id]["params"].blockLen) == Number(PluginParams[plugins[plugIdx.val - 1].id]["params"].blockLen) && PluginParams[plugins[plugIdx.val].id]["params"].timeDomainWithoutOverlap == PluginParams[plugins[plugIdx.val - 1].id]["params"].timeDomainWithoutOverlap))
            await new Promise(resolve => setTimeout(resolve, 0));
            executePreprocess = false;
            if (PluginParams[plugins[plugIdxStart].id]["params"].domain == "Domain.Frequency")
                Complex2Interleaved(plugins[plugIdxStart].spec, plugins[plugIdxStart].fftBlock);
            for (channel = 0; channel < data.length; channel++) {
                if (PluginParams[plugins[plugIdxStart].id]["params"].domain == "Domain.Frequency") {
                    fft.completeSpectrum(plugins[plugIdxStart].fftBlock[channel]);
                    fft.inverseTransform(plugins[plugIdxStart].fftOut, plugins[plugIdxStart].fftBlock[channel]);
                    plugins[plugIdxStart].block[channel].set(new Float32Array(plugins[plugIdxStart].fftOut.filter(function (value, index, Arr) {
                        return index % 2 == 0;
                    })), 0);
                }
                plugins[plugIdxStart].lastBlockOut[channel] = multvector(plugins[plugIdxStart].windowOut, plugins[plugIdxStart].block[channel])
                dataOut[channel].set(sumvector(dataOut[channel].slice(idx, idx + blocklen), plugins[plugIdxStart].lastBlockOut[channel]), idx);
                if (!PluginParams[plugins[plugIdxStart].id]["params"].timeDomainWithoutOverlap)
                    plugins[plugIdxStart].lastBlockOut[channel] = plugins[plugIdxStart].lastBlockOut[channel].slice(blocklen/2);
            }
            idx += parseInt(blocklen / (PluginParams[plugins[plugIdxStart].id]["params"].timeDomainWithoutOverlap ? 1 : 2))
        }    
    }
    return dataOut;
}

function Interleaved2Complex(data, spec) {
    if (!spec)
        spec = [];
    for (var channel = 0; channel < data.length; channel++) {
        if (!spec[channel] || spec[channel].length != data[channel].length / 4 + 1)
            spec[channel] = []
        for (var idx = 0; idx < data[channel].length / 2 + 1; idx += 2) {
            spec[channel][idx / 2] = Complex(data[channel][idx], data[channel][idx + 1]);
        }
    }
    return spec;
}

function Complex2Interleaved(spec, data) {
    if (!data)
        data = [];
    for (var channel = 0; channel < spec.length; channel++) {
        if (!data[channel] || data[channel].length > spec[channel].length * 4)
            data[channel] = []
        for (var idx = 0; idx < spec[channel].length; idx++) {
            data[channel][2 * idx] = spec[channel][idx].re;
            data[channel][2 * idx + 1] = spec[channel][idx].im;
        }
    }
    return data;
}


function convertParams2Sendable(obj) {
    for (var key in obj)
    {
        if (obj[key].buffer) {
            var buffer = "";
            for (var item in new Uint8Array(obj[key].buffer))
                buffer += String.fromCharCode(item)
            obj[key] = buffer;
        }
        else if (typeof obj[key] == "object" && obj[key] !== null)
            obj[key] = convertParams2Sendable(obj[key]);
    }
    return obj;
}

window.setExecutePreprocess = setExecutePreprocess;
window.convertParams2Sendable = convertParams2Sendable;
window.wola = wola;
