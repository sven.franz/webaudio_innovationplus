import { wavesurfer, setRefreshSpetrogram, setAudioSignalHasChanges, SystemPreferences, DataSet } from "./script.js";
import { addImportedAnnotation, selectRegion, deleteAnnotation, selectedRegion } from "./annotation.js"
import { fitWindowSize } from "./dropdown.js";
import { addToHistory, HistoryType } from "./history.js";
import { createPopUpWindow } from "./plugins.js";
import { setOverlays } from "./processing.js";

export var memoryBuffer = null;
var CrossFadeSamples = 1024;
export function setMemoryBuffer(value) { memoryBuffer = value; }
export function pushToMemoryBuffer(value) { memoryBuffer.push(value); }
export function AppendToMemoryBuffer(value) {
    if (!memoryBuffer)
        memoryBuffer = [];
    for (var channel = 0; channel < value.length; channel++) {
        if (memoryBuffer.length < value.length)
            memoryBuffer.push(new Float32Array());
        memoryBuffer[channel] = new Float32Array([...memoryBuffer[channel], ...value[channel]]);
    }
}

export async function pasteAudioData(forcePaste = false, addHistory = true, modifieGui = true) {
    if (modifieGui)
        document.getElementById("btnPasteAudioData").blur();
    if (memoryBuffer && memoryBuffer.length > 0 && memoryBuffer[0].length > 0 && (forcePaste || document.getElementById("hideWavesurfer").style.visibility != "visible")) {
        wavesurfer.pause();
        if (addHistory)
            await addToHistory(HistoryType.annotationAndAudiodata, "Paste Audiodata");
        if (modifieGui) {
            document.getElementById("hideWavesurfer").style.visibility = "visible";
            await new Promise(resolve => setTimeout(resolve, 0));
            document.querySelector('[data-action="paste"]').blur();
        }
        setOverlays({});
        var start = wavesurfer.getCurrentTime();
        var newBuffer1 = [];
        var newBuffer2 = [];
        for (var channel = 0; channel < wavesurfer.backend.buffer.numberOfChannels; channel++) {
            newBuffer1.push(new Float32Array(Math.floor(start * wavesurfer.backend.buffer.sampleRate)));
            newBuffer2.push(new Float32Array(Math.floor((wavesurfer.backend.buffer.duration - start) * wavesurfer.backend.buffer.sampleRate)));
            wavesurfer.backend.buffer.copyFromChannel(newBuffer1[channel], channel, 0);
            wavesurfer.backend.buffer.copyFromChannel(newBuffer2[channel], channel, start * wavesurfer.backend.buffer.sampleRate);
            for (var idx = 0; idx < CrossFadeSamples; idx++) {
                if (idx < memoryBuffer[channel].length && idx < newBuffer2[channel].length) {
                    memoryBuffer[channel][Math.min(memoryBuffer[channel].length, Math.max(0, idx))] = (idx / CrossFadeSamples) * memoryBuffer[channel][Math.min(memoryBuffer[channel].length, Math.max(0, idx))];
                    memoryBuffer[channel][Math.min(memoryBuffer[channel].length, Math.max(0, idx))] += (1 - (idx / CrossFadeSamples)) * newBuffer2[channel][Math.min(newBuffer2[channel].length, Math.max(0, idx))]
                }
                if (memoryBuffer[channel].length - CrossFadeSamples + idx >= 0 && newBuffer1[channel].length - CrossFadeSamples + idx >= 0) {
                    memoryBuffer[channel][Math.min(memoryBuffer[channel].length, Math.max(0, memoryBuffer[channel].length - CrossFadeSamples + idx))] = (1 - (idx / CrossFadeSamples)) * memoryBuffer[channel][Math.min(memoryBuffer[channel].length, Math.max(0, memoryBuffer[channel].length - idx))];
                    memoryBuffer[channel][Math.min(memoryBuffer[channel].length, Math.max(0, memoryBuffer[channel].length - CrossFadeSamples + idx))] += (idx / CrossFadeSamples) * newBuffer1[channel][Math.min(newBuffer1[channel].length, Math.max(0, newBuffer1[channel].length - CrossFadeSamples + idx))]
                }
            }
        }
        wavesurfer.backend.buffer = wavesurfer.backend.ac.createBuffer(
            newBuffer1.length,
            (newBuffer1[0].length + memoryBuffer[0].length + newBuffer2[0].length),
            wavesurfer.backend.buffer.sampleRate
        );
        for (var channel = 0; channel < wavesurfer.backend.buffer.numberOfChannels; channel++) {
            wavesurfer.backend.buffer.copyToChannel(newBuffer1[channel], channel, 0);
            wavesurfer.backend.buffer.copyToChannel(memoryBuffer[channel], channel, start * wavesurfer.backend.buffer.sampleRate);
            wavesurfer.backend.buffer.copyToChannel(newBuffer2[channel], channel, start * wavesurfer.backend.buffer.sampleRate + memoryBuffer[channel].length);
        }
        Object.keys(wavesurfer.regions.list).forEach(element => {
            var hasChanges = false;
            if (wavesurfer.regions.list[element].start > start) {
                wavesurfer.regions.list[element].start += memoryBuffer[0].length / wavesurfer.backend.buffer.sampleRate;
                wavesurfer.regions.list[element].end += memoryBuffer[0].length / wavesurfer.backend.buffer.sampleRate;
                hasChanges = true;
            }
            if (hasChanges) {
                wavesurfer.regions.list[element].hasChanges = true;
                if (wavesurfer.regions.list[element].marker) {
                    wavesurfer.regions.list[element].marker.time = wavesurfer.regions.list[element].start;
                    wavesurfer.regions.list[element].marker.endTime = wavesurfer.regions.list[element].end;
                }
            }
        });
        setAudioSignalHasChanges(DataSet.audioSignalHasChanges);
        wavesurfer.markers._updateMarkerPositions();
        var item = {}
        item.start = new Date(start * 1000).toISOString().substr(11, 12);
        item.end = new Date((start + memoryBuffer[0].length / wavesurfer.backend.buffer.sampleRate) * 1000).toISOString().substr(11, 12);
        var region = await addImportedAnnotation(item);
        selectRegion(region);
        setAudioSignalHasChanges(true);
        if (modifieGui) {
            document.getElementById("hideWavesurfer").style.visibility = "hidden";
        }
        //wavesurfer.empty();
        wavesurfer.drawBuffer();
        setRefreshSpetrogram(true);
        await fitWindowSize();
    }
}

export async function cutCopyAudioData(cut = false, addHistory = true, fordeAction = false, modifieGui = true) {
    if (modifieGui) {
        document.getElementById("btnCopyAudioData").blur();
        document.getElementById("btnCutAudioData").blur();
    }
    if (selectedRegion && (fordeAction || document.getElementById("hideWavesurfer").style.visibility != "visible")) {
        if (modifieGui) {
            document.getElementById("hideWavesurfer").style.visibility = "visible";
            await new Promise(resolve => setTimeout(resolve, 0));
            document.querySelector('[data-action="cut"]').blur();
            document.querySelector('[data-action="copy"]').blur();
        }
        if (cut && addHistory)
            await addToHistory(HistoryType.annotationAndAudiodata, "Cut Audiodata");
        var start = selectedRegion.start;
        var end = selectedRegion.end;
        memoryBuffer = [];
        var newBuffer1 = [];
        var newBuffer2 = [];
        for (var channel = 0; channel < wavesurfer.backend.buffer.numberOfChannels; channel++) {
            memoryBuffer.push(new Float32Array(Math.floor((end - start) * wavesurfer.backend.buffer.sampleRate)));
            wavesurfer.backend.buffer.copyFromChannel(memoryBuffer[channel], channel, start * wavesurfer.backend.buffer.sampleRate)
            if (cut) {
                setOverlays({});
                newBuffer1.push(new Float32Array(Math.min(wavesurfer.backend.buffer.length, Math.max(0, Math.floor(start * wavesurfer.backend.buffer.sampleRate)))));
                newBuffer2.push(new Float32Array(Math.min(wavesurfer.backend.buffer.length, Math.max(0, Math.floor((wavesurfer.backend.buffer.duration - end) * wavesurfer.backend.buffer.sampleRate)))));
                wavesurfer.backend.buffer.copyFromChannel(newBuffer1[channel], channel, 0);
                wavesurfer.backend.buffer.copyFromChannel(newBuffer2[channel], channel, end * wavesurfer.backend.buffer.sampleRate);
                var tempOriginalChannel = wavesurfer.backend.buffer.getChannelData(channel)
                for (var idx = 0; idx < CrossFadeSamples / 2; idx++) {
                    if (newBuffer1[channel].length + (idx - CrossFadeSamples / 2) >= 0 && Math.floor(end * wavesurfer.backend.buffer.sampleRate) + (idx - CrossFadeSamples / 2) >= 0) {
                        newBuffer1[channel][Math.min(newBuffer1[channel].length, Math.max(0, newBuffer1[channel].length + (idx - CrossFadeSamples / 2)))] *= 1 - (idx / CrossFadeSamples);
                        newBuffer1[channel][Math.min(newBuffer1[channel].length, Math.max(0, newBuffer1[channel].length + (idx - CrossFadeSamples / 2)))] += (idx / CrossFadeSamples) * tempOriginalChannel[Math.min(tempOriginalChannel.length, Math.max(0, Math.floor(end * wavesurfer.backend.buffer.sampleRate) + (idx - CrossFadeSamples / 2)))];
                    }
                    if (idx < newBuffer2[channel].length && Math.floor(start * wavesurfer.backend.buffer.sampleRate) + idx < tempOriginalChannel.length) {
                        newBuffer2[channel][Math.min(newBuffer2[channel].length, Math.max(0, idx))] *= 1 - ((CrossFadeSamples / 2 - idx) / CrossFadeSamples);
                        newBuffer2[channel][Math.min(newBuffer2[channel].length, Math.max(0, idx))] += ((CrossFadeSamples / 2 - idx) / CrossFadeSamples) * tempOriginalChannel[Math.min(tempOriginalChannel.length, Math.max(0, Math.floor(start * wavesurfer.backend.buffer.sampleRate) + idx))];
                    }
                }
            }
        }
        if (cut) {
            await deleteAnnotation(selectedRegion, false);
            wavesurfer.backend.buffer = wavesurfer.backend.ac.createBuffer(
                newBuffer1.length,
                Math.max(1, (newBuffer1[0].length + newBuffer2[0].length)),
                wavesurfer.backend.buffer.sampleRate
            );
            for (var channel = 0; channel < wavesurfer.backend.buffer.numberOfChannels; channel++) {
                wavesurfer.backend.buffer.copyToChannel(newBuffer1[channel], channel, 0);
                wavesurfer.backend.buffer.copyToChannel(newBuffer2[channel], channel, start * wavesurfer.backend.buffer.sampleRate);
            }
            Object.keys(wavesurfer.regions.list).forEach(async element => {
                var hasChanges = false;
                if (wavesurfer.regions.list[element].start >= end) {
                    wavesurfer.regions.list[element].start -= (end - start);
                    hasChanges = true;
                }
                else if (wavesurfer.regions.list[element].start > start) {
                    wavesurfer.regions.list[element].start = start;
                    hasChanges = true;
                }
                if (wavesurfer.regions.list[element].end > start) {
                    wavesurfer.regions.list[element].end = Math.max(start, wavesurfer.regions.list[element].end - (end - start));
                    hasChanges = true;
                }
                wavesurfer.regions.list[element].marker.time = wavesurfer.regions.list[element].start;
                wavesurfer.regions.list[element].marker.endTime = wavesurfer.regions.list[element].end;
                if (wavesurfer.regions.list[element].end - wavesurfer.regions.list[element].start <= 0) {
                    await deleteAnnotation(wavesurfer.regions.list[element], false);
                }
                else if (hasChanges) {
                    wavesurfer.regions.list[element].hasChanges = true;
                    if (wavesurfer.regions.list[element].marker)
                        wavesurfer.regions.list[element].marker.time = wavesurfer.regions.list[element].start;
                }
            });
            setAudioSignalHasChanges(DataSet.audioSignalHasChanges);
            wavesurfer.markers._updateMarkerPositions();
            //wavesurfer.empty();
            wavesurfer.drawBuffer();
            setRefreshSpetrogram(true);
            await fitWindowSize();
            wavesurfer.seekAndCenter(Math.min(1, Math.max(0, start / wavesurfer.backend.buffer.duration)));
            setAudioSignalHasChanges(true);
        }
        if (modifieGui) {
            document.querySelector('[data-action="paste"]').style.color = "white";
            document.getElementById("hideWavesurfer").style.visibility = "hidden";
        }
    }
}

export async function recordFromMicrofone() {
    wavesurfer.pause();
    var mediaRecorder = null;
    var audioChunks = [];
    var startTime = 0;
    var blnIsRecorded = false;
    var btnIsPlaying = false;
    var windowName = "Recorder";
    createPopUpWindow(windowName, windowName, true);
    var divWindow = document.getElementById(windowName);
    var divContent = document.getElementById(windowName + "_content");
    var divControls = document.getElementById(windowName + "_controls");
    divWindow.onclose = function (id) {
        if (document.getElementById(id))
            document.body.removeChild(document.getElementById(id));
        document.getElementById("hideWavesurfer").style.visibility = "hidden"
        wavesurferRecord.microphone.stop();
        wavesurferRecord.microphone.destroy();
        wavesurferRecord = null;
        btnRcord.style.borderStyle = 'outset';
        btnRcord.style.color = 'white';
    }

    var divTimeinfo = window.document.createElement('div');
    divTimeinfo.className = 'Timeinfo';
    divControls.appendChild(divTimeinfo);

    var div = window.document.createElement('div');
    div.id = "RecordTime";
    div.innerHTML = "00:00:00.000";
    div.style.flex = 1;
    div.className = 'Timeinfo';
    divTimeinfo.appendChild(div);

    var divControlButtons = window.document.createElement('div');
    divControlButtons.className = 'controls';
    divControlButtons.style.float = "right";
    divControls.className = "controlsArea";
    divControls.appendChild(divControlButtons);

    var btn = window.document.createElement('button');
    btn.id = "btnRecordingStart";
    btn.title = "Record (STRG+R)"
    btn.innerHTML = "<i class='glyphicon glyphicon-record'></i>";
    btn.className = 'btn btn-primary';
    divControlButtons.appendChild(btn);

    btn.addEventListener("click", function (e) {
        this.blur();
        btnIsPlaying = !btnIsPlaying;
        if (btnIsPlaying) {
            this.style.borderStyle = 'inset';
            this.style.color = 'black';
            this.innerHTML = "<i class='glyphicon glyphicon-stop'></i>";
            wavesurferRecord.microphone.start();
        }
        else {
            this.style.borderStyle = 'outset';
            this.style.color = 'white';
            this.innerHTML = "<i class='glyphicon glyphicon-record'></i>";
            wavesurferRecord.microphone.stop();
        }
        document.getElementById("btnRecordingPlay").disabled = btnIsPlaying;
        document.getElementById("btnRecordingPaste").disabled = btnIsPlaying;
    });

    btn = window.document.createElement('button');
    btn.id = "btnRecordingPlay";
    btn.innerHTML = "<i class='glyphicon glyphicon-play'></i>";
    btn.title = "Play/Stop (Spacebar)"
    btn.className = 'btn btn-primary';
    btn.disabled = true;
    divControlButtons.appendChild(btn);
    btn.addEventListener("click", function (e) {
        this.blur();
        btnIsPlaying = !btnIsPlaying;
        if (e == false)
            btnIsPlaying = false;
        if (btnIsPlaying) {
            this.style.borderStyle = 'inset';
            this.style.color = 'black';
            this.innerHTML = "<i class='glyphicon glyphicon-play'></i>";
            wavesurferRecord.play();
        }
        else {
            this.style.borderStyle = 'outset';
            this.style.color = 'white';
            wavesurferRecord.stop();
        }
        document.getElementById("btnRecordingStart").disabled = btnIsPlaying;
        document.getElementById("btnRecordingPaste").disabled = btnIsPlaying;
    });

    btn = window.document.createElement('button');
    btn.id = "btnRecordingPaste";
    btn.parentWindowId = divWindow.id;
    btn.title = "Insert (ENTER)"
    btn.innerHTML = "<i class='glyphicon glyphicon-paste'></i>";
    btn.className = 'btn btn-primary';
    btn.disabled = true;
    divControlButtons.appendChild(btn);
    btn.addEventListener("click", async function (e) {
        this.blur();
        await addToHistory(HistoryType.audiodata, "Signal recorded");
        divWindow = document.getElementById(this.parentWindowId);
        divWindow.setEnable(false);
        await cutCopyAudioData(true, false);
        setMemoryBuffer([]);
        pushToMemoryBuffer(new Float32Array(wavesurferRecord.backend.buffer.duration * wavesurferRecord.backend.buffer.sampleRate));
        pushToMemoryBuffer(new Float32Array(wavesurferRecord.backend.buffer.duration * wavesurferRecord.backend.buffer.sampleRate));
        wavesurferRecord.backend.buffer.copyFromChannel(memoryBuffer[0], 0, 0);
        wavesurferRecord.backend.buffer.copyFromChannel(memoryBuffer[1], 1, 0);
        await pasteAudioData(true, false);
        divWindow.onclose(divWindow.id);
        setMemoryBuffer([]);
    });

    var btnRcord = document.getElementById("btnRecord");
    btnRcord.blur();
    btnRcord.style.borderStyle = 'inset';
    btnRcord.style.color = 'black';

    var wavesurferRecord = WaveSurfer.create({
        container: "#" + divContent.id,
        waveColor: SystemPreferences.wavecolor,
        progressColor: SystemPreferences.progresscolor,
        loaderColor: SystemPreferences.progresscolor,
        backend: 'WebAudio',
        height: divContent.offsetHeight / 2,
        cursorColor: 'yellow',
        hideScrollbar: true,
        hideCursor: false,
        partialRender: false,
        fillParent: true,
        autoCenter: true,
        forceDecode: true,
        responsive: true,
        splitChannels: true,
        interact: false,
        cursorWidth: 0,
        plugins: [
            WaveSurfer.microphone.create({
                bufferSize: 4096,
                numberOfInputChannels: 2,
                numberOfOutputChannels: 2,
                constraints: {
                    video: false,
                    audio: true
                }
            }),
        ]
    });
    wavesurferRecord.on('redraw', function () {
        document.getElementById("RecordTime").innerHTML = new Date((wavesurferRecord.microphone.micContext.currentTime - startTime) * 1000).toISOString().substr(11, 12);
    });

    wavesurferRecord.microphone.on('deviceReady', function (stream) {
        wavesurferRecord.setHeight(divContent.offsetHeight / 2);
        audioChunks = [];
        blnIsRecorded = false;
        mediaRecorder = new MediaRecorder(stream);
        mediaRecorder.start();
        startTime = wavesurferRecord.microphone.micContext.currentTime;
        mediaRecorder.addEventListener('dataavailable', event => {
            audioChunks.push(event.data);
        })
        mediaRecorder.addEventListener("stop", () => {
            if (wavesurferRecord) {
                blnIsRecorded = true;
                var audioBlob = new Blob(audioChunks);
                wavesurferRecord.loadBlob(audioBlob);
            }
        });
    });

    wavesurferRecord.on("pause", function () {
        document.getElementById("btnRecordingPlay").click(false);
    });

    wavesurferRecord.on("ready", function () {
        if (blnIsRecorded) {
            if (wavesurferRecord.backend.buffer.numberOfChannels == 1) {
                divWindow.setEnable(false);
                var tempArray = new Float32Array(wavesurferRecord.backend.buffer.duration * wavesurferRecord.backend.buffer.sampleRate);
                wavesurferRecord.backend.buffer.copyFromChannel(tempArray, 0, 0);
                var tempBuffer = wavesurferRecord.backend.ac.createBuffer(2, tempArray.length, wavesurferRecord.backend.buffer.sampleRate);
                tempBuffer.copyToChannel(tempArray, 0, 0);
                tempBuffer.copyToChannel(tempArray, 1, 0);
                wavesurferRecord.loadDecodedBuffer(tempBuffer);
                document.getElementById("RecordTime").innerHTML = new Date(tempBuffer.duration * 1000).toISOString().substr(11, 12);
            }
            else
                divWindow.setEnable(true);
        }
    });
    wavesurferRecord.microphone.on('deviceError', function (code) {
    });
    divWindow.setEnable(true);
}