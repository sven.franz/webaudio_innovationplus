import { closeDropdowns, fitWindowSize, updateDropDownMenuCheckboxes } from "./dropdown.js";
import { updateOverlays } from "./processing.js";
import { wavesurfer, showSpec, setShowSpec, colorMap, refreshSpetrogram, setRefreshSpetrogram, windowResize, SystemPreferences } from "./script.js";

var spectrogramWorker = null;
var maxBufferSizeWarning = 44100 * 5 * 60;

if (window.Worker)
    spectrogramWorker = new Worker("/static/JS/ftt_worker.js");

export async function showSpectrogram() {
    closeDropdowns();
    await new Promise(resolve => setTimeout(resolve, 0));
    var proceed = true;
    if (wavesurfer.backend.buffer.length > maxBufferSizeWarning)
        proceed = !confirm("Your Audiofile is very large! Spectrogram-View is not recommended! Cancel?");
    setShowSpec(proceed);
    if (showSpec) {
        var initSpec = false;
        if (!wavesurfer.spectrogram) {
            initSpec = true
            wavesurfer.addPlugin(WaveSurfer.spectrogram.create({
                container: "#wave-spectrogram",
                colorMap: colorMap,
                splitChannels: true,
                frequencyMax: Math.min(SystemPreferences.frequencyRange[1], wavesurfer.backend.buffer.sampleRate / 2),
                frequencyMin: Math.min(SystemPreferences.frequencyRange[0], SystemPreferences.frequencyRange[1]),
                //height: window.innerHeight - 90 - 88,
                windowFunc: SystemPreferences.window,
                noverlap: Math.min(Number(SystemPreferences.noverlap) * Number(SystemPreferences.fftSize), Number(SystemPreferences.fftSize) - 1),
                fftSamples: Number(SystemPreferences.fftSize),
                labels: true
            }));
            //

            //wavesurfer.spectrogram.fftSamples = (1 << 31 - Math.clz32(wavesurfer.container.offsetHeight));
            //wavesurfer.spectrogram.height = wavesurfer.spectrogram.fftSamples;

            wavesurfer.spectrogram.render = async function () {
                if (refreshSpetrogram && showSpec) {
                    setRefreshSpetrogram(false);
                    this.updateCanvasStyle();
                    if (this.frequenciesDataUrl) {
                        this.loadFrequenciesData(this.frequenciesDataUrl);
                    } else {
                        if (window.Worker) {
                            if (!spectrogramWorker.onmessage) {
                                spectrogramWorker.onmessage = async function (event) {
                                    wavesurfer.spectrogram.frequencies = event.data;
                                    // now draw spectrogram when FFT has done in background!
                                    if (wavesurfer.spectrogram)
                                        wavesurfer.spectrogram.drawSpectrogram(wavesurfer.spectrogram.frequencies, wavesurfer.spectrogram);
                                    wavesurfer.spectrogram.canvas.style.height = wavesurfer.container.offsetHeight + "px"
                                    /*
                                    for (var idx = 0; idx < document.getElementsByClassName("spec-labels").length; idx++) {
                                        document.getElementsByClassName("frequencies")[idx].appendChild(document.getElementsByClassName("spec-labels")[idx]);
                                        document.getElementsByClassName("spec-labels")[idx].style.position = "relative";
                                        document.getElementsByClassName("spec-labels")[idx].style.top = "0px";
                                    }
                                    */
                                    //for (var idx = 0; idx < document.getElementsByClassName("spec-labels").length; idx++)
                                    //    document.getElementsByClassName("spec-labels")[idx].style.height = wavesurfer.container.offsetHeight + "px";

                                    //wavesurfer.spectrogram.canvas.parentElement.style.height = wavesurfer.container.offsetHeight + "px"
                                    //wavesurfer.spectrogram.canvas.parentElement.getElementsByClassName("spec-labels")[0].style.height = wavesurfer.container.offsetHeight + "px";
                                    //if (Number(document.getElementById("wave-spectrogram").childNodes[0].style.width.replace("px", "")) != wavesurfer.drawer.container.offsetWidth) {
                                    //    document.getElementById("wave-spectrogram").childNodes[0].style.width = wavesurfer.drawer.container.offsetWidth + "px";
                                    //}
                                    //
                                    //wavesurfer.spectrogram.canvas.style.width = Math.max(wavesurfer.drawer.getWidth(), Math.round(wavesurfer.getDuration() * wavesurfer.params.minPxPerSec * wavesurfer.params.pixelRatio)) + "px"
                                    //wavesurfer.spectrogram.canvas.parentElement.getElementsByClassName("spec-labels")[0].style.display = "block";
                                    await new Promise(resolve => setTimeout(resolve, 0));
                                    await updateOverlays();
                                };
                            }
                            wavesurfer.spectrogram.buffer = this.wavesurfer.backend.buffer;
                            spectrogramWorker.postMessage({
                                sampleRate: wavesurfer.spectrogram.buffer.sampleRate,
                                duration: wavesurfer.spectrogram.buffer.duration,
                                length: wavesurfer.spectrogram.buffer.length,
                                numberOfChannels: wavesurfer.spectrogram.buffer.numberOfChannels,
                                fftSamples: wavesurfer.spectrogram.fftSamples,  //(1 << 31 - Math.clz32(wavesurfer.container.offsetHeight)) * 2,
                                buffer: wavesurfer.spectrogram.buffer.numberOfChannels == 1 ? [wavesurfer.spectrogram.buffer.getChannelData(0)] : [wavesurfer.spectrogram.buffer.getChannelData(0), wavesurfer.spectrogram.buffer.getChannelData(1)],
                                windowFunc: SystemPreferences.window,
                                noverlap: Math.min(Number(SystemPreferences.noverlap) * Number(SystemPreferences.fftSize), Number(SystemPreferences.fftSize) - 1),
                                frequencyMax: wavesurfer.spectrogram.frequencyMax,
                                frequencyMin: wavesurfer.spectrogram.frequencyMin,
                                preEmphasis: SystemPreferences.preEmphasis,
                                preEmphasisFrequency: SystemPreferences.preEmphasisFrequency
                            });
                        }
                        else {
                            this.getFrequencies(this.drawSpectrogram);
                        }
                    }
                }
            }
        }
        if (initSpec) {
            if (document.getElementById("spec-labels_Container"))
                document.getElementById("spec-labels_Container").remove();
            wavesurfer.initPlugin('spectrogram')
            if (wavesurfer.spectrogram.canvas && wavesurfer.spectrogram.canvas.parentElement)
                wavesurfer.spectrogram.canvas.parentElement.style.height = "200%";;
            wavesurfer.spectrogram.canvas.style.height = "100%";
            wavesurfer.spectrogram.canvas.parentElement.style.position = "relative";
            wavesurfer.spectrogram.height = 1 << 31 - Math.clz32(wavesurfer.spectrogram.canvas.offsetHeight / wavesurfer.backend.buffer.numberOfChannels);
            wavesurfer.spectrogram.loadLabels('rgba(68,68,68,0.5)', '12px', '10px', '', '#fff', '#f7f7f7', 'center', '#specLabels');

            if (!document.getElementById("spec-labels_Container")) {
                var labels_Container = window.document.createElement('div');
                labels_Container.id = "spec-labels_Container";
                labels_Container.style.position = "absolute";
                labels_Container.style.top = "0px";
                labels_Container.style.height = "200%";
                document.getElementsByClassName("spec-labels")[0].parentElement.parentElement.appendChild(labels_Container)
            }
            document.getElementById("spec-labels_Container").appendChild(document.getElementsByClassName("spec-labels")[0]);
            document.getElementsByClassName("spec-labels")[0].style.position = "relative";
            document.getElementsByClassName("spec-labels")[0].style.height = "100%";
        }
        else;
            wavesurfer.spectrogram._onRender();
        //wavesurfer.spectrogram.canvas.parentElement.getElementsByClassName("spec-labels")[0].style.display = "none";
    }
    updateDropDownMenuCheckboxes();
}

export async function hideSpectrogram() {
    if (wavesurfer.spectrogram) {
        wavesurfer.destroyPlugin("spectrogram");
        wavesurfer.spectrogram = null;
        setRefreshSpetrogram(true);
    }
    setShowSpec(false);
    if (document.getElementById("spec-labels_Container"))
        document.getElementById("spec-labels_Container").remove();
    updateDropDownMenuCheckboxes();
    document.cookie = "showSpec=" + showSpec;
}