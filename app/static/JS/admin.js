var gui = null;
var controls = null;
var ItemText = null;
var currentTab = "";
var newItemTextField = null;

/*
 * uuid-timestamp (emitter)
 * UUID v4 based on timestamp
 *
 * Created by tarkh
 * tarkh.com (C) 2020
 */
const uuidEmit = () => {
    // Get now time
    const n = Date.now();
    // Generate random
    const r = Math.random();
    // Stringify now time and generate additional random number
    const s = String(n) + String(~~(r * 9e4) + 1e4);
    // Form UUID and return it
    return `${s.slice(0, 8)}-${s.slice(8, 12)}-4${s.slice(12, 15)}-${[8, 9, 'a', 'b'][~~(r * 3)]}${s.slice(15, 18)}-${s.slice(s.length - 12)}`;
};

function getCookie(key, defaultValue = false) {
    var list = document.cookie.split('; ')
    if (list.length) {
        var item = list.find(row => row.startsWith(key + '='))
        if (item)
            return item.split('=')[1]
    }
    return defaultValue;
}

document.addEventListener("DOMContentLoaded", function(e){
    currentTab = getCookie("currentTab", "Folders");
    document.getElementById("btn" + currentTab).click();
 });

 window.addEventListener("contextmenu", e => {
    if (e.target.tagName !== "IMG") {
        e.preventDefault();
    }
});

async function openTab(evt, name) {
    currentTab = name;
    const d = new Date();
    d.setTime(d.getTime() + (24 * 60 * 60 * 1000));    
    document.cookie = "currentTab=" + currentTab + ";expires=" + d.toUTCString() + ";path=/";
    await clearGui();
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
        tabcontent[i].getElementsByClassName("tree")[0].id = tabcontent[i].id + "_tree";
        tabcontent[i].getElementsByClassName("tree")[0].innerHTML = "";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");

    }
    document.getElementById(name).style.display = "flex";
    document.getElementById(name + "_tree").id = "div_tree";
    document.getElementById("btn" + name).className += " active";
    if (Object.keys(window).find((key) => key == "load" + name))
        window["load" + name]();
}

async function showTree(data) {
    await clearGui();
    var parent = document.getElementById("div_tree");
    var btn = window.document.createElement('button');
    btn.id = "add";
    btn.innerHTML = "Add";
    btn.className = 'btn btn-primary';
    btn.addEventListener("click", async function (e) {
        text = window.prompt("Value: ", "New Item");
        if (text) {
            var parent = document.getElementById("treeRoot");
            if (parent.getElementsByTagName("ul").length == 0)
                parent.appendChild(document.createElement("ul"));
            var newItem = document.createElement("li");
            newItem.id = uuidEmit();
            currItemText = ItemText;
            if (Array.isArray(currItemText))
                currItemText = currItemText[0];
            text = currItemText.replaceAll("{{ID}}", newItem.id).replaceAll("{{" + newItemTextField.toUpperCase() + "}}", text);
            if (text.match(/\{{[^}]+}\}/g))
                for (var tag of text.match(/\{{[^}]+}\}/g))
                    if (tag.toLowerCase() != newItemTextField.toLowerCase())
                        text = text.replaceAll(tag, "");
            newItem.innerHTML = "<a href='#'>" + text + "</a>";
            parent.getElementsByTagName("ul")[0].appendChild(newItem);
            if (currentTab == "Usergroups")
            newItem.setAttribute("noDrag", true);
            treeObj.__initItem(newItem);
        }
    });
    parent.appendChild(btn);
    var btn = window.document.createElement('button');
    btn.id = "Save";
    btn.innerHTML = "Save";
    btn.className = 'btn btn-primary';
    btn.addEventListener("click", async function (e) {
        document.getElementById("hideWavesurfer").style.visibility = "visible";
        var tree = document.getElementById("treeRoot");
        data = {};
        data[currentTab] = Array();
        for (let item of tree.getElementsByTagName("li")) {
            currItemText = ItemText;
            if (Array.isArray(currItemText) && item.getElementsByTagName("level").length)
                currItemText = currItemText[Number(item.getElementsByTagName("level")[0].innerHTML)];
            var tags = currItemText.match(/\{{[^}]+}\}/g);
            data[currentTab].push({ id: item.id, parent_id: null, deleted: (item.hasAttribute("deleted") && item.getAttribute("deleted") == "true") })
            if (item.getElementsByTagName("level").length)
                data[currentTab][data[currentTab].length - 1].level = Number(item.getElementsByTagName("level")[0].innerHTML);
            if (item.parentElement.parentElement.id != "treeRoot")
                data[currentTab][data[currentTab].length - 1]["parent_id"] = item.parentElement.parentElement.id
            if (item.parentElement.parentElement.hasAttribute("deleted") && item.parentElement.parentElement.getAttribute("deleted") == "true") {
                data[currentTab][data[currentTab].length - 1]["deleted"] = true;
                item.setAttribute("deleted", true);
            }
            for (var idxTag = 0; idxTag < tags.length; idxTag++) {
                var tag = tags[idxTag].replaceAll("{{", "").replaceAll("}}", "");
                if (item.getElementsByTagName(tag).length) {
                    var value = item.getElementsByTagName(tag)[0].innerHTML;
                    if (tag.toLowerCase() == "color")
                        value = item.getElementsByTagName(tag)[0].style.backgroundColor
                    if (!isNaN(value))
                        value = Number(value);
                    data[currentTab][data[currentTab].length - 1][tag] = value;
                }
            }
        };
        const storeAdministrationData = new XMLHttpRequest();
        storeAdministrationData.responseType = 'json';
        storeAdministrationData.open('POST', '/storeAdministrationData');
        storeAdministrationData.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        storeAdministrationData.onreadystatechange = () => {
            if (storeAdministrationData.readyState == XMLHttpRequest.DONE && storeAdministrationData.response) {
                var item = JSON.parse(storeAdministrationData.response);
                if (item.succsess) {
                }
                else {
                    alert("Error saving data! Please check Server!")
                }
                openTab(null, currentTab);
                document.getElementById("hideWavesurfer").style.visibility = "hidden";
            }
        }
        storeAdministrationData.send(JSON.stringify(data));
    });
    parent.appendChild(btn);
    var tree = document.createElement("ul");
    tree.id = "dhtmlgoodies_tree"
    tree.setAttribute("class", "dhtmlgoodies_tree")
    parent.appendChild(tree);

    var treeRoot = document.createElement("li");
    treeRoot.id = "treeRoot";
    treeRoot.innerHTML = "<a href='#'><b>" + currentTab + "</b></a>";
    treeRoot.setAttribute("noDrag", true);
    treeRoot.setAttribute("noSiblings", true);
    treeRoot.setAttribute("noDelete", true);
    treeRoot.setAttribute("noRename", true);
    if (currentTab == "Usergroups")
        treeRoot.setAttribute("noChildren", true);
    tree.appendChild(treeRoot);

    data.forEach((item) => {
        parent = treeRoot;
        if (item.parent)
            parent = document.getElementById(item.parent);
        if (parent.getElementsByTagName("ul").length == 0)
            parent.appendChild(document.createElement("ul"));
        var newItem = document.createElement("li");
        newItem.id = item.id;
        newItem.innerHTML = "<a href='#'>" + item.text + "</a>";
        if (currentTab == "Usergroups") {
            if (!item.parent)
                newItem.setAttribute("noDrag", true);
            else
                newItem.setAttribute("noChildren", true);
        }
        else if (currentTab == "Folders" && item.level > 0)
            newItem.setAttribute("noChildren", true);
        parent.getElementsByTagName("ul")[0].appendChild(newItem);
    });
    var treeObj = new JSDragDropTree();
    treeObj.setTreeId('dhtmlgoodies_tree');
    treeObj.setMaximumDepth(10);
    treeObj.setMessageMaximumDepthReached('Maximum depth reached');
    treeObj.initTree();
    treeObj.expandAll();
    document.getElementById("hideWavesurfer").style.visibility = "hidden";
}

function clearGui() {
    if (gui) {
        gui.container.remove();
        delete gui;
    }
    gui = null;
    return true;
}

async function editTreeItem(obj1, obj2) {
    await clearGui();
    var editWindow = document.getElementById(currentTab + "Edit");
    var node = obj2;
    while (node.tagName != "A")
        node = node.parentElement
    if (node && node.getElementsByTagName(newItemTextField).length) {
        var id = node.id;
        gui = new guify({
            title: "Edit" + currentTab,
            root: editWindow,
            width: "100%",
            open: true,
            barMode: "none",
        });
        gui.container.style.position = "relative";
        gui.container.style.height = "100%";
        gui.container.childNodes[0].style.height = "100%";
        gui.container.childNodes[0].style.overflowX = "hidden";
        gui.container.childNodes[0].style.overflowY = "auto";
        gui.container.childNodes[0].style.zIndex = 0

        var myControls = JSON.parse(JSON.stringify(controls))
        if (Array.isArray(controls) && Array.isArray(controls[0])) {
            tmpObj = obj2;
            while (tmpObj.getElementsByTagName("level").length == 0)
                tmpObj = tmpObj.parentElement;
            myControls = JSON.parse(JSON.stringify(controls[Number(tmpObj.getElementsByTagName("level")[0].innerHTML)]))
        }


        for (idx in myControls) {
            myControls[idx].object = eval(myControls[idx].object);
            if (myControls[idx].type.toLowerCase() == "checkbox") {
                /* myControls[idx].onChange= (value) => {
                    console.log(value);
                }
                myControls[idx].onInitialize = (a, index = idx) => {
                    gui.loadedComponents[index].input.value = "off"
                    if (myControls[index].initial == "false")
                        gui.loadedComponents[index].input.checked = false;
                    else
                        gui.loadedComponents[index].input.checked = true;
                }*/
            }
        }
        gui.Register(myControls);
        if (currentTab == "Usergroups" && Number(tmpObj.getElementsByTagName("level")[0].innerHTML) == 0 && tmpObj.getElementsByTagName("name")[0].innerText != "None")
            showGroupPermissions(gui.container.childNodes[0], JSON.parse(node.getElementsByTagName("permissions")[0].innerText));
        var btn = window.document.createElement('button');
        btn.id = "delete";
        btn.parent = id;
        btn.innerHTML = "Delete";
        btn.className = 'btn btn-primary';
        btn.addEventListener("click", async function (e) {
            if (confirm("Do you really want to delete this Node and all Childs?")) {
                var nodes = document.querySelectorAll(`value#${CSS.escape(e.target.parent)}`);
                if (nodes.length == 0)
                    nodes = document.querySelectorAll(`#${CSS.escape(e.target.parent)}`);
                var node = nodes[0];
                while (node.tagName != "LI")
                    node = node.parentElement
                node.style.display = "none";
                node.setAttribute("deleted", true)
                await clearGui();
            }
        });
        if ((currentTab == "Usergroups" || currentTab == "Folders") && (node.parentElement.id == "temp" || (node.parentElement.getElementsByTagName("ul").length && node.parentElement.getElementsByTagName("ul")[0].getElementsByTagName("li").length)))
            btn.disabled = true;
        gui.container.childNodes[0].appendChild(btn);
    }
}

function createItemText(data, idx = 0) {
    currItemText = ItemText;
    if (Array.isArray(currItemText))
        currItemText = currItemText[idx];
    var ItemTextTemp = currItemText.replaceAll("{{ID}}", data.id);
    for (var tag of currItemText.match(/\{{[^}]+}\}/g)) {
        if (Object.keys(data).find(key => key.toLowerCase() === tag.substring(2, tag.length - 2).toLowerCase()))
            ItemTextTemp = ItemTextTemp.replaceAll(tag, data[Object.keys(data).find(key => key.toLowerCase() === tag.substring(2, tag.length - 2).toLowerCase())]);
    }
    return { id: data.id, text: ItemTextTemp };
}

async function getDataFromServer(url) {
    const fetcher = fetch(url, {
        headers: { 'Content-Type': 'application/json' },
        credentials: 'same-origin'
    }).then((response) => response.json()).then((data) => { return data; })
    data = await fetcher;
    if (typeof (data) == 'object')
        return data;
    else
        return JSON.parse(data);
}

// ************************************************************************************************************************
// Source: https://css-tricks.com/indeterminate-checkboxes/

//  helper function to create nodeArrays (not collections)
var nodeArray = (selector, parent=document) => [].slice.call(parent.querySelectorAll(selector));
//  checkboxes of interest 
var allThings = nodeArray('input');

//  global listener
function onChangeCheckbox(e) {
  let check = e.target;
  //  exit if change event did not come from 
  //  our list of allThings 
  if(allThings.indexOf(check) === -1) return;
  //  check/unchek children (includes check itself)
  var permissions = JSON.parse(gui.loadedComponents[0].binding.object.parentElement.getElementsByTagName("permissions")[0].innerText);
  permissions.find(item => item.folder_id == check.id.split("_")[0])[check.id.split("_")[1]] = check.checked;
  const children = nodeArray('input', check.parentNode.parentNode);
  for (child of children) {
    var key = check.id.split("_")[1];
    if (key == child.id.split("_")[1]) {
        child.checked = check.checked
        permissions.find(item => item.folder_id == child.id.split("_")[0])[child.id.split("_")[1]] = child.checked;
    }
    if (key == "allowRead") {
        if (child.id.split("_")[1] != "allowRead")
            child.disabled = !check.checked;
            for (subChild of children) {
                if ((child.id.split("_")[1] == "allowManualAnnotationTags" && subChild.id.split("_")[1] == "allowManualAnnotationTags") || 
                (child.id.split("_")[1] == "allowModify" && subChild.id.split("_")[1] == "allowChangeProtection"))
                    subChild.disabled = !child.checked;
                if ((subChild.id.split("_")[1] == "allowManualAnnotationTags" && child.id.split("_")[1] == "allowManualAnnotationTags") ||
                (subChild.id.split("_")[1] == "allowModify" && child.id.split("_")[1] == "allowChangeProtection"))
                    child.disabled = !subChild.checked;
            }
    }
    if (key == "allowModify") {
        if (child.id.split("_")[1] == "allowChangeProtection")
            child.disabled = !check.checked;
    }
    if (key == "allowAnnotationTags") {
        if (child.id.split("_")[1] == "allowManualAnnotationTags")
            child.disabled = !check.checked;
    }
    if (key == "allowChangeProtection") {
        if (child.id.split("_")[1] == "allowModify")
            check.disabled = !child.checked;
    }
    if (key == "allowManualAnnotationTags") {
        if (child.id.split("_")[1] == "allowAnnotationTags")
            check.disabled = !child.checked;
    }
    if (child.disabled) {
        child.checked = false;
        permissions.find(item => item.folder_id == child.id.split("_")[0])[child.id.split("_")[1]] = child.checked;
    }
  }
  //  traverse up from target check
  while(check){
    //  find parent and sibling checkboxes (quick'n'dirty)
    var parent   = (check.closest(['ul']).parentNode).querySelector('input');
    while (check.id.split("_")[1] != parent.id.split("_")[1])
        parent = parent.nextSibling;
    const allSiblings = nodeArray('input', parent.closest('li').querySelector(['ul']));
    var siblings = Array();
    allSiblings.forEach(sibling => {
        if (check.id.split("_")[1] == sibling.id.split("_")[1])
            siblings.push(sibling);
      });
    //  get checked state of siblings
    //  are every or some siblings checked (using Boolean as test function) 
    const checkStatus = siblings.map(check => check.checked);
    const every  = checkStatus.every(Boolean);
    const some = checkStatus.some(Boolean);   
    //  check parent if all siblings are checked
    //  set indeterminate if not all and not none are checked
    parent.checked = every;   
    permissions.find(item => item.folder_id == parent.id.split("_")[0])[parent.id.split("_")[1]] = parent.checked;
    parent.indeterminate = !every && every !== some;
    //  prepare for nex loop
    check = check != parent ? parent : 0;
  }
  gui.loadedComponents[0].binding.object.parentElement.getElementsByTagName("permissions")[0].innerText = JSON.stringify(permissions)
}


// ************************************************************************************************************************

async function loadFolders() {
    function convertDataToTree(data, items, parent = 0) {
        if (items) {
            if (items.hasOwnProperty("folders") && items.folders) {
                for (idx in items.folders) {
                    folder = createItemText(items.folders[idx], 0)
                    folder.level = 0;
                    folder.parent = parent;
                    if (parent == 0)
                        folder.parent = null;
                    data.push(folder);
                    data = convertDataToTree(data, { folders: items.folders[idx].folders, files: items.folders[idx].files }, items.folders[idx].id);
                }
            }
            if (items.hasOwnProperty("files") && items.files) {
                for (file of items.files) {
                    file.name = file.name;
                    file.filepartcount = 1;
                    if (file.fileparts)
                        file.filepartcount = file.fileparts.length;
                    file = createItemText(file, 1);
                    file.parent = parent;
                    if (parent == 0)
                        file.parent = null;
                    file.level = 1;
                    data.push(file);
                }
            }
        }
        return data;
    }
    document.getElementById("hideWavesurfer").style.visibility = "visible";
    ItemText = new Array();
    ItemText[0] = "<b><name>{{NAME}}</name></b><level style='display: none'>0</level>";
    ItemText[1] = "<name>{{NAME}}</name> [<channels>{{CHANNELS}}</channels>, <samplingRate>{{SAMPLINGRATE}}</samplingRate> Hz, <duration>{{DURATION}}</duration> s]<filepartcount style='display: none'>{{FILEPARTCOUNT}}</filepartcount><level style='display: none'>1</level>"
    newItemTextField = "name";
    controls = new Array();
    controls[0] = [
        { type: "text", label: "Name", property: "innerText", object: "node.children[0]" }
    ];
    controls[1] = [
        { type: "text", label: "Name", property: "innerText", object: "node.children[0]" },
        { type: "display", label: "Channels", property: "innerText", object: "node.children[1]" },
        { type: "display", label: "Sampling Rate / Hz", property: "innerText", object: "node.children[2]" },
        { type: "display", label: "Duration / s", property: "innerText", object: "node.children[3]" },
        { type: "display", label: "Fileparts", property: "innerText", object: "node.children[4]" },
    ];
    data = await getDataFromServer('/loadFilesAndFolders?admin');
    data = await convertDataToTree([], data);
    showTree(data);
}

async function loadTags() {
    function convertDataToTree(data, items, parent = 0) {
        for (idx in items) {
            if (items[idx].color.substring(0, 4).toLowerCase() != "rgb(")
                items[idx].color = "rgb(" + items[idx].color + ")";
            else if (items[idx].color.toLowerCase() == "rgb(")
                items[idx].color = "rgb(0, 0, 0)";
            item = createItemText(items[idx])
            if (parent != 0)
                item.parent = parent;
            data.push(item);
            if (items[idx].children.length)
                data = convertDataToTree(data, items[idx].children, items[idx].id);
        }
        return data;
    }
    document.getElementById("hideWavesurfer").style.visibility = "visible";
    ItemText = "<color style='background-color: {{COLOR}}'>&nbsp;&nbsp;&nbsp;&nbsp;</color>&nbsp;<name>{{NAME}}</name>"
    newItemTextField = "NAME";
    controls = [
        { type: "text", label: "Tag", property: "innerText", object: "node.children[1]" },
        { type: "color", label: "Color", property: "backgroundColor", object: "node.children[0].style", format: "rgb" },
    ];
    showTree(await convertDataToTree([], await getDataFromServer('/hierarchicalTagList')));
}

async function loadUsergroups() {
    function convertDataToTree(data, groups, parent = 0) {
        for (group of groups) {
            if (!group.id)
                group.id = "temp";
            group.level = 0;
            group.permissions = JSON.stringify(group.permissions);
            data.push(createItemText(group));
            for (user of group.users) {
                user = createItemText(user, 1)
                user.parent = group.id;
                user.level = 1;
                data.push(user);
            }
        }
        return data;
    }
    document.getElementById("hideWavesurfer").style.visibility = "visible";
    ItemText = new Array();
    ItemText[0] = "<b><name>{{NAME}}</name></b><level style='display: none'>0</level><permissions style='display: none'>{{PERMISSIONS}}</permissions>";
    ItemText[1] = "<name>{{NAME}}</name> (<email>{{EMAIL}}</email>)<password style='display: none'>{{PASSWORD}}</password>, Dev: <isDeveloper>{{ISDEVELOPER}}</isDeveloper>, Admin: <isAdmin>{{ISADMIN}}</isAdmin><level style='display: none'>1</level>";
    newItemTextField = "NAME";
    controls = new Array();
    controls[0] = [
        { type: "text", label: "Name", property: "innerText", object: "node.children[0]" }
    ];
    controls[1] = [
        { type: "text", label: "Name", property: "innerText", object: "node.children[0]" },
        { type: "text", label: "Email", property: "innerText", object: "node.children[1]" },
        { type: "checkbox", label: "Developer", property: "innerText", object: "node.children[4]" },
        { type: "checkbox", label: "Administrator", property: "innerText", object: "node.children[5]" },
    ];
    showTree(await convertDataToTree([], await getDataFromServer('/UsergroupsAndUsersList')));
}

function showGroupPermissions(container, data) {
    var tabularDivId = "Permissions" + currentTab;
    if (document.getElementById(tabularDivId))
        document.getElementById(tabularDivId).remove();
    parentDiv = window.document.createElement('div');
    parentDiv.id = tabularDivId;
    container.appendChild(parentDiv);

    var tree = document.createElement("ul");
    tree.style.marginTop = "110px";
    tree.id = "dhtmlgoodies_PermissionsTree"
    tree.setAttribute("class", "dhtmlgoodies_tree")
    parentDiv.appendChild(tree);

    var treeRoot = document.createElement("li");
    treeRoot.id = "treeRootGroupPermissions";

    treeRoot.innerHTML = "<a href='#' style='display: inline-block; width: 200px; overflow: hidden;'>Permissions</a>" +
    "<span style='display: inline-block; position: absolute; right: 50%;'><span class='rotate'>Open&nbsp;File</span>" +
    "<span class='rotate'>Modify&nbsp;Signal</span>" +
    "<span class='rotate'>Annotations</span>" +
    "<span class='rotate'>Manual&nbsp;Annotations</span>" +
    "<span class='rotate'>Change&nbsp;Protection</span></span>"  

    treeRoot.setAttribute("noDrag", true);
    treeRoot.setAttribute("noSiblings", true);
    treeRoot.setAttribute("noDelete", true);
    treeRoot.setAttribute("noRename", true);
    tree.appendChild(treeRoot);

    data.forEach((item) => {
        parent = treeRoot;
        if (item.parentFolder_id) {
            for (let folderItem of document.getElementsByTagName("folderId")) {
                if (folderItem.innerText == item.parentFolder_id)
                    parent = folderItem.parentElement.parentElement;
            }
        }
        if (parent.getElementsByTagName("ul").length == 0)
            parent.appendChild(document.createElement("ul"));
        var newItem = document.createElement("li");
        newItem.id = item.id;
        newItem.innerHTML = "<a href='#' style='display: inline-block; width: 200px; overflow: hidden;'>" + item.name + "<folderId style='display: none;'>" + item.folder_id + "</folderId></a>" +
        "<span style='display: inline-block; position: absolute; right: 50%;'>" +
        "<input type='checkbox' id='" + item.folder_id + "_allowRead' " + (item.allowRead ? "checked" : "") +" />" +
        "<input type='checkbox' id='" + item.folder_id + "_allowModify' " + (item.allowModify ? "checked" : "") +" />" +
        "<input type='checkbox' id='" + item.folder_id + "_allowAnnotationTags' " + (item.allowAnnotationTags ? "checked" : "") +" />" +
        "<input type='checkbox' id='" + item.folder_id + "_allowManualAnnotationTags' " + (item.allowManualAnnotationTags ? "checked" : "") +" />" +
        "<input type='checkbox' id='" + item.folder_id + "_allowChangeProtection' " + (item.allowChangeProtection ? "checked" : "") +" /></span>"  
        newItem.setAttribute("noDrag", true);
        newItem.setAttribute("noSiblings", true);
        newItem.setAttribute("noChildren", true);
        newItem.setAttribute("noDelete", true);
        newItem.setAttribute("noRename", true);
        parent.getElementsByTagName("ul")[0].appendChild(newItem);
    });
    nodeArray = (selector, parent=document) => [].slice.call(parent.querySelectorAll(selector));
    allThings = nodeArray('input');
    for (item of allThings) {
        if (item.type == "checkbox")
        {
            item.addEventListener('change', onChangeCheckbox);
            var event = new Event('change');
            item.dispatchEvent(event);
        }
    }
    var treePermissions = new JSDragDropTree();
    treePermissions.setDeleteAllowed(false);
    treePermissions.setRenameAllowed(false)
    treePermissions.setTreeId('dhtmlgoodies_PermissionsTree');
    treePermissions.initTree();
    treePermissions.expandAll();
}