import { showWave, showSpec, showListAnnotations, wavesurfer, zoomLevel, setShowWave, updateWaveColors, setShowListAnnotations, setAudioSignalHasChanges, SystemPreferences, getColormap, setColorMap, setShowListOverlays, showListOverlays, setWavesurfer, clearFilePreferences, setFilePreferenceChanges, showListPlugins, DataSet, loadFilePreferences, updateFilePreferences, UserSettings } from "./script.js";
import { setDeletedRegions, selectedRegion, addImportedAnnotation, loadAnnotations } from "./annotation.js";
import { bufferToWave } from "./bufferToWave.js";
import { closePlugin, createPopUpWindow, dockWindow, getPluginWindows, loadPluginList, onClick_OK, PluginParams } from "./plugins.js";
import { updateOverlays, setOverlays } from "./processing.js";
import { hideSpectrogram, showSpectrogram } from "./spectrogram.js";


var DropDownsVisible = false;
/* When the user clicks on the button,
toggle between hiding and showing the dropdown content *///mouseoverDropdown
export function openDropdown(element, click = true) {
  var openDropDown = true;
  if (DropDownsVisible && click)
    openDropDown = false;
  closeDropdowns();
  if (openDropDown) {
    DropDownsVisible = true;
    document.getElementById(element.parentElement.children[1].id).classList.toggle("show");
  }
}

export function mouseoverDropdown(element) {
  if (DropDownsVisible)
    openDropdown(element, false);
}

export function closeDropdowns() {
  DropDownsVisible = false;
  var i;
  var dropdownBtns = document.getElementsByClassName("dropbtn");
  for (i = 0; i < dropdownBtns.length; i++) {
    dropdownBtns[i].blur();
  }
  var dropdowns = document.getElementsByClassName("dropdown-content");
  for (i = 0; i < dropdowns.length; i++) {
    var openDropdown = dropdowns[i];
    if (openDropdown.classList.contains('show')) {
      openDropdown.classList.remove('show');
    }
  }
}

window.onclick = function (event) {
  if (!event.target.matches('.dropbtn')) {
    closeDropdowns();
  }
}

export function showWaveform(show) {
  setShowWave(show);
  var myNode = document.getElementById("showWaveform");
  if (showWave)
    myNode.innerHTML = "&#x2713;" + myNode.text.substr(myNode.innerHTML.indexOf(" "), myNode.text.length);
  else
    myNode.innerHTML = "&#x2718;" + myNode.text.substr(myNode.innerHTML.indexOf(" "), myNode.text.length);
  document.cookie = "showWave=" + showWave + ";path=/; max-age=31536000";
  updateWaveColors();
  updateDropDownMenuCheckboxes();
}

export async function updateDropDownMenuCheckboxes() {
  var showWaveformNode = document.getElementById("showWaveform");
  if (showWave)
    showWaveformNode.innerHTML = "&#x2713;" + showWaveformNode.text.substr(showWaveformNode.innerHTML.indexOf(" "), showWaveformNode.text.length);
  else
    showWaveformNode.innerHTML = "&#x2718;" + showWaveformNode.text.substr(showWaveformNode.innerHTML.indexOf(" "), showWaveformNode.text.length);
  var showSpecNode = document.getElementById("showSpectrogram");
  if (showSpec)
    showSpecNode.innerHTML = "&#x2713;" + showSpecNode.text.substr(showSpecNode.innerHTML.indexOf(" "), showSpecNode.text.length);
  else
    showSpecNode.innerHTML = "&#x2718;" + showSpecNode.text.substr(showSpecNode.innerHTML.indexOf(" "), showSpecNode.text.length);
  var showListAnnotationsNode = document.getElementById("showAnnotationList");
  setShowListAnnotations(document.getElementById("_ListAnnotations") != null);
  document.cookie = "showListAnnotations=" + showListAnnotations + ";path=/; max-age=31536000";
  if (showListAnnotations)
    showListAnnotationsNode.innerHTML = "&#x2713;" + showListAnnotationsNode.text.substr(showListAnnotationsNode.innerHTML.indexOf(" "), showListAnnotationsNode.text.length);
  else
    showListAnnotationsNode.innerHTML = "&#x2718;" + showListAnnotationsNode.text.substr(showListAnnotationsNode.innerHTML.indexOf(" "), showListAnnotationsNode.text.length);
  var showListOverlaysNode = document.getElementById("showOverlayList");
  setShowListOverlays(document.getElementById("_ListOverlays") != null);
  var showListPluginsNode = document.getElementById("showPluginList");
  //setShowListPlugins(document.getElementById("_ListPlugins") != null);
  document.cookie = "showListOverlays=" + showListOverlays + ";path=/; max-age=31536000";
  document.cookie = "showListPlugins=" + showListPlugins + ";path=/; max-age=31536000";
  if (showListOverlays)
    showListOverlaysNode.innerHTML = "&#x2713;" + showListOverlaysNode.text.substr(showListOverlaysNode.innerHTML.indexOf(" "), showListOverlaysNode.text.length);
  else
    showListOverlaysNode.innerHTML = "&#x2718;" + showListOverlaysNode.text.substr(showListOverlaysNode.innerHTML.indexOf(" "), showListOverlaysNode.text.length);
  if (!showWave && !showSpec)
    document.getElementById("ViewDisabledInfo").style.display = "block";
  else
    document.getElementById("ViewDisabledInfo").style.display = "none";
  if (showListPlugins) {
    showListPluginsNode.innerHTML = "&#x2713;" + showListPluginsNode.text.substr(showListPluginsNode.innerHTML.indexOf(" "), showListPluginsNode.text.length);
    await loadPluginList();
  }
  else
    showListPluginsNode.innerHTML = "&#x2718;" + showListPluginsNode.text.substr(showListPluginsNode.innerHTML.indexOf(" "), showListPluginsNode.text.length);
  await updateOverlays();
}

export async function saveFile() {
  function checkUpdateComplete() {
    if (blnFileUploadFinished && blnAnnotationUploadFinished) {
      setDeletedRegions([]);
      setAudioSignalHasChanges(false);
      setFilePreferenceChanges(false);
      document.getElementById("progress-bar").style.display = 'none';
      document.getElementById("hideWavesurfer").style.visibility = "hidden";
    }
  }
  async function saveActions(reloadFile) {
    if (actions.length) {
      if (UserSettings["allowAnnotationTags"]) {
        const saveAnnotations = new XMLHttpRequest();
        saveAnnotations.responseType = 'json';
        saveAnnotations.open('POST', '/saveAnnotations');
        saveAnnotations.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        saveAnnotations.onreadystatechange = async () => {
          if (saveAnnotations.readyState == XMLHttpRequest.DONE && saveAnnotations.response) {
            var item = JSON.parse(saveAnnotations.response);
            if (item.succsess) {
              blnAnnotationUploadFinished = true;
              checkUpdateComplete();
              if (reloadFile)
                loadFile(DataSet.FilePreferences.id);
              else {
                wavesurfer.regions.clear();
                wavesurfer.markers.clear();
                loadAnnotations();
              }
            }
            else {
              alert("Error saving annotations! Please check Server!")
              document.getElementById("hideWavesurfer").style.visibility = "hidden";
            }
          }
        }
        saveAnnotations.send(JSON.stringify(actions));
      }
      else
        alert("Permission for storing Annotations not set!");
    }
    else {
      checkUpdateComplete();
      if (reloadFile)
        loadFile(DataSet.FilePreferences.id)
    }
  }

  var reloadFile = false;
  var blnFileUploadFinished = !DataSet.audioSignalHasChanges;
  var actions = [];
  if (wavesurfer && wavesurfer.regions) {
    Object.keys(wavesurfer.regions.list).forEach(element => {
      if (wavesurfer.regions.list[element].hasChanges) {
        var region = wavesurfer.regions.list[element];
        if (region.markerLabel && region.markerLabel.textContent) {
          var item = { tag_id: region.tag_id, value: region.markerLabel.textContent, start: region.start, end: region.end, action: "modified" }
          if (region.annotation_id)
            item.id = region.annotation_id
          else
            item.action = "added"
          actions.push(item);
        }
      }
    });
    for (var idx = 0; idx < DataSet.deletedRegions.length; idx++) {
      var item = { id: DataSet.deletedRegions[idx], action: "deleted" }
      actions.push(item);
    }
    var blnAnnotationUploadFinished = (actions.length == 0);
    if (!blnFileUploadFinished || !blnAnnotationUploadFinished) {
      document.querySelector('[data-action="saveFile"]').blur();
      document.getElementById("hideWavesurfer").style.visibility = "visible";
      document.getElementById("progress-bar").style.display = 'block';
      await new Promise(resolve => setTimeout(resolve, 0));
    }
  }
  else if (wavesurfer)
    setAudioSignalHasChanges(true);
  if (UserSettings["allowModify"] && (DataSet.audioSignalHasChanges || DataSet.filePreferenceChanges)) {
    let request = new XMLHttpRequest();
    let data = new FormData();
    if (DataSet.audioSignalHasChanges) {
      var audioFile = bufferToWave(wavesurfer.backend.buffer, 0, wavesurfer.backend.buffer.length);
      let audioFileData = await fetch(audioFile).then(r => r.blob());
      data.append("audiofile", audioFileData, DataSet.FilePreferences.filename);
    }
    data.append("FilePreferences", JSON.stringify(DataSet.FilePreferences));
    request.open('POST', '/upload', true);

    request.upload.addEventListener('progress', function (e) {
      let percent_completed = (e.loaded / e.total) * 100;
      document.getElementById("progress-bar").querySelector('.progress-bar').style.width = Math.round(percent_completed) + '%';
    });
    request.addEventListener('load', async function (e) {
      blnFileUploadFinished = true;
      if (!DataSet.FilePreferences.id)
        reloadFile = true
      DataSet.FilePreferences.id = JSON.parse(await e.target.response)['FileId']
      saveActions(reloadFile)
    });
    request.send(data);
    return;
  }
  else if (UserSettings["allowModify"] == false) 
    alert("Permission for storing audio file modifications not set!");
  saveActions(false);
}

export function exportAnnotations() {
  var a = window.document.createElement('a');
  var content = ""
  Object.keys(wavesurfer.regions.list).forEach(element => {
    var region = wavesurfer.regions.list[element];
    content += region.start + "\t" + region.end + "\t" + region.markerLabel.textContent + "\n";
  });
  a.href = window.URL.createObjectURL(new Blob([content], { type: "text/plain;charset=utf-8" }));
  a.download = DataSet.FilePreferences.filename.replace(".", "_") + "_annotations_" + new Date(Date.now()).toISOString().substr(0, 10) + ".txt";
  document.body.appendChild(a);
  a.click();
  document.body.removeChild(a);
}

var AnnotationsFileReader = new FileReader();
AnnotationsFileReader.onload = async function () {
  var rows = AnnotationsFileReader.result.split("\n");
  for (var idx = 0; idx < rows.length; idx++) {
    var items = rows[idx].split('\t');
    if (items.length == 3 && !isNaN(items[0]) && !isNaN(items[1])) {
      var item = [];
      var start = new Date(Number(items[0].trim()) * 1000)
      var end = new Date(Number(items[1].trim()) * 1000)
      item.start = start.getUTCHours() + ":" + start.getUTCMinutes() + ":" + start.getUTCSeconds() + "." + start.getUTCMilliseconds();
      item.end = end.getUTCHours() + ":" + end.getUTCMinutes() + ":" + end.getUTCSeconds() + "." + end.getUTCMilliseconds();
      item.color = null;
      item.name = items[2].trim();
      item.tag_id = null;
      var blnAdd = UserSettings["allowManualAnnotationTags"];
      if (UserSettings["allowManualAnnotationTags"] == false) {
        for (var idxTags = 0; idxTags < DataSet.tagList.length; idxTags++) {
          blnAdd = blnAdd || (item.value == DataSet.tagList[idxTags].value)
        }
      }
      if (Number(items[1]) > wavesurfer.backend.buffer.duration)
        blnAdd = false;
      if (blnAdd && UserSettings["allowAnnotationTags"]) {
        var region = await addImportedAnnotation(item);
        region.hasChanges = true;
      }
      setAudioSignalHasChanges(DataSet.audioSignalHasChanges);
    }
  }
}

export function ImportAnnotations() {
  var input = document.createElement("input");
  input.type = "file";
  input.click();
  input.addEventListener('change', e => {
    AnnotationsFileReader.readAsText(input.files[0]);
  }, false);
  return false;
}

export function openFile() {
  if (getPluginWindows().length == 0) {
    wavesurfer.pause();
    closeDropdowns();
    var proceed = true;
    var hasChanges = (DataSet.deletedRegions.length > 0);
    Object.keys(wavesurfer.regions.list).forEach(element => {
      hasChanges = hasChanges || wavesurfer.regions.list[element].hasChanges;
    });
    if (hasChanges)
      proceed = confirm("You have unsaved modifications! Continue?");
    if (proceed) {
      var PluginName = "loadFileWindow";
      createPopUpWindow(PluginName, "Load File", true, false)
      var divWindow = document.getElementById(PluginName);
      var divContent = document.getElementById(PluginName + "_content");
      var divControls = document.getElementById(PluginName + "_controls");
      divControls.style.display = "hidden";
      divWindow.setEnable(true);
      document.querySelector('[data-action="openFile"]').blur();
      document.querySelector('#progress-bar').style.display = 'none';
      loadFilesAndFolders();
    }
  }
}

export async function loadFile(id) {
  document.getElementById("hideWavesurfer").style.visibility = "visible";
  document.querySelector('#progress-bar').style.display = 'block';
  //document.getElementsByTagName("timelinewaveform")[0].style.visibility = "hidden";
  await new Promise(resolve => setTimeout(resolve, 0));
  wavesurfer.regions.clear();
  wavesurfer.markers.clear();
  if (showListAnnotations)
    showAnnotationList();
  if (wavesurfer.spectrogram) {
    wavesurfer.destroyPlugin("spectrogram");
    wavesurfer.spectrogram = null;
  }
  closePlugin("loadFileWindow");
  setOverlays({});
  updateOverlayList();
  setDeletedRegions([]);
  DataSet.FilePreferences.id = id;
  document.cookie = "FileId=" + id + ";path=/; max-age=31536000";
  const setCurrentFile = new XMLHttpRequest();
  setCurrentFile.responseType = 'json';
  setCurrentFile.open('POST', '/setCurrentFile');
  setCurrentFile.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  setCurrentFile.onreadystatechange = () => {
      if (setCurrentFile.readyState == XMLHttpRequest.DONE && setCurrentFile.response) {
          wavesurfer.load("download");
      }
  }
  setCurrentFile.send(JSON.stringify({file: id}));
}

function loadFilesAndFolders() {
  const FilesAndFolders = fetch('/loadFilesAndFolders', {
    headers: { 'Content-Type': 'application/json' },
    credentials: 'same-origin'
  })
    .then((response) => response.json())
    .then((FilesAndFolders) => { return FilesAndFolders; })
  const FilesAndFoldersLoaded = async () => {
    showTree(JSON.parse(await FilesAndFolders), [0])
  };
  FilesAndFoldersLoaded();
}

function showTree(tree, level, fileIdx = -1) {
  var PluginName = "loadFileWindow";
  var divContent = document.getElementById(PluginName + "_content");
  divContent.innerHTML = "";
  var tempTree = JSON.parse(JSON.stringify(tree))
  if (tempTree.folders.length == 0 && tempTree.files.length == 0) {
    divContent.innerHTML = "<br><b>No audio files available on server storage! You can close this message and import an audiofile via File-menu...</b>";
    divContent.style.textAlign = "center";
  }
  else {
    for (var idx = 1; idx < level.length; idx++) {
      tempTree = tempTree.folders[level[idx]];
    }
    if (!tempTree.folders)
      tempTree.folders = [];
    if (level.length > 1 || fileIdx >= 0)
      tempTree.folders.unshift({ "name": ".." })
    if (tempTree.folders) {
      for (var idx = 0; idx < tempTree.folders.length; idx++) {
        var folder = tempTree.folders[idx];
        var a = window.document.createElement('a');
        a.classList.add('glyphicon');
        a.classList.add('glyphicon-folder-open');
        a.classList.add('folderItem');
        a.level = JSON.parse(JSON.stringify(level));
        if (folder.name != "..")
          a.level.push(idx);
        else
          a.level.pop();
        a.addEventListener("dblclick", function () { showTree(tree, this.level) });
        a.text = " " + folder.name;
        divContent.appendChild(a);
      }
    }
    if (tempTree.files) {
      var files = tempTree.files;
      if (fileIdx >= 0)
        files = tempTree.files[fileIdx].fileParts;
      for (var idx = 0; idx < files.length; idx++) {
        var file = files[idx];
        var a = window.document.createElement('a');
        a.classList.add('folderItem');
        a.id = file.id;
        a.level = level
        a.filename = file.name;
        if (file.hasOwnProperty("fileParts") && file.fileParts.length == 1) {
          a.id = file.fileParts[0].id;
          a.addEventListener("dblclick", async function () { await loadFile(this.id) });
        }
        else if (fileIdx >= 0) {
          a.addEventListener("dblclick", async function () { await loadFile(this.id) });
        }
        else {
          a.idx = idx;
          a.addEventListener("dblclick", function () { showTree(tree, this.level, this.idx) });
        }
        a.style.display = "flex";
        a.innerHTML = "<div style='flex: 1; overflow-x: hidden; overflow-y: hidden;' class='glyphicon glyphicon-file'>" + file.name + "</div>";
        if (!file.isPublic)
          a.innerHTML += "<span style='width:20px'><i class=\"glyphicon glyphicon-lock\"></span>"
        a.innerHTML += "<span style='width:40px'>" + file.channels + "</span>"
        a.innerHTML += "<span style='width:100px'>" + new Date(file.duration * 1000).toISOString().substr(11, 12) + "</span>";
        divContent.appendChild(a);
      }
    }
  }
}

export async function dropHandler(ev) {
  ev.preventDefault();
  ev.stopPropagation();
  if (ev.dataTransfer.items) {
    var files = [];
    for (var i = 0; i < ev.dataTransfer.items.length; i++) {
      if (ev.dataTransfer.items[i].kind === 'file') {
        var file = ev.dataTransfer.items[i].getAsFile();
        if ((file.name.toLowerCase().startsWith("psd_") && file.name.endsWith(".feat")) || (ev.dataTransfer.items[i].type == "audio/wav" || ev.dataTransfer.items[i].type == 'audio/mpeg' || ev.dataTransfer.items[i].type == 'audio/ogg')) {
          files.push(file)
        }
        else {
          var file = ev.dataTransfer.items[i].getAsFile();
          AnnotationsFileReader.readAsText(file);
        }
      }
    }
    if (files.length)
      await importAudiofile(files);
  }
  return false;
}

export function dragOverHandler(ev) {
  ev.preventDefault();
  ev.stopPropagation();
}

export async function showAnnotationList() {
  var PluginName = "_ListAnnotations";
  var divWindow = document.getElementById(PluginName);
  if (!divWindow) {
    createPopUpWindow(PluginName, "Annotation-List", false, false)
    divWindow = document.getElementById(PluginName);
    divWindow.setEnable(true);
    dockWindow(divWindow, "right");
    var divControls = document.getElementById(PluginName + "_controls");
    divControls.style.display = "hidden";
  }
  var divContent = document.getElementById(PluginName + "_content");
  divContent.innerHTML = "";
  var regionList = [];
  Object.keys(wavesurfer.regions.list).forEach(element => {
    var region = wavesurfer.regions.list[element];
    regionList.push(region);
  });
  regionList.sort((a, b) => a.start - b.start)
  regionList.forEach((region) => {
    if (region.marker) {
      var a = window.document.createElement('a');
      a.region = region;
      a.addEventListener("click", function (e) {
        e.preventDefault();
        wavesurfer.fireEvent('region-click', this.region, e);
        return false;
      });
      a.addEventListener("dblclick", function (e) {
        e.preventDefault();
        wavesurfer.fireEvent('region-dblclick', this.region, e);
        return false;
      });
      a.classList.add('folderItem');
      a.classList.add('task');
      a.id = "short"
      region.shortLink = a;
      updateShortLink(region);
      divContent.appendChild(a);
    }
  });
  await fitWindowSize();
  if (wavesurfer.drawer.canvases.length && wavesurfer.drawer.canvases[0].wave && wavesurfer.drawer.canvases[0].wave.width < wavesurfer.container.offsetWidth)
    wavesurfer.zoom(zoomLevel);
}

export async function showOverlayList() {
  var PluginName = "_ListOverlays";
  var divWindow = document.getElementById(PluginName);
  if (!divWindow) {
    createPopUpWindow(PluginName, "Overlay-List", false, false)
    divWindow = document.getElementById(PluginName);
    divWindow.setEnable(true);
    dockWindow(divWindow, "right");
    var divControls = document.getElementById(PluginName + "_controls");
    divControls.style.display = "hidden";
  }
  updateOverlayList();
  await fitWindowSize();
  if (wavesurfer.drawer.canvases.length && wavesurfer.drawer.canvases[0].wave && wavesurfer.drawer.canvases[0].wave.width < wavesurfer.container.offsetWidth)
    wavesurfer.zoom(zoomLevel);
}

export function updateOverlayList() {
  if (showListOverlays) {
    var PluginName = "_ListOverlays";
    var divContent = document.getElementById(PluginName + "_content");
    var scrollPos = divContent.scrollTop;
    divContent.innerHTML = "";
    var keys = Object.keys(DataSet.Overlays).sort();
    for (const key of keys) {
      var div = window.document.createElement('div');
      div.style.display = "flex";

      var a = window.document.createElement('a');
      a.parent = key;
      if ((DataSet.Overlays[key].domain == "time" && DataSet.Overlays[key].visible) || (wavesurfer.spectrogram && DataSet.Overlays[key].domain == "frequency" && DataSet.Overlays[key].visible))
        a.style.color = "white";
      else
        a.style.color = "gray";
      a.style.overflowX = "hidden";
      a.addEventListener("click", function (e) {
        e.preventDefault();
        return false;
      });
      a.addEventListener("dblclick", async function (e) {
        e.preventDefault();
        if (DataSet.Overlays.hasOwnProperty(this.parent)) {
          DataSet.Overlays[this.parent].visible = !DataSet.Overlays[this.parent].visible;
          if (!DataSet.Overlays[this.parent].visible)
            wavesurfer.drawBuffer();
          else
            await updateOverlays();
        }
        else if (DataSet.Overlays.hasOwnProperty(this.parent) && wavesurfer.spectrogram) {
          DataSet.Overlays[this.parent].visible = !DataSet.Overlays[this.parent].visible;
          if (!DataSet.Overlays[this.parent].visible)
            wavesurfer.spectrogram.drawSpectrogram(wavesurfer.spectrogram.frequencies, wavesurfer.spectrogram);
          await updateOverlays();
        }
        return false;
      });
      a.style.flex = 1;
      a.style.overflowX = "hidden";
      a.style.display = "block"
      a.classList.add('folderItem');
      a.classList.add('task');
      a.id = "overlay";
      a.innerHTML = key;
      div.appendChild(a);

      a = window.document.createElement('a');
      a.parent = key;

      if (key.includes(" #"))
        a.style.color = "white";
      else
        a.style.color = "gray";
      a.addEventListener("click", function (e) {
        e.preventDefault();
        return false;
      });
      a.addEventListener("dblclick", function (e) {
        e.preventDefault();
        var parentKey = this.parent;
        var highestNumer = 0;
        if (this.parent.includes(" #"))
          parentKey = this.parent.substr(0, this.parent.indexOf(" #"))
        for (const key of Object.keys(DataSet.Overlays)) {
          if (key.startsWith(parentKey + " #"))
            highestNumer = parseInt(key.substr(key.indexOf(" #") + 2))
        }
        if (!this.parent.includes(" #")) {
          DataSet.Overlays[this.parent + " #" + (highestNumer + 1)] = DataSet.Overlays[this.parent];
          delete DataSet.Overlays[this.parent];
        }
        else {
          if (parentKey != this.parent && !Object.keys(DataSet.Overlays).includes(parentKey))
            DataSet.Overlays[parentKey] = DataSet.Overlays[this.parent]
          delete DataSet.Overlays[this.parent];
        }
        updateOverlayList();
        return false;
      });
      a.innerHTML = "<i class='glyphicon glyphicon-lock'></i>";
      div.appendChild(a);

      divContent.appendChild(div);
    }
    divContent.scrollTop = scrollPos;
  }
}

export function updateShortLink(region) {
  if (region.shortLink && document.getElementById("_ListAnnotations") != null) {
    var colors = region.markerColor.split(',');
    region.shortLink.style.color = region.color.replace(colors[colors.length - 1], "1.0)")
    if (region == selectedRegion)
      region.shortLink.style.backgroundColor = "rgba(50, 50, 50, 0.5)";
    else
      region.shortLink.style.backgroundColor = "";
    region.shortLink.style.display = "flex";
    region.shortLink.innerHTML = "<div style='flex: 1; overflow-x: hidden;'>" + region.markerLabel.textContent + "</div><span>" + new Date(region.start * 1000).toISOString().substr(11, 12) + "&nbsp;&nbsp;</span>" + "<span>" + new Date(region.end * 1000).toISOString().substr(11, 12) + "&nbsp;</span>";
  }
}

export async function fitWindowSize() {
  if (wavesurfer) {
    await new Promise(resolve => setTimeout(resolve, 0));
    wavesurfer.markers._updateMarkerPositions();
    //wavesurfer.minimap.params.barHeight = 
    wavesurfer.minimap.render();
    /*
    wavesurfer.minimap.ratio = wavesurfer.drawer.width / wavesurfer.minimap.drawer.width;
    if (wavesurfer.drawer.width < wavesurfer.minimap.drawer.width)
      wavesurfer.minimap.ratio = 1 / wavesurfer.minimap.ratio;
    wavesurfer.minimap.overviewWidth = wavesurfer.drawer.container.offsetWidth / wavesurfer.minimap.ratio;
    wavesurfer.minimap.overviewRegion.style.width = wavesurfer.minimap.overviewWidth + "px";
    */
    if (wavesurfer.spectrogram) {
      if (Number(document.getElementById("wave-spectrogram").childNodes[0].style.width.replace("px", "")) != wavesurfer.drawer.container.offsetWidth) {
        document.getElementById("wave-spectrogram").childNodes[0].style.width = wavesurfer.drawer.container.offsetWidth + "px";
      }
      /*
      wavesurfer.spectrogram.canvas.style.height = wavesurfer.container.offsetHeight + "px"
      wavesurfer.spectrogram.canvas.style.width = Math.max(wavesurfer.drawer.getWidth(), Math.round(wavesurfer.getDuration() * wavesurfer.params.minPxPerSec * wavesurfer.params.pixelRatio)) + "px"
      wavesurfer.spectrogram.canvas.parentElement.style.height = wavesurfer.container.offsetHeight + "px"
      wavesurfer.spectrogram.canvas.parentElement.getElementsByClassName("spec-labels")[0].style.height = wavesurfer.container.offsetHeight + "px";
      */
    }
    //if (wavesurfer.container.offsetHeight > window.innerHeight / 2)
    //document.getElementById("MidLine").style.top = (wavesurfer.container.offsetHeight / 2 + document.getElementById("timeline").offsetHeight - 2) + "px";
  }
}

export function exportAudiofile() {
  if (wavesurfer && wavesurfer.backend && wavesurfer.backend.buffer) {
    var a = window.document.createElement('a');
    a.href = bufferToWave(wavesurfer.backend.buffer, 0, wavesurfer.backend.buffer.length);
    var filenameTemp = DataSet.FilePreferences.filename;
    if (!filenameTemp)
      filenameTemp = "File.wav";
    var items = filenameTemp.split(".");
    a.download = new Date(Date.now()).toISOString().substr(0, 10) + "_" + filenameTemp.replace("." + items[items.length - 1], ".wav");
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  }
}

export async function importAudiofile(file = "") {
  async function readAsBlob(file) {
    let result = await new Promise((resolve) => {
      let reader = new FileReader();
      reader.onload = (e) => resolve(reader.result);
      return reader.readAsArrayBuffer(file);
    });
    return new window.Blob([new Uint8Array(result)]);
  }
  async function loadBlobIntowavesurfer(blob) {
    wavesurfer.loadBlob(blob)
    const data = await new Promise((resolve) => {
      wavesurfer.on('ready', (e) => resolve(true))
    });
    return data
  }
  async function convertFeatureFileToWav(files) {
    return new Promise(async function (resolve, reject) {
      let request = new XMLHttpRequest();
      request.responseType = 'arraybuffer'
      let data = new FormData();
      for (idx in files)
        data.append("featureFile", file[idx], file[idx].name);
      request.open('POST', '/convertFeatureFileToWav', true);
      request.upload.addEventListener('progress', function (e) {
        let percent_completed = (e.loaded / e.total) * 100;
        document.getElementById("progress-bar").querySelector('.progress-bar').style.width = Math.round(percent_completed) + '%';
      });
      request.onload = function () {
        var status = request.status;
        if (status == 200) {
          var file = new File([request.response], DataSet.FilePreferences.filename.replace(".feat", ".wav"), {type: "audio/mpeg"});
          var objectUrl = URL.createObjectURL(file);
          resolve(objectUrl);
        } else {
          reject(status);
        }
      };
      document.getElementById("hideWavesurfer").style.visibility = "visible";
      document.getElementById("progress-bar").style.display = 'block';
      await new Promise(resolve => setTimeout(resolve, 0));
      request.send(data);
    });
  }

  if (file == "") {
    var input = document.createElement("input");
    input.type = "file";
    input.click();
    input.addEventListener('change', async ev => {
      for (var i = 0; i < input.files.length; i++) {
        if ((input.files[i].name.toLowerCase().startsWith("psd_") && input.files[i].name.endsWith(".feat")) || input.files[i].type == "audio/wav" || input.files[i].type == "audio/x-wav" || input.files[i].type == "audio/vnd.wave" || input.files[i].type == "audio/wave" || input.files[i].type == 'audio/mpeg' || input.files[i].type == 'audio/ogg') {
          if (input.files[i].hasOwnProperty("getAsFile"))
            await importAudiofile(input.files[i].getAsFile());
          else
            await importAudiofile(input.files[i]);
        }
      }
    }, false);
    return false;
  }
  else {
    clearFilePreferences();

    DataSet.FilePreferences.filename = ""
    if (!Array.isArray(file))
      file = [file]
    for (var idx in file)
      DataSet.FilePreferences.filename += file[idx].name.replace(".feat", "") + "_";
    DataSet.FilePreferences.filename = DataSet.FilePreferences.filename.substr(0, DataSet.FilePreferences.filename.length -1 )
    await new Promise(resolve => setTimeout(resolve, 0));
    if (!wavesurfer) {
      setWavesurfer(WaveSurfer.create({ container: '#waveform', backend: 'WebAudio', splitChannelsOptions: {} }));
      if (file.name.toLowerCase().startsWith("psd_") && file.name.endsWith(".feat"))
        await loadBlobIntowavesurfer(await readAsBlob(await convertFeatureFileToWav(file)));
      else
        await loadBlobIntowavesurfer(await readAsBlob(file));
    }
    else {
      if (typeof(file) === "string")
        file = [file];
      if (file[0].name.toLowerCase().startsWith("psd_") && file[0].name.endsWith(".feat"))
        wavesurfer.load(await convertFeatureFileToWav(file))
      else
        wavesurfer.loadBlob(await readAsBlob(file[0]));
    }
    setAudioSignalHasChanges(true);
  }
}

export async function newFile() {
  var windowId = "Insert_Signal";
  document.getElementById(windowId + "_MenuLink").click();
  var divWindow = document.getElementById(windowId);
  divWindow.onclose = function (event) {
    closePlugin(this.id);
    document.getElementById("btnRecord").disabled = (getPluginWindows().length != 0);
  };
  var closeLink = document.getElementById(windowId + "_closeLink").cloneNode(true);
  var divHeader = document.getElementById(windowId + "header");
  closeLink.parent = windowId;
  closeLink.onclick = function (event) {
    var divWindow = document.getElementById(this.parent);
    if (divWindow.onclose)
      divWindow.onclose(this.parent);
  }
  divHeader.innerHTML = "New Signal";
  divHeader.appendChild(closeLink);
  var divContent = document.getElementById(windowId + "_content");
  var divControls = document.getElementById(windowId + "_controls");
  //document.getElementById(divWindow.id + "PresetControls").style.display = "none"; 
  document.getElementById(windowId + "Apply").style.display = "none";
  var btnOK = document.getElementById(windowId + "OK");
  btnOK.removeEventListener("click", onClick_OK);
  btnOK.addEventListener("click", async function (e) {
    var divWindow = document.getElementById(this.id.replace("OK", ""));
    divWindow.setEnable(false);
    var tempBuffer = wavesurfer.backend.ac.createBuffer(2, PluginParams[divWindow.durationValueName] * wavesurfer.backend.buffer.sampleRate, wavesurfer.backend.buffer.sampleRate);
    clearFilePreferences();
    wavesurfer.loadDecodedBuffer(tempBuffer);
    await initProcessing(this.PluginName, sources)
    setAudioSignalHasChanges(true);
  });
}

export function showFilePreferences() {
  var PluginName = "_filePreferences";
  createPopUpWindow(PluginName, "File Parameters", true, true)
  var divWindow = document.getElementById(PluginName);
  var divContent = document.getElementById(PluginName + "_content");
  var divControls = document.getElementById(PluginName + "_controls");
  divWindow.setEnable(true);
  document.querySelector('#progress-bar').style.display = 'none';
  divWindow.FilePreferences_orig = Object.assign({}, DataSet.FilePreferences);
  updateFilePreferences();
  var controls = [
    { type: "text", label: "Filename", property: "filename", object: DataSet.FilePreferences },
    { type: "display", label: "Duration / s", property: "duration", object: DataSet.FilePreferences },
    { type: "display", label: "Channels", property: "channels", object: DataSet.FilePreferences },
    { type: "display", label: "Samplerate / Hz", property: "samplingRate", object: DataSet.FilePreferences },
    { type: "checkbox", label: "isPublic", property: "isPublic", object: DataSet.FilePreferences, enabled: UserSettings["allowChangeProtection"] || DataSet.FilePreferences.isOwner }]
  divWindow.gui.Register(controls);
  divWindow.onclose = function (event) {
    for (const [key, value] of Object.entries(DataSet.FilePreferences)) {
      if (DataSet.FilePreferences[key] != this.FilePreferences_orig[key])
        setFilePreferenceChanges(true);
    }
  }
}

export async function showSystemPreferences() {
  var PluginName = "_SystemPreferences";
  createPopUpWindow(PluginName, "Preferences", true, true)
  var divWindow = document.getElementById(PluginName);
  var divContent = document.getElementById(PluginName + "_content");
  var divControls = document.getElementById(PluginName + "_controls");
  divWindow.setEnable(true);
  document.querySelector('#progress-bar').style.display = 'none';
  SystemPreferences.frequencyRange[1] = Math.min(SystemPreferences.frequencyRange[1], wavesurfer.backend.buffer.sampleRate / 2);
  SystemPreferences.frequencyRange[0] = Math.min(SystemPreferences.frequencyRange[0], SystemPreferences.frequencyRange[1]);
  var controls = [
    { type: "title", label: "Time Domain" },
    { type: "color", label: "Wave Color", property: "wavecolor", object: SystemPreferences, format: "rgb" },
    { type: "color", label: "Progress Color", property: "progresscolor", object: SystemPreferences, format: "rgb" },
    { type: "title", label: "Frequency Domain" },
    { type: "interval", label: "Frequency Range / Hz", property: "frequencyRange", object: SystemPreferences, min: 0, max: wavesurfer.backend.buffer.sampleRate / 2, step: 10, precision: 1 },
    { type: "select", label: "FFT Size", property: "fftSize", object: SystemPreferences, options: [64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768] },
    { type: "select", label: "Window", property: "window", object: SystemPreferences, options: ["bartlett", "bartlettHann", "blackman", "cosine", "gauss", "hamming", "hann", "rectangular"] },
    { type: "range", label: "Overlap", property: "noverlap", object: SystemPreferences, min: 0, max: 1, step: .01, precision: 2 },
    { type: "select", label: "Colormap", property: "colormap", object: SystemPreferences, options: ['Viridis', 'Plasma', 'Inferno', 'Magma', 'Cividis', 'Gray', 'Bone', 'Pink', 'Spring', 'Summer', 'Autumn', 'Winter', 'Cool', 'Wistia', 'Hot', 'Afmhot', 'Gist_heat', 'Copper'] },
    { type: "checkbox", label: "Pre-Emphasis", property: "preEmphasis", object: SystemPreferences }
  ]
  divWindow.gui.Register(controls);
  var btn = window.document.createElement('button');
  btn.id = divWindow.id + "OK";
  btn.innerText = "OK";
  btn.className = 'btn btn-primary subwindowcontrols_btn';
  btn.style.float = "right";
  btn.PluginName = PluginName;
  divControls.appendChild(btn);
  btn.addEventListener("click", async function (event) {
    setColorMap(await getColormap({ colormap: SystemPreferences.colormap }));
    wavesurfer.setWaveColor(SystemPreferences.wavecolor);
    wavesurfer.setProgressColor(SystemPreferences.progresscolor);
    wavesurfer.minimap.params.waveColor = SystemPreferences.wavecolor;
    wavesurfer.minimap.params.progressColor = SystemPreferences.progresscolor;
    for (var idx = 0; idx < wavesurfer.backend.buffer.numberOfChannels; idx++) {
      wavesurfer.setWaveColor(SystemPreferences.wavecolor, idx);
      wavesurfer.setProgressColor(SystemPreferences.progresscolor, idx);
      wavesurfer.minimap.params.splitChannelsOptions.channelColors[idx].waveColor = SystemPreferences.wavecolor;
      wavesurfer.minimap.params.splitChannelsOptions.channelColors[idx].progressColor = SystemPreferences.progresscolor;
    }
    if (showSpec) {
      await hideSpectrogram();
      await showSpectrogram()
      //await fitWindowSize();
    }
    document.cookie = "SystemPreferences=" + JSON.stringify(SystemPreferences) + ";path=/; max-age=31536000";
    closePlugin(this.PluginName);
  });
}