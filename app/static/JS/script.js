// https://developer.mozilla.org/en-US/docs/Web/API/AudioWorkletNode
// https://codepen.io/oxmanroman/pen/wywzRo

import { fitWindowSize, showAnnotationList, showOverlayList, loadFile, openFile, saveFile, openDropdown, mouseoverDropdown, closeDropdowns, showWaveform, ImportAnnotations, exportAnnotations, exportAudiofile, dropHandler, dragOverHandler, newFile, importAudiofile, updateDropDownMenuCheckboxes, showFilePreferences, showSystemPreferences } from "./dropdown.js";
import { initAnnotation, selectedRegion, loadAnnotations, setSelectedRegion, unselectRegions, deleteAnnotation, enableAnnotation, addAnnotation, addImportedAnnotation, selectRegion, setDeletedRegions, treeselect } from "./annotation.js";
import { hideSpectrogram, showSpectrogram } from "./spectrogram.js";
import { closePlugin, createPopUpWindow, getPluginWindows, loadPluginList, PluginParamsStructured, receiveParams } from "./plugins.js";
import { pasteAudioData, cutCopyAudioData, recordFromMicrofone } from "./actions.js";
import { undo, redo, clearHistory } from "./history.js";
import { initResize } from "./resizeWindow.js";
import { clearLastSettings, lastSettings, updateOverlays, updateLastSettings, fromSource, wolaClientProcess, initProcessing, sources, nextWavesurferBufferBlock, processingInfo } from "./processing.js";
import { initIndexedDB } from "./IndexedDB.js";


export var gray1 = "#8F9092";
export var gray2 = "#e1e3e0";

export var UserSettings = {};
export var History = { Items: [], Idx: -1 };
export var DataSet = { id: 0, audiodata: null, FilePreferences: null, audioSignalHasChanges: false, tagList: null, deletedRegions: [], filePreferenceChanges: false, Overlays: {}, description: null, annotations: [] };
export var SystemPreferences = { databaseName: "InnovationPlus", frequencyRange: [0, 8000], colormap: "Viridis", preEmphasis: true, preEmphasisFrequency: 200, wavecolor: "#e30613", progresscolor: gray1, fftSize: 1024, window: "hann", noverlap: 0.5 };
SystemPreferences = Object.assign({}, SystemPreferences, JSON.parse(getCookie("SystemPreferences", null)));

export var Gains = [1, 1];
var ShiftPressed = false;
var DataSetListWindow = null;
var DataLoggerWindow = null;
var zoomFaktor = 1.2;
export var wavesurfer = null;
export function setWavesurfer(value) { wavesurfer = value; }
export var socket = null;
export var refreshSpetrogram = true;
export function setRefreshSpetrogram(value) { refreshSpetrogram = value; if (value && showSpec && wavesurfer.spectrogram) wavesurfer.spectrogram.render(); }
export var showWave = getCookie("showWave", "true") == "true";
export function setShowWave(value) { showWave = value; }
export var showSpec = getCookie("showSpec", "false") == "true";
export function setShowSpec(value) { showSpec = value; document.cookie = "showSpec=" + showSpec; }
export var showListAnnotations = getCookie("showListAnnotations") == "true";
export function setShowListAnnotations(value) {
    showListAnnotations = value;
}
export var showListOverlays = getCookie("showListOverlays") == "true";
export function setShowListOverlays(value) { showListOverlays = value; }
export var showListPlugins = getCookie("showListPlugins") == "true";
export function setShowListPlugins(value) { showListPlugins = value; }
export var colorMap = null
export function setColorMap(value) { colorMap = value; }
export var zoomLevel = Number(getCookie("zoomLevel", "20"));
export var zoomLevelVertical = Number(getCookie("zoomLevelVertical", "1"));
export function setZoomLevel(value) { zoomLevel = value; }
export function setZoomLevelVertical(value) { zoomLevelVertical = value; }
export var CtrlPressed = false;
export function clearFilePreferences() { updateFilePreferences(true); }
clearFilePreferences();
export function updateFilePreferences(clear = false) {
    var temp = Object.assign({}, DataSet.FilePreferences);
    DataSet.FilePreferences = { id: null, filename: null, isPublic: null, duration: null, samplingRate: null, channels: null, isOwner: null };
    if (clear == false) {
        if (wavesurfer && wavesurfer.backend && wavesurfer.backend.buffer) {
            DataSet.FilePreferences.duration = Math.round(wavesurfer.backend.buffer.duration * 100) / 100;
            DataSet.FilePreferences.samplingRate = wavesurfer.backend.buffer.sampleRate;
            DataSet.FilePreferences.channels = wavesurfer.backend.buffer.numberOfChannels;
        }
        for (const [key, value] of Object.entries(temp)) {
            if (value)
                DataSet.FilePreferences[key] = value;
        }
    }
}
export var buttonZoomIn = document.querySelector('[data-action="zoom-in"]');
export var buttonZoomOut = document.querySelector('[data-action="zoom-out"]');
export var buttonZoomVerticalIn = document.createElement('button');
export var buttonZoomVerticalOut = document.createElement('button');
export var lastClickTime = null;
export function setLastClickTime(value) { lastClickTime = value; }
export function setFilePreferenceChanges(value) { DataSet.filePreferenceChanges = value; setAudioSignalHasChanges(DataSet.audioSignalHasChanges); }
export function setAudioSignalHasChanges(value) {
    DataSet.audioSignalHasChanges = value;
    if (wavesurfer.regions) {
        var hasChanges = (DataSet.deletedRegions.length > 0) || DataSet.audioSignalHasChanges || DataSet.filePreferenceChanges;
        Object.keys(wavesurfer.regions.list).forEach(element => {
            hasChanges = hasChanges || wavesurfer.regions.list[element].hasChanges;
        });
        if (hasChanges)
            document.querySelector('[data-action="saveFile"]').style.color = 'white';
        else
            document.querySelector('[data-action="saveFile"]').style.color = 'black';
    }
    return hasChanges;
}
/*
 * uuid-timestamp (emitter)
 * UUID v4 based on timestamp
 *
 * Created by tarkh
 * tarkh.com (C) 2020
 */
export const uuidEmit = () => {
    // Get now time
    const n = Date.now();
    // Generate random
    const r = Math.random();
    // Stringify now time and generate additional random number
    const s = String(n) + String(~~(r * 9e4) + 1e4);
    // Form UUID and return it
    return `${s.slice(0, 8)}-${s.slice(8, 12)}-4${s.slice(12, 15)}-${[8, 9, 'a', 'b'][~~(r * 3)]}${s.slice(15, 18)}-${s.slice(s.length - 12)}`;
};

if (isNaN(zoomLevel)) zoomLevel = 20;
if (isNaN(zoomLevelVertical)) zoomLevelVertical = 1;

export function getCookie(key, defaultValue = false) {
    var list = document.cookie.split('; ')
    if (list.length) {
        var item = list.find(row => row.startsWith(key + '='))
        if (item)
            return item.split('=')[1]
    }
    return defaultValue;
}

export async function getColormap(data) {
    const connector = fetch('/getColormap', {
        method: "POST",
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(data),
        credentials: 'same-origin'
    })
        .then((response) => response.json())
        .then((connector) => { return connector; })
    const receiveColormap = async () => {
        return JSON.parse(await connector);
    };
    return receiveColormap();
}

export async function getUserSettings(data = {}) {
    const connector = fetch('/getUserSettings', {
        method: "POST",
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(data),
        credentials: 'same-origin'
    })
        .then((response) => response.json())
        .then((connector) => { return connector; })
    const receiveUserSetting = async () => {
        return JSON.parse(await connector);
    };
    var settings = await receiveUserSetting()
    document.getElementById("showDocumentation").style.display = settings.isDeveloper ? 'block' : 'none';
    document.getElementById("showAdministration").style.display = settings.isAdmin ? 'block' : 'none';
    return settings;
}

document.addEventListener('DOMContentLoaded', async function () {
    if (document.getElementsByTagName("main").length) {
        var menuItems = document.getElementsByClassName("dropbtn");
        for (var idx = 0; idx < menuItems.length; idx++) {
            menuItems[idx].onclick = function (event) { openDropdown(this); }
            menuItems[idx].onmouseover = function (event) { mouseoverDropdown(this); }
        }
        document.getElementsByTagName("main")[0].ondrop = function (event) { dropHandler(event); }
        document.getElementsByTagName("main")[0].ondragover = function (event) { dragOverHandler(event); }
        document.getElementById("newFile").onclick = async function (event) { await newFile(); this.blur(); }
        document.getElementById("openFile").onclick = function (event) { openFile(); }
        document.getElementById("saveFile").onclick = async function (event) { await saveFile(); }
        document.getElementById("ImportFile").onclick = async function (event) { await importAudiofile(); }
        document.getElementById("ImportAnnotations").onclick = function (event) { ImportAnnotations(); }
        document.getElementById("exportAnnotations").onclick = function (event) { exportAnnotations(); }
        document.getElementById("exportAudiofile").onclick = function (event) { exportAudiofile(); }
        document.getElementById("undo").onclick = async function (event) { await undo(); }
        document.getElementById("redo").onclick = async function (event) { await redo(); }
        document.getElementById("copyAudioData").onclick = async function (event) { await cutCopyAudioData(); }
        document.getElementById("cutAudioData").onclick = async function (event) { await cutCopyAudioData(true); }
        document.getElementById("pasteAudioData").onclick = async function (event) { await pasteAudioData(); }
        document.getElementById("showWaveform").onclick = function (event) { showWaveform(!showWave); }
        document.getElementById("showSpectrogram").onclick = async function (event) { if (showSpec) { await hideSpectrogram(); } else { await showSpectrogram(); } }
        document.getElementById("btnOpenFile").onclick = function (event) { openFile(); this.blur(); }
        document.getElementById("btnSaveFile").onclick = async function (event) { await saveFile(); this.blur(); }
        document.getElementById("btnRecord").onclick = async function (event) { await recordFromMicrofone(); this.blur(); }
        document.getElementById("btnCopyAudioData").onclick = async function (event) { await cutCopyAudioData(); this.blur(); }
        document.getElementById("btnCutAudioData").onclick = async function (event) { await cutCopyAudioData(true); this.blur(); }
        document.getElementById("btnPasteAudioData").onclick = async function (event) { await pasteAudioData(); this.blur(); }
        document.getElementById("btnLock").onclick = function (event) { showFilePreferences(); this.blur(); }
        document.getElementById("FileParameters").onclick = function (event) { showFilePreferences(); this.blur(); }
        document.getElementById("SystemPreferences").onclick = function (event) { showSystemPreferences(); }
        //document.getElementById("PluginsMenu").disabled = true;
        document.getElementById("showAnnotationList").onclick = function (event) {
            showListAnnotations = !showListAnnotations;
            if (showListAnnotations)
                showAnnotationList();
            else
                closePlugin("_ListAnnotations")
            updateDropDownMenuCheckboxes();
        }
        document.getElementById("showOverlayList").onclick = function (event) {
            showListOverlays = !showListOverlays;
            if (showListOverlays)
                showOverlayList();
            else
                closePlugin("_ListOverlays")
            updateDropDownMenuCheckboxes();
        }
        document.getElementById("showPluginList").onclick = async function (event) {
            showListPlugins = !showListPlugins;
            if (showListPlugins)
                await loadPluginList();
            else
                closePlugin("_ListPlugins")
            updateDropDownMenuCheckboxes();
        }
        UserSettings = await getUserSettings();
        setColorMap(await getColormap({ colormap: SystemPreferences.colormap }));
        await init();
    }

    window.addEventListener("error", (event) => {
        showError(event);
    });

    window.addEventListener('unhandledrejection', function (event) {
        showError(event);
    })

    function showError(event) {
        wavesurfer.pause();
        console.log(event);
        var message = "";
        if (event.reason && event.reason.stack) {
            message += event.reason.stack.replaceAll("\n", "<br>") + "<br>";
        }
        else {
            message = event.type + ":<br>";
            if (event.message)
                message += event.message + "<br>";
        }
        var cc = "";
        if (UserSettings.adminMails.length > 1)
            cc = "cc=" + UserSettings.adminMails.slice(1).join();
        message += "<br>&nbsp;<a href='mailto:" + UserSettings.adminMails[0] + "?" + cc + "subject=webAudio Error&body=" + message.replaceAll("<br>", "\n") + "'>Send Error-Message to Admin</a>";
        var title = "Error";
        var id = "PopUpWindow_" + title;
        var popup = createPopUpWindow(id, title, true);
        popup.setEnable(true);
        var content = document.getElementById(id + "_content");
        content.innerHTML = message;
    }

    window.addEventListener("click", function (event) {
        if (document.getElementById("annotation") && document.getElementById("annotation").style.visibility == "visible") {
            var hide = true;
            var treeselectContainers = document.getElementsByClassName("treeselect-list__group-container")
            for (var container of treeselectContainers) {
                var parent = container;
                while (parent.parentElement.tagName == 'DIV')
                    parent = parent.parentElement;
                if (parent.contains(event.target))
                    hide = false;
            }
            if (document.getElementById("annotation").contains(event.target))
                hide = false;
            if (hide) {
                document.getElementById("annotation").style.visibility = "hidden";
                treeselect.destroy();
            }
        }
    });
});

async function init() {
    await initIndexedDB();
    wavesurfer = WaveSurfer.create({
        container: '#waveform',
        waveColor: SystemPreferences.wavecolor,
        progressColor: SystemPreferences.progresscolor,
        loaderColor: SystemPreferences.progresscolor,
        backend: 'WebAudio',
        hideScrollbar: true,
        partialRender: false,
        minPxPerSec: zoomLevel,
        fillParent: true,
        autoCenter: true,
        forceDecode: false,
        responsive: true,
        cursorColor: 'yellow',
        splitChannels: true,
        scrollParent: true,
        splitChannelsOptions: {
            overlay: false,
            channelColors: {
                0: {
                    progressColor: SystemPreferences.progresscolor,
                    waveColor: SystemPreferences.wavecolor
                },
                1: {
                    progressColor: SystemPreferences.progresscolor,
                    waveColor: SystemPreferences.wavecolor
                }
            }
        },
        plugins: [
            WaveSurfer.playhead.create({
                returnOnPause: false,
                moveOnSeek: true,
                draw: true
            }),
            WaveSurfer.timeline.create({
                container: '#timeline'
            }),
            WaveSurfer.minimap.create({
                container: '#wave-minimap',
                normalize: true,
                waveColor: SystemPreferences.wavecolor,
                progressColor: SystemPreferences.progresscolor,
                splitChannels: false,
                height: 50,
                showRegions: true,
                showOverview: true,
                overviewBorderColor: gray1,
                splitChannelsOptions: {
                    channelColors: {
                        0: {
                            progressColor: SystemPreferences.progresscolor,
                            waveColor: SystemPreferences.wavecolor,
                        },
                        1: {
                            progressColor: SystemPreferences.progresscolor,
                            waveColor: SystemPreferences.wavecolor
                        }
                    }
                }
            }),
            WaveSurfer.regions.create({
                color: "rgba(255, 255, 255, 0.2)",
                regionsMinLength: 0.001,
                channelIdx: null,
                regions: [],
                Selection: {
                    slop: 5
                }
            }),
            WaveSurfer.markers.create({
                markers: []
            }),
        ]
    });

    /* Progress bar */
    (function () {
        var progressDiv = document.querySelector('#progress-bar');
        var progressBar = progressDiv.querySelector('.progress-bar');
        var showProgress = function (percent) {
            document.getElementById("hideWavesurfer").style.visibility = "visible";
            progressDiv.style.display = 'block';
            progressBar.style.width = percent + '%';
        };
        var hideProgress = function () {
            document.getElementById("hideWavesurfer").style.visibility = "hidden";
            progressDiv.style.display = 'none';
            progressBar.style.width = '0%';
        };
        wavesurfer.on('loading', showProgress);
        wavesurfer.on('ready', hideProgress);
        wavesurfer.on('destroy', hideProgress);
        wavesurfer.on('error', hideProgress);
    })();

    await wavesurfer.backend.ac.audioWorklet.addModule('static/JS/StreamProcessor.js');

    socket = io.connect(window.location.protocol + "//" + window.location.host);

    // WORKAROUND PARSING NUMPY NaN
    socket.io.decoder.tryParse = function (str) {
        try {
            return JSON.parse(str.replaceAll("NaN", "null"), this.reviver);
        }
        catch (e) {
            return false;
        }
    }

    wavesurfer.on('seek', async function () {
        Object.keys(wavesurfer.regions.list).forEach(element => {
            wavesurfer.regions.list[element].setLoop(false);
        });
        if (CtrlPressed && lastClickTime) {
            var item = {}
            if (lastClickTime < wavesurfer.getCurrentTime()) {
                item.start = new Date(lastClickTime * 1000).toISOString().substr(11, 12);
                item.end = new Date(wavesurfer.getCurrentTime() * 1000).toISOString().substr(11, 12);
            }
            else {
                item.end = new Date(lastClickTime * 1000).toISOString().substr(11, 12);
                item.start = new Date(wavesurfer.getCurrentTime() * 1000).toISOString().substr(11, 12);
            }
            var region = await addImportedAnnotation(item);
            selectRegion(region);
        }
        setCurrentTime(wavesurfer.getCurrentTime());
    });

    wavesurfer.on('wolaClientProcess', function (data) {
        wolaClientProcess(data)
    });

    wavesurfer.on('nextWavesurferBufferBlock', function (data) {
        nextWavesurferBufferBlock(data)
    });

    wavesurfer.on('ready', async function () {
        await loadPluginList();
        UserSettings = await getUserSettings();
        wavesurfer.container.childNodes[0].style.flex = 1;
        document.getElementById("muteButton2").style.display = wavesurfer.backend.buffer.numberOfChannels >= 2 ? "flex" : "none";
        document.getElementById("MidLine").style.display = wavesurfer.backend.buffer.numberOfChannels >= 2 ? "block" : "none";
        document.getElementById("VUmeter1").style.display = wavesurfer.backend.buffer.numberOfChannels >= 2 ? "block" : "none";
        wavesurfer.markers.clear();
        wavesurfer.regions.clear();
        if (showListAnnotations)
            showAnnotationList();
        updateWaveColors();
        await clearHistory();
        setDeletedRegions([]);
        if (DataSet.FilePreferences.id != null) {
            setAudioSignalHasChanges(false);
            loadFilePreferences({ fileId: DataSet.FilePreferences.id });
        }
        refreshSpetrogram = true;
        //document.getElementsByTagName("timelinewaveform")[0].style.visibility = "visible"
        //wavesurfer.play();
        document.getElementById("Duration").innerHTML = new Date(wavesurfer.getDuration() * 1000).toISOString().substr(11, 12);
        Gains = [1, 1];
        wavesurfer.container.children[0].style.zIndex = 100;
        await enableAnnotation(false);
        zoom();
        setAmplitudes();
        windowResize();
        loadAnnotations();
        if (showSpec)
            showSpectrogram();
        else
            hideSpectrogram();
    });

    socket.on('error', async function (value) {
        wavesurfer.pause();
        document.getElementById(value.target + "_content").innerHTML = value.message;
        if (document.getElementById(value.target + "Apply"))
            document.getElementById(value.target + "Apply").disabled = true;
        if (document.getElementById(value.target + "OK"))
            document.getElementById(value.target + "OK").disabled = true;
        document.getElementById(value.target).setEnable(true);
    });

    socket.on('message', async function (value) {
        var divWindows = getPluginWindows();
        if (value.hasOwnProperty("settings") && Object.entries(value).length == 1) {
            //if (value.hasOwnProperty("settings")) {
            if (divWindows.find(plugin => plugin.isModal))
                divWindows = [divWindows.find(plugin => plugin.isModal)];
            for (var idx = 0; idx < divWindows.length; idx++) {
                if (value.settings.hasOwnProperty(divWindows[idx].id) && value.settings[divWindows[idx].id].hasOwnProperty("params"))
                    updateLastSettings(divWindows[idx].id, ["params"], value.settings[divWindows[idx].id]["params"]);
            }
            receiveParams(value["settings"]);
        }
        for (var idx = 0; idx < divWindows.length; idx++) {
            var divWindow = divWindows[idx];
            if (value.hasOwnProperty("postProcess") && value.postProcess && value.hasOwnProperty("PluginId") && value.PluginId == divWindow.id) {
                if (value.hasOwnProperty("data") && divWindow.hasOwnProperty("pluginJSObject") && typeof divWindow.pluginJSObject.postprocess === 'function') { //  && value.data.hasOwnProperty(divWindow.id)
                    divWindow.pluginJSObject.postprocess(value.data[divWindow.id], value.settings[divWindow.id].params, document.getElementById(divWindow.id + "_content"));
                }
                document.getElementById("hideWavesurfer").style.visibility = "hidden";
                divWindow.setEnable(true);
                if (document.getElementById(value.PluginId + "_progress"))
                    divWindow.removeChild(document.getElementById(value.PluginId + "_progress"));
                if (!divWindow.isModal)
                    document.getElementById("hideWavesurfer").style.visibility = "hidden";
                wavesurfer.drawBuffer();
                setRefreshSpetrogram(true);
                await fitWindowSize();
                var okButton = document.getElementById(divWindow.id + "OK");
                var applyButton = document.getElementById(divWindow.id + "Apply");
                if ((applyButton && applyButton.disabled == true || !applyButton) && okButton.disabled == false)
                    closePlugin(divWindow.id);
                else {
                    okButton.disabled = false;
                    applyButton.disabled = false;
                }
                divWindow.style.cursor = "";
                /*
                if (divWindow.currentMode == pluginMode.newSignal) {
                    wavesurfer.seekAndCenter(0);
                    setDeletedRegions({});
                    await clearHistory();
                }
                */
                if (!wavesurfer.isPlaying() && processingInfo.divWindows.length == 1 && divWindow.type == 'PluginType.insert') {
                    wavesurfer.seekTo(processingInfo.indices.start / wavesurfer.backend.buffer.length);
                    await pasteAudioData(true, false, false);
                }
                if (selectedRegion && DataSet.Overlays[divWindow.id]) {
                    var values = new Array(Math.round((wavesurfer.backend.buffer.duration - selectedRegion.end) * wavesurfer.backend.buffer.sampleRate / (value.settings[divWindow.id].params.blockLen * (value.settings[divWindow.id].params.timeDomainWithoutOverlap ? 1 : .5))));
                    DataSet.Overlays[divWindow.id].values = DataSet.Overlays[divWindow.id].values.concat(values)
                }
                await updateOverlays();
                /*
                if (divWindow.currentMode == pluginMode.processing || divWindow.currentMode == pluginMode.newSignal) {
                    divWindow.setEnable(true);
                    if (blnHasError == false) {
                        closePlugin(divWindow.id);
                    }
                }
                */
                //divWindow.currentMode = pluginMode.undefined;
            }
        }
        if (!value.hasOwnProperty("postProcess"))
            wolaClientProcess(value);
    });

    socket.on('connect_error', function (err) {
        wavesurfer.pause();
        socket.close();
    });

    wavesurfer.on('audioprocess', function () {
        setCurrentTime(wavesurfer.getCurrentTime());
    });

    wavesurfer.on('play', async function () {
        var settings = PluginParamsStructured;
        buttonPlay.style.borderStyle = 'inset';
        buttonPlay.style.color = 'black';
        clearLastSettings();

        wavesurfer.backend.source.disconnect();

        if (lastSettings)
            settings = lastSettings;
        if (!wavesurfer.scriptNode) {
            var StreamProcessor = new AudioWorkletNode(wavesurfer.backend.ac, 'StreamProcessor');
            wavesurfer.scriptNode = StreamProcessor;
            wavesurfer.scriptNode.port.onmessage = function (value) { fromSource(value.data) };
        }
        wavesurfer.backend.source.connect(wavesurfer.scriptNode);
        wavesurfer.scriptNode.connect(wavesurfer.backend.ac.destination);
        initProcessing(null, sources.StreamProcessor);
        await wavesurfer.backend.ac.resume();
    });

    wavesurfer.on('pause', async function () {
        buttonPlay.style.borderStyle = 'outset';
        buttonPlay.style.color = 'white';
        await wavesurfer.backend.ac.suspend();
        if (selectedRegion && selectedRegion.loop) {
            wavesurfer.stop;
            selectedRegion.playLoop();
        }
    });

    //
    // Play Button

    var buttonPlay = document.querySelector('[data-action="play"]');
    buttonPlay.addEventListener('click', () => {
        Object.keys(wavesurfer.regions.list).forEach(element => {
            wavesurfer.regions.list[element].setLoop(false);
        });
        if (wavesurfer.microphone)
            wavesurfer.microphone.stop();
        wavesurfer.playPause();
        buttonPlay.blur();
    });
    var buttonStop = document.querySelector('[data-action="stop"]');
    buttonStop.addEventListener('click', () => { wavesurfer.stop(); buttonStop.blur(); });
    var buttonPause = document.querySelector('[data-action="pause"]');
    buttonPause.addEventListener('click', () => { wavesurfer.pause(); buttonPause.blur(); });
    var buttonBackward = document.querySelector('[data-action="backward"]');
    buttonBackward.addEventListener('click', () => { wavesurfer.seekAndCenter(Math.max(0, wavesurfer.getCurrentTime() / wavesurfer.getDuration() - .05)); buttonBackward.blur(); });
    var buttonForward = document.querySelector('[data-action="forward"]');
    buttonForward.addEventListener('click', () => { wavesurfer.seekAndCenter(Math.min(1, wavesurfer.getCurrentTime() / wavesurfer.getDuration() + .05)); buttonForward.blur(); });
    var buttonFastBackward = document.querySelector('[data-action="fast-backward"]');
    buttonFastBackward.addEventListener('click', () => { wavesurfer.seekAndCenter(0); buttonFastBackward.blur(); });
    var buttonFastForward = document.querySelector('[data-action="fast-forward"]');
    buttonFastForward.addEventListener('click', () => { wavesurfer.seekAndCenter(1); buttonFastForward.blur(); });

    // zoom-in
    buttonZoomIn.addEventListener('click', () => {zoom(+1);});

    // zoom-out
    buttonZoomOut.addEventListener('click', async () => {zoom(-1);});

    // zoomVertical-in
    buttonZoomVerticalIn.addEventListener('click', async () => {
        zoomLevelVertical = Math.max(1, Math.min(1000, zoomLevelVertical + 1));
        wavesurfer.params.barHeight = zoomLevelVertical;
        setAmplitudes();
        buttonZoomVerticalIn.blur();
        //await wavesurfer.empty();
        await wavesurfer.drawBuffer();
        await fitWindowSize();
    });

    // zoomVertical-out
    buttonZoomVerticalOut.addEventListener('click', async () => {
        zoomLevelVertical = Math.max(1, Math.min(1000, zoomLevelVertical - 1));
        wavesurfer.params.barHeight = zoomLevelVertical;
        setAmplitudes();
        buttonZoomVerticalOut.blur();
        //await wavesurfer.empty();
        await wavesurfer.drawBuffer();
        await fitWindowSize();
    });

    function setAmplitudes() {
        for (var itemList of document.getElementsByClassName("amplitudes")) {
            var items = itemList.getElementsByClassName("amplitude");
            var idx = 0;
            for (var item of items) {
                var number = 1 / wavesurfer.params.barHeight / items.length * 2 * (items.length / 2 - idx);
                var temp = number.toString().split('.')
                if (temp.length == 1)
                    temp[1] = "";
                temp[1] = temp[1].padEnd(10, '0');
                if (number < 0)
                    number = temp.join('.').substring(0, 6);
                else
                    number = temp.join('.').substring(0, 5);
                if (item.getElementsByTagName("SPAN").length > 0)
                    item.getElementsByTagName("SPAN")[0].innerText = number;
                else
                    item.innerText = number;
                if (idx == items.length / 2 - 1)
                    idx += 2;
                else
                    idx++;
            }
        }
    }

    // Mute Left
    var buttonMuteLeft = document.querySelector('[data-action="muteLeft"]');
    buttonMuteLeft.addEventListener('click', () => {
        Gains[0] = +!Gains[0];
        if (wavesurfer.scriptNode)
            wavesurfer.scriptNode.port.postMessage({ "LeftGain": Gains[0] });
        updateWaveColors();
        buttonMuteLeft.blur();
        buttonMuteLeft.style.borderStyle = (Gains[0] ? 'outset' : 'inset');
        buttonMuteLeft.style.color = (Gains[0] ? 'white' : 'black');
    });
    // Mute Right
    var buttonMuteRight = document.querySelector('[data-action="muteRight"]');
    buttonMuteRight.addEventListener('click', () => {
        Gains[1] = +!Gains[1];
        if (wavesurfer.scriptNode)
            wavesurfer.scriptNode.port.postMessage({ "RightGain": Gains[1] });
        updateWaveColors();
        buttonMuteRight.blur();
        buttonMuteRight.style.borderStyle = (Gains[1] ? 'outset' : 'inset');
        buttonMuteRight.style.color = (Gains[1] ? 'white' : 'black');
    });
    // Log errors
    wavesurfer.on('error', function (msg) {
        if (wavesurfer.backend.buffer == null)
            openFile();
        console.log(msg);
    });

    document.onkeydown = async function (e) {
        if (e.code == "Escape") {
            var pluginWindows = document.getElementsByClassName("pluginwindow");
            for (var idx = 0; idx < pluginWindows.length; idx++) {
                var windowId = pluginWindows[idx].id;
                pluginWindows[idx].onclose(windowId);
                if (document.getElementById((windowId)))
                    closePlugin(windowId);
            }
            closeDropdowns();
            closePlugin("loadFileWindow");
            if (selectedRegion) {
                setSelectedRegion(null);
                unselectRegions();
            }
        }
        if (document.getElementById("Recorder")) {
            var divWindow = document.getElementById("Recorder");
            if (e.code == "Space") {
                document.getElementById("btnRecordingPlay").click();
                document.getElementById("btnRecordingStart").click();
            }
            else if (e.code == "Enter")
                document.getElementById("btnRecordingPaste").click();
            else if (e.code == "KeyR" && (e.metaKey || e.ctrlKey)) {
                e.preventDefault();
                e.stopPropagation();
                document.getElementById("btnRecordingStart").click();
            }
        }
        if (document.activeElement.tagName == "BODY" && document.getElementById("hideWavesurfer").style.visibility != "visible" && e.code == "KeyA" && (e.metaKey || e.ctrlKey) && wavesurfer.backend.buffer) {
            var region = wavesurfer.regions.add({ start: 0, end: wavesurfer.backend.buffer.duration })
            selectRegion(region);
            return false;
        }
        else if (document.activeElement.tagName == "BODY" && (document.getElementById("hideWavesurfer").style.visibility == "visible" || (e.code == "KeyA" && (e.metaKey || e.ctrlKey)))) {
            return false;
        }
        if ((e.metaKey || e.ctrlKey)) {
            CtrlPressed = true;
            await enableAnnotation(true);
        }
        if (document.activeElement.tagName == "BODY") {
            e.preventDefault();
            e.stopPropagation();
            if (e.code == "Space")
                buttonPlay.click();
            else if (e.code == "ShiftLeft" || e.code == "ShiftRight")
                ShiftPressed = true;
            if (e.code == "BracketRight") {
                if (!ShiftPressed)
                    buttonZoomIn.click();
                else
                    buttonZoomVerticalIn.click();
                return (!e.metaKey && !e.ctrlKey); // !e.ctrlKey
            }
            else if (e.code == "Slash") {
                if (!ShiftPressed)
                    buttonZoomOut.click();
                else
                    buttonZoomVerticalOut.click();
                return (!e.metaKey && !e.ctrlKey); // !e.ctrlKey
            }
            else if (e.code == "ArrowRight" && !ShiftPressed)
                buttonForward.click();
            else if (e.code == "ArrowLeft" && !ShiftPressed)
                buttonBackward.click();
            else if (e.code == "ArrowRight" && ShiftPressed)
                buttonFastForward.click();
            else if (e.code == "ArrowLeft" && ShiftPressed)
                buttonFastBackward.click();
            else if (e.code == "KeyB" && selectedRegion != null && (e.metaKey || e.ctrlKey) && document.getElementById("annotation").style.visibility != "visible")
                addAnnotation(selectedRegion);
            else if (e.code == "KeyR" && (e.metaKey || e.ctrlKey))
                await recordFromMicrofone();
            else if (e.code == "KeyN" && (e.metaKey || e.ctrlKey))
                await newFile();
            else if (e.code == "KeyY" && (e.metaKey || e.ctrlKey))
                await undo();
            else if (e.code == "KeyZ" && (e.metaKey || e.ctrlKey))
                await redo();
            else if (e.code == "KeyX" && (e.metaKey || e.ctrlKey) && selectedRegion)
                await cutCopyAudioData(true);
            else if (e.code == "KeyC" && (e.metaKey || e.ctrlKey) && selectedRegion)
                await cutCopyAudioData(false);
            else if (e.code == "KeyV" && (e.metaKey || e.ctrlKey))
                await pasteAudioData();
            else if (e.code == "KeyS" && (e.metaKey || e.ctrlKey))
                await saveFile();
            else if (e.code == "KeyO" && (e.metaKey || e.ctrlKey))
                openFile();
            else if (e.code == "Delete" && selectedRegion != null) {
                await deleteAnnotation(selectedRegion);
                setSelectedRegion(null);
            }
        }
        //else
        //    console.log(e.code);
        return false;
    }

    document.onkeyup = async function (e) {
        if (e.key == "Shift")
            ShiftPressed = false;
        if (e.key == "Control") {
            CtrlPressed = false;
            await enableAnnotation(false);
        }
    }

    wavesurfer.drawer.on('click', function (e) {
        e.preventDefault();
        if (selectedRegion != null) {
            setSelectedRegion(null);
            unselectRegions();
        }
        CtrlPressed = (e.metaKey || e.ctrlKey);
    })

    wavesurfer.on('zoom', async e => {
        if (wavesurfer.spectrogram)
            wavesurfer.spectrogram.canvas.style.width = Math.max(wavesurfer.drawer.getWidth(), Math.round(wavesurfer.getDuration() * wavesurfer.params.minPxPerSec * wavesurfer.params.pixelRatio)) + "px"
        buttonZoomOut.blur();
        buttonZoomIn.blur();
        buttonZoomVerticalIn.blur();
        buttonZoomVerticalOut.blur();
        await fitWindowSize();
    });

    wavesurfer.container.addEventListener('mousemove', e => {
        const mod = (n, m) => (n % m + m) % m;
        const bbox = wavesurfer.container.getBoundingClientRect();
        var x = e.clientX - bbox.left;
        var y = e.clientY - bbox.top;
        const duration = wavesurfer.getDuration();
        const elementWidth = wavesurfer.drawer.width / wavesurfer.params.pixelRatio;
        const scrollWidth = wavesurfer.drawer.getScrollX();
        const scrollTime = (duration / wavesurfer.drawer.width) * scrollWidth;
        const timeValue = Math.max(0, (x / elementWidth) * duration) + scrollTime;
        var channel = Math.floor(y / wavesurfer.container.offsetHeight);
        if (!isNaN(timeValue)) {
            document.getElementById("MouseTime").innerText = new Date(timeValue * 1000).toISOString().substr(11, 12);
            document.getElementById("Amplitude").innerText = Math.round(1000 * mod(Math.round((1 - (y - (channel * wavesurfer.container.offsetHeight)) / wavesurfer.container.offsetHeight) / wavesurfer.params.barHeight * 1000 * wavesurfer.backend.buffer.numberOfChannels - 1000) / 1000, 1 / wavesurfer.params.barHeight)) / 1000;
            var freq = "0";
            var level = "0.00";
            if (wavesurfer.spectrogram && wavesurfer.spectrogram.frequencies) {
                freq = Math.round((1 - (y - (channel * wavesurfer.container.offsetHeight)) / wavesurfer.container.offsetHeight) * (SystemPreferences.frequencyRange[1] - SystemPreferences.frequencyRange[0]) + SystemPreferences.frequencyRange[0]);
                var freqs = wavesurfer.spectrogram.frequencies[channel];
                if (freqs && freqs[parseInt(timeValue / duration * freqs.length)])
                    level = freqs[parseInt(timeValue / duration * freqs.length)][parseInt(freq / (wavesurfer.backend.buffer.sampleRate / 2) * (wavesurfer.spectrogram.fftSamples / 2))]
                freq = mod((freq - SystemPreferences.frequencyRange[0]) * wavesurfer.backend.buffer.numberOfChannels - (SystemPreferences.frequencyRange[1] - SystemPreferences.frequencyRange[0]), (SystemPreferences.frequencyRange[1] - SystemPreferences.frequencyRange[0])) + SystemPreferences.frequencyRange[0];
            }
            document.getElementById("Level").innerText = freq + " Hz: " + level + " dB";
        }
    });

    wavesurfer.container.addEventListener('wheel', async e => {
        e.preventDefault();
        //if (e.deltaX != 0 && e.deltaY != 0)
        //  return;
        if (e.deltaX < 0) {
            wavesurfer.drawer.wrapper.scrollLeft = Math.max(0, wavesurfer.drawer.getScrollX() - 50)
        }
        else if (e.deltaX > 0) {
            wavesurfer.drawer.wrapper.scrollLeft = Math.max(0, wavesurfer.drawer.getScrollX() + 50)
        }
        if (e.deltaY > 0) {
            if (!e.shiftKey)
                buttonZoomOut.click();
            else
                buttonZoomVerticalOut.click();
        }
        else if (e.deltaY < 0) {
            if (!e.shiftKey)
                buttonZoomIn.click();
            else
                buttonZoomVerticalIn.click();
        }
    });

    /*
    wavesurfer.minimap.drawer.container.addEventListener('mouseout', (e) => {
        var minimapCoordinates = wavesurfer.minimap.drawer.container.getBoundingClientRect();
        minimapCoordinates.top
        minimapCoordinates.bottom
        minimapCoordinates.left
        minimapCoordinates.right
        console.log(e.clientX, e.clientY);
        e.preventDefault();
        //e.stopPropagation();
        return false;
    });
    */

    /*
    wavesurfer.drawer.updateProgress = function(position) {
        console.log(position);
        this.style(this.progressWave, {
            width: position + 'px'
        });
    }
    */

    wavesurfer.container.style.display = "flex";
    wavesurfer.container.style.flexDirection = "column";
    wavesurfer.on('redraw', updateOverlays);
    initResize();
    window.addEventListener('resize', windowResize);
    initAnnotation(wavesurfer);
    if (showListAnnotations)
        showAnnotationList();
    if (showListOverlays)
        showOverlayList();
    if (getCookie("FileId", false)) {
        DataSet.FilePreferences.id = getCookie("FileId", "");
        await loadFile(DataSet.FilePreferences.id);
    }
    else
        openFile();

    /*
    var WindowName = "_DataSetList";
    createPopUpWindow(WindowName, "DataSet List", false, false)
    DataSetListWindow = document.getElementById(WindowName);
    DataSetListWindow.setEnable(true);
    DataSetListWindow.onclose = function (event) {
        setShowListPlugins(false);
        closePlugin(this.id);
    }
    dockWindow(DataSetListWindow, "left");

    var WindowName = "_DataLogger";
    createPopUpWindow(WindowName, "Data Logger", false, false);
    DataLoggerWindow = document.getElementById(WindowName);
    DataLoggerWindow.setEnable(true);
    DataLoggerWindow.onclose = function (event) {
        setShowListPlugins(false);
        closePlugin(this.id);
    }
    dockWindow(DataLoggerWindow, "bottom");
    */

}

function zoom(action = 0) {
    var maxZoomLevel = wavesurfer.backend.buffer.sampleRate;
    var lastZoomLevel = zoomLevel;
    var tmpzoomFaktor = zoomLevel < 10 ? 2 : zoomFaktor;
    if (action > 0)
        zoomLevel = zoomLevel * tmpzoomFaktor;
    else if (action < 0)
        zoomLevel = zoomLevel / tmpzoomFaktor;
    zoomLevel = Math.floor(Math.min(maxZoomLevel, Math.max(100, zoomLevel)))
    document.cookie = "zoomLevel=" + zoomLevel + ";path=/; max-age=31536000";
    document.getElementById("zoom-value").innerText = zoomLevel;
    var currentZoom = Math.floor(zoomLevel / 100 * Math.floor(Math.max(1 , wavesurfer.drawer.getWidth() / (wavesurfer.backend.buffer.length / wavesurfer.backend.buffer.sampleRate))));
    if (lastZoomLevel != zoomLevel || action == 0)
        wavesurfer.zoom(currentZoom);
}

export function updateWaveColors() {
    for (var idx = 0; idx < Gains.length; idx++) {
        if (showWave) {
            if (Gains[idx]) {
                if (wavesurfer.getWaveColor(idx) != SystemPreferences.wavecolor)
                    wavesurfer.setWaveColor(SystemPreferences.wavecolor, idx);
                if (wavesurfer.getProgressColor(idx) != SystemPreferences.progresscolor)
                    wavesurfer.setProgressColor(SystemPreferences.progresscolor, idx);
            }
            else {
                if (wavesurfer.getWaveColor(idx) != "rgba(50, 50, 50, 0.4)")
                    wavesurfer.setWaveColor("rgba(50, 50, 50, 0.4)", idx);
                if (wavesurfer.getProgressColor(idx) != "rgba(50, 50, 50, 0.4)")
                    wavesurfer.setProgressColor("rgba(50, 50, 50, 0.4)", idx);
            }
        }
        else {
            wavesurfer.setWaveColor("rgba(0, 0, 0, 0)", idx)
            wavesurfer.setProgressColor("rgba(0, 0, 0, 0)", idx)
        }
    }
}

export async function windowResize(event) {
    var dockOffsetHight = 0;
    var divWindows = document.getElementsByClassName("pluginwindow");
    for (var idx = 0; idx < divWindows.length; idx++) {
        if (divWindows[idx].hasOwnProperty("dock")) {
            if (divWindows[idx].dock == "right" || divWindows[idx].dock == "left")
                divWindows[idx].style.height = "0px";
            if (divWindows[idx].dock == "bottom")
                dockOffsetHight = divWindows[idx].offsetHeight;
        }
        else {
            divWindows[idx].style.top = Math.max(Math.min(divWindows[idx].offsetTop, window.innerHeight - divWindows[idx].offsetHeight), 0) + "px";
            divWindows[idx].style.left = Math.max(Math.min(divWindows[idx].offsetLeft, window.innerWidth - divWindows[idx].offsetWidth), 0) + "px";
        }
    }
    var tmp = document.getElementById("mainmenu").offsetHeight + document.getElementById("controlsArea").offsetHeight + document.getElementById("wave-minimap").offsetHeight + document.getElementById("VUmeter").offsetHeight + document.getElementById("timeline").offsetHeight + document.getElementById("StatusBar").offsetHeight;
    var tmHeight = (window.innerHeight - tmp) / 2 - (dockOffsetHight / 2);
    if (wavesurfer) {
        //wavesurfer.setHeight(tmHeight);
        //wavesurfer.params.height = tmHeight;
        wavesurfer.drawBuffer();
        wavesurfer.minimap.renderRegions();
        //if (wavesurfer.spectrogram)
        //   wavesurfer.spectrogram.canvas.style.height = wavesurfer.container.offsetHeight + "px";
        if (wavesurfer.drawer.canvases[0].wave.width != wavesurfer.container.offsetWidth) {
            zoom();
        }
        await fitWindowSize();
        if (wavesurfer.spectrogram)
            wavesurfer.spectrogram.canvas.style.width = Math.max(wavesurfer.drawer.getWidth(), Math.round(wavesurfer.getDuration() * wavesurfer.params.minPxPerSec * wavesurfer.params.pixelRatio)) + "px"
        wavesurfer.markers._updateMarkerPositions();
    }
    for (var idx = 0; idx < divWindows.length; idx++) {
        if (divWindows[idx].hasOwnProperty("dock") && divWindows[idx].dock == "right" || divWindows[idx].dock == "left")
            divWindows[idx].style.height = divWindows[idx].parentElement.offsetHeight + "px";
    }
}

export function setCurrentTime(value) {
    lastClickTime = wavesurfer.getCurrentTime();
    var plugins = document.getElementsByClassName("pluginwindow")
    var noUpdate = false;
    var divWindow = null;
    for (var idx = 0; idx < plugins.length; idx++) {
        divWindow = document.getElementById(plugins[idx].id);
        noUpdate = noUpdate || (divWindow.type == 'PluginType.insert' && divWindow.close == false)
    }
    if (!noUpdate) {
        document.getElementById("CurrentTime").style.color = "white";
        document.getElementById("CurrentTime").innerHTML = new Date(value * 1000).toISOString().substr(11, 12);
    }
}

if (document.getElementsByTagName("main").length) {
    window.onbeforeunload = function (e) {
        e = e || window.event;
        if (setAudioSignalHasChanges(DataSet.audioSignalHasChanges)) {
            if (e)
                e.returnValue = 'text';
            return 'text';
        }
        /*
        if (IndexedDB)
            await deleteDB(SystemPreferences.databaseName);
        */
    };

    window.addEventListener("contextmenu", e => {
        if (e.target.tagName !== "IMG") {
            e.preventDefault();
        }
    });
}

export function loadFilePreferences(data = null) {
    if (data) {
        const preferences = fetch('/getFilePreferences', {
            method: "POST",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(data),
            credentials: 'same-origin'
        })
            .then((response) => response.json())
            .then((preferences) => { return preferences; })
        const preferencesLoaded = async () => {
            DataSet.FilePreferences = JSON.parse(await preferences);
        };
        preferencesLoaded();
    }
}