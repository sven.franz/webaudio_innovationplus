self.importScripts('https://unpkg.com/wavesurfer.js/dist/plugin/wavesurfer.spectrogram.js');

class Buffer {
    constructor(data) {
        this.sampleRate = data.sampleRate;
        this.duration = data.duration;
        this.length = data.length;
        this.numberOfChannels = data.numberOfChannels;
        this.buffer = data.buffer;
        this.fftSamples = data.fftSamples;
        this.windowFunc = data.windowFunc;
        this.noverlap = data.noverlap;
        this.frequencyMax = data.frequencyMax;
        this.frequencyMin = data.frequencyMin;
        this.preEmphasis = data.preEmphasis;
        this.preEmphasisFrequency = data.preEmphasisFrequency;
    }

    getChannelData(channel) {
        return this.buffer[channel];
    }
}

self.addEventListener("message", function (e) {
    const buffer = new Buffer(e.data);
    if (buffer.preEmphasis) {
        var alpha = Math.exp(-2 * Math.PI * buffer.preEmphasisFrequency / buffer.sampleRate);
        for (var channelIdx = 0; channelIdx < buffer.numberOfChannels; channelIdx++) {
            var lastValue = 0;
            var channel = buffer.getChannelData(channelIdx)
            for (var idx = 0; idx < channel.length; idx++) {
                lastValue = channel[idx];
                channel[idx] = channel[idx] - alpha * lastValue;
            }
        }
    }
    var wavesurfer = new WaveSurfer.spectrogram([], { util: [], backend: { buffer: buffer } });
    wavesurfer.splitChannels = buffer.numberOfChannels > 1;
    wavesurfer.channels = buffer.numberOfChannels;
    wavesurfer.pixelRatio = 20;
    wavesurfer.fftSamples = buffer.fftSamples;
    wavesurfer.windowFunc = buffer.windowFunc;
    wavesurfer.noverlap = buffer.noverlap;
    wavesurfer.frequencyMax = buffer.frequencyMax;
    wavesurfer.frequencyMin = buffer.frequencyMin;
    wavesurfer.getFrequencies((frequencies) => {
        postMessage(frequencies);
    });
}, false);
