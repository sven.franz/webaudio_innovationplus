import { SystemPreferences } from "./script.js";

export var IndexedDB = null;

export async function initIndexedDB() {

  var version = 1;

  try {
    IndexedDB = await idb.openDB(SystemPreferences.databaseName, version, {
      upgrade(db, oldVersion, newVersion, transaction) {
        if (!db.objectStoreNames.contains("History")) { // if there's no "books" store
          db.createObjectStore("History", { keyPath: 'id', autoIncrement: true });
        }
      },
      blocked() {
        // this event shouldn't trigger if we handle onversionchange correctly
        // it means that there's another open connection to the same database
        // and it wasn't closed after IndexedDB.onversionchange triggered for it
      },
      blocking() {
      },
      terminated() {
        IndexedDB = null;
      },
    });
  }
  catch { }
}
