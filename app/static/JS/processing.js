import { getPluginWindows, PluginParamsStructured } from "./plugins.js";
import { DataSet, Gains, setAudioSignalHasChanges, showSpec, showWave, socket, SystemPreferences, wavesurfer } from "./script.js";
import { updateOverlayList } from "./dropdown.js";
import { addToHistory, HistoryType } from "./history.js";
import { AppendToMemoryBuffer, cutCopyAudioData, setMemoryBuffer } from "./actions.js";
import { addImportedAnnotation, selectedRegion } from "./annotation.js";
import { wola } from "./wola.js";
import "./pluginHelper.js";

export async function setOverlays(value) { DataSet.Overlays = value; await updateOverlays(); };
export var lastSettings = {};
export function clearLastSettings() { lastSettings = {}; }
export function updateLastSettings(id, key, value) {
    if (!lastSettings.hasOwnProperty(id))
        lastSettings[id] = { params: {} };
    if (lastSettings[id][key] != value)
        lastSettings[id][key] = value;
}

export const sources = {
    none: 0,
    StreamProcessor: 1,
    WavesurferBuffer: 2,
    newSignal: 3,
};

export var processingInfo = { source: null, divWindows: [], indices: { start: 0, current: 0, end: 0 }, blockLen: 0 };

export async function initProcessing(PluginId, sourceType) {
    /*
    for (var plugin of document.getElementsByClassName("pluginwindow")) {
        delete plugin.sqrtWindow;
        delete plugin.lastBlockIn;
        delete plugin.lastBlockOut;
        delete plugin.block;
        delete plugin.fftBlock;
        delete plugin.fftOut;
        delete plugin.fftBlockHalf;
        console.log("TEST")
    }
    */
    if (wavesurfer.scriptNode)
        wavesurfer.scriptNode.port.postMessage({ Init: true, LeftGain: Gains[0], RightGain: Gains[1] });
    clearLastSettings();
    setExecutePreprocess(true)
    lastSettings = PluginParamsStructured;
    processingInfo.indices.current = 0;
    processingInfo.indices.end = wavesurfer.backend.buffer.duration * wavesurfer.backend.buffer.sampleRate;

    if (sourceType != sources.none)
        processingInfo.source = sourceType;
    else if (wavesurfer.isPlaying())
        processingInfo.source = sources.StreamProcessor;
    else
        processingInfo.source = sources.WavesurferBuffer;

    var divWindow = document.getElementById(PluginId);
    if (divWindow) {
        wavesurfer.pause();
        await addToHistory(HistoryType.audiodata, PluginId + " processed");
        divWindow.gui.container.style.zIndex = divWindow.style.zIndex;
        setAudioSignalHasChanges(true);
        divWindow.setEnable(false);
        document.getElementById("hideWavesurfer").style.visibility = "visible";
        var divProgress = document.getElementById("progress-bar").cloneNode(true);
        divProgress.id = PluginId + "_progress";
        divProgress.style.visibility = "visible";
        divProgress.style.display = 'block';
        divWindow.appendChild(divProgress);
        divProgress.classList.add("progress");
        divProgress.style.zIndex = 10000
        divWindow.style.cursor = "wait";
        processingInfo.blockLen = 16384 * 5;
        if (DataSet.Overlays.hasOwnProperty(divWindow.id))
            delete DataSet.Overlays[divWindow.id];
        processingInfo.indices.start = 0;
        processingInfo.indices.end = wavesurfer.backend.buffer.length;
        if (selectedRegion) {
            processingInfo.indices.start = Math.round(selectedRegion.start * wavesurfer.backend.buffer.sampleRate);
            processingInfo.indices.end = Math.floor(selectedRegion.end * wavesurfer.backend.buffer.sampleRate);
        }
        if (divWindow.type == 'PluginType.insert') {
            if (!PluginParamsStructured[divWindow.id].params.hasOwnProperty("duration"))
                throw new Error("Strict necessary parameter 'duration' is missing in Plugin '" + divWindow.id + "'!");
            processingInfo.blockLen = 1024 * 4;
            if (!selectedRegion)
                processingInfo.indices.start = Math.round(wavesurfer.getCurrentTime() * wavesurfer.backend.buffer.sampleRate);
            processingInfo.indices.end = processingInfo.indices.start + PluginParamsStructured[divWindow.id].params.duration * wavesurfer.backend.buffer.sampleRate;
            await cutCopyAudioData(true, false, true, false);
            setMemoryBuffer([]);
        }
        await new Promise(resolve => setTimeout(resolve, 0));
        processingInfo.indices.current = processingInfo.indices.start;
        var temSettings = JSON.parse(JSON.stringify(PluginParamsStructured))
        temSettings[PluginId].Init = { sampleRate: wavesurfer.backend.buffer.sampleRate, channels: wavesurfer.backend.buffer.numberOfChannels };
        processingInfo.divWindows = getPluginWindows();
        socket.emit('message', { PluginId: PluginId, settings: temSettings });
    }
}

export async function nextWavesurferBufferBlock(data) {
    var audioDataBuffer = [];
    for (var channel = 0; channel < wavesurfer.backend.buffer.numberOfChannels; channel++) {
        audioDataBuffer.push(new Float32Array(Math.min(processingInfo.blockLen, processingInfo.indices.end - processingInfo.indices.current)));
        if (!(!wavesurfer.isPlaying && processingInfo.divWindows.length == 1 && processingInfo.divWindows[0].type == 'PluginType.insert'))
            wavesurfer.backend.buffer.copyFromChannel(audioDataBuffer[channel], channel, processingInfo.indices.current);
    }
    if (audioDataBuffer.length > 0 && audioDataBuffer[0].length > 0)
        fromSource({ data: audioDataBuffer, PluginId: data.PluginId, settings: lastSettings })
    else
        socket.emit('message', Object.assign({ postProcess: true, PluginId: data.PluginId, settings: lastSettings }));
}

export async function fromSource(data) {
    processingInfo.divWindows = getPluginWindows();
    if (processingInfo.divWindows.find(plugin => plugin.isModal))
        processingInfo.divWindows = [processingInfo.divWindows.find(plugin => plugin.isModal)];
    if ((data.hasOwnProperty("delayInSamples"))) {
        document.getElementById("delay-value").innerText = ("000" + (data.delayInSamples / wavesurfer.backend.buffer.sampleRate * 1000).toFixed(2)).slice(-6);
    }
    if (data.hasOwnProperty("RMS")) {
        for (var channel = 0; channel < Math.min(2, data.RMS.length); channel++)
            document.getElementById("VUmeter" + channel).style.width = Math.min(100, Math.max(0, 100 + 20 * Math.log10(data.RMS[channel] + Number.MIN_VALUE))) + "%";
    }
    if (data.hasOwnProperty("data")) {
        if (processingInfo.divWindows.find(plugin => plugin.isServerSide))
            socket.emit('message', { data: data.data, PluginId: data.PluginId, settings: lastSettings });
        else
            wavesurfer.fireEvent('wolaClientProcess', { data: data.data, PluginId: data.PluginId, settings: lastSettings });
    }
}

export async function wolaClientProcess(data) {
    var audioDataBuffer = [];
    for (var channel = 0; channel < wavesurfer.backend.buffer.numberOfChannels; channel++) {
        if (data.data instanceof ArrayBuffer)
            audioDataBuffer.push(new Float32Array(data.data.slice(channel * data.data.byteLength / wavesurfer.backend.buffer.numberOfChannels, channel * data.data.byteLength / wavesurfer.backend.buffer.numberOfChannels + Math.min(data.data.byteLength / wavesurfer.backend.buffer.numberOfChannels, (processingInfo.indices.end - processingInfo.indices.current) * 4))));
        else if (data.data instanceof Array && (data.data[0] instanceof Float32Array || data.data[0] instanceof Array))
            audioDataBuffer.push(data.data[channel]);
        else if (data.data == false && audioDataBuffer[channel])
            audioDataBuffer.push(audioDataBuffer[channel]);
    }
    if (audioDataBuffer && audioDataBuffer.length)
        data.data = audioDataBuffer;
    if (data.data && data.data.length && data.data[0].length) {
        var idx = { val: 0 };
        while (idx.val < processingInfo.divWindows.length) {
            var divWindow = processingInfo.divWindows[idx.val];
            if (divWindow.hasOwnProperty("pluginJSObject") && divWindow.pluginJSObject)
                data.data = await wola(processingInfo.divWindows, data.data, idx, PluginParamsStructured);
            else
                idx.val += 1;
        }
    }
    toSink(data)
}

export async function toSink(data) {
    if (data.hasOwnProperty("settings") && data.settings && processingInfo.divWindows.length) {
        for (var idx = 0; idx < processingInfo.divWindows.length; idx++) {
            if (data.settings.hasOwnProperty(processingInfo.divWindows[idx].id)) {
                for (const [key, value] of Object.entries(data.settings[processingInfo.divWindows[idx].id]))
                    updateLastSettings(processingInfo.divWindows[idx].id, key, value);
            }
        }
    }
    if (data.hasOwnProperty("Overlays_2D") || data.hasOwnProperty("Overlays_3D")) {
        for (var idx = 0; idx < processingInfo.divWindows.length; idx++) {
            var values = [];
            if (selectedRegion)
                values = new Array(Math.round(selectedRegion.start * wavesurfer.backend.buffer.sampleRate / (data.settings[processingInfo.divWindows[idx].id].params.blockLen * (data.settings[processingInfo.divWindows[idx].id].params.timeDomainWithoutOverlap ? 1 : .5))));
            if (data.hasOwnProperty("Overlays_2D") && data.Overlays_2D.hasOwnProperty(processingInfo.divWindows[idx].id) && data.Overlays_2D.hasOwnProperty(processingInfo.divWindows[idx].id)) {
                if (!DataSet.Overlays.hasOwnProperty(processingInfo.divWindows[idx].id)) {
                    DataSet.Overlays[processingInfo.divWindows[idx].id] = { values: values, visible: true, domain: "time" };
                }

                DataSet.Overlays[processingInfo.divWindows[idx].id].values = DataSet.Overlays[processingInfo.divWindows[idx].id].values.concat(data.Overlays_2D[processingInfo.divWindows[idx].id]);
            }
            if (data.hasOwnProperty("Overlays_3D") && data.Overlays_3D.hasOwnProperty(processingInfo.divWindows[idx].id) && data.Overlays_3D.hasOwnProperty(processingInfo.divWindows[idx].id)) {
                if (!DataSet.Overlays.hasOwnProperty(processingInfo.divWindows[idx].id))
                    DataSet.Overlays[processingInfo.divWindows[idx].id] = { values: values, visible: true, domain: "frequency" };
                DataSet.Overlays[processingInfo.divWindows[idx].id].values = DataSet.Overlays[processingInfo.divWindows[idx].id].values.concat(data.Overlays_3D[processingInfo.divWindows[idx].id]);
            }
        }
    }
    if (data.hasOwnProperty("annotations")) {
        for (var idx = 0; idx < data.annotations[0].length; idx++) {
            var item = JSON.parse(data.annotations[0][idx])
            item.id = ""
            await addImportedAnnotation(item);
        }
    }
    if (!wavesurfer.isPlaying() && data.data && data.data.length && data.data[0].length && processingInfo.divWindows.length == 1 && processingInfo.divWindows[0].type == 'PluginType.insert')
        AppendToMemoryBuffer(data.data);
    else if (!wavesurfer.isPlaying() && data.data && data.data.length && data.data[0].length && processingInfo.divWindows.length > 0) {
        for (var channel = 0; channel < wavesurfer.backend.buffer.numberOfChannels; channel++) {
            wavesurfer.backend.buffer.copyToChannel(data.data[channel], channel, processingInfo.indices.current);
        }
    }
    else if (wavesurfer.isPlaying() && wavesurfer.scriptNode && wavesurfer.scriptNode.port)
        wavesurfer.scriptNode.port.postMessage({ data: data.data, counter: data.counter });

    if (document.getElementById(data.PluginId + "_progress")) {
        var divWindow = document.getElementById(data.PluginId);
        document.getElementById(divWindow.id + "_progress").querySelector('.progress-bar').style.width = Math.round((processingInfo.indices.current - processingInfo.indices.start) / (processingInfo.indices.end - processingInfo.indices.start) * 100) + '%';
        await new Promise(resolve => setTimeout(resolve, 0));
    }
    if (!data.hasOwnProperty("Init"))
        processingInfo.indices.current = Math.min(processingInfo.indices.end, processingInfo.indices.current + processingInfo.blockLen);
    if (!wavesurfer.isPlaying() && processingInfo.source == sources.WavesurferBuffer && document.getElementById(data.PluginId + "_progress"))
        wavesurfer.fireEvent('nextWavesurferBufferBlock', { data: data.data, PluginId: data.PluginId, settings: lastSettings });
}

var refreshCount = 0;
export async function updateOverlays() {
    var myRefreshCount = ++refreshCount;
    await new Promise(resolve => setTimeout(resolve, 100));
    for (const [key, Overlay] of Object.entries(DataSet.Overlays)) {
        if (Overlay.domain == "time" && Overlay.visible && showWave && myRefreshCount == refreshCount) {
            var start = 0;
            for (var canvasIdx = 0; canvasIdx < wavesurfer.drawer.canvases.length; canvasIdx++) {
                for (var channel = 0; channel < wavesurfer.backend.buffer.numberOfChannels; channel++) {
                    var progressImageData = null;
                    var canvas = wavesurfer.drawer.canvases[canvasIdx]
                    var height = wavesurfer.drawer.height / wavesurfer.backend.buffer.numberOfChannels;
                    var width = canvas.wave.width;
                    var width2 = parseInt(Overlay.values.length * canvas.wave.width / wavesurfer.drawer.width);
                    var imageData = canvas.wave.getContext('2d').getImageData(0, channel * height, width, height);
                    if (wavesurfer.drawer.hasProgressCanvas)
                        progressImageData = canvas.progress.getContext('2d').getImageData(0, channel * height, width, height);
                    for (var idx = start; idx < Math.min(start + width2, Overlay.values.length); idx++) {
                        if (Overlay.values[idx] && Math.abs(Overlay.values[idx][channel][0]) <= 1) {
                            if (!Array.isArray(Overlay.values[idx][channel][1]))
                                Overlay.values[idx][channel][1] = Overlay.values[idx][channel][1].match(/\d+/g)
                            var color = Overlay.values[idx][channel][1];
                            for (var pos = 0; pos < color.length; pos++) {
                                color[pos] = parseInt(color[pos]);
                                if (color[pos] > 0 && color[pos] < 1)
                                    color[pos] *= 255;
                            }
                            if (color.length < 3)
                                color = [color[0], color[0], color[0]]
                            if (color.length < 4)
                                color[3] = 255;
                            for (var pixel = 0; pixel < Math.ceil(width / width2); pixel++) {
                                var redIndex = ((parseInt((height - Overlay.values[idx][channel][0] * height * wavesurfer.params.barHeight) / 2)) * width + parseInt((idx - start) * width / width2) + pixel) * 4;
                                imageData.data[redIndex + 0] = color[0];
                                imageData.data[redIndex + 1] = color[1];
                                imageData.data[redIndex + 2] = color[2];
                                imageData.data[redIndex + 3] = color[3];
                                if (progressImageData) {
                                    progressImageData.data[redIndex + 0] = color[0];
                                    progressImageData.data[redIndex + 1] = color[1];
                                    progressImageData.data[redIndex + 2] = color[2];
                                    progressImageData.data[redIndex + 3] = color[3];
                                }
                            }
                        }
                    }
                    await canvas.wave.getContext('2d').putImageData(imageData, 0, channel * height);
                    if (progressImageData)
                        await canvas.progress.getContext('2d').putImageData(progressImageData, 0, channel * height);
                }
                start += width2;
            }
        }
        if (Overlay.domain == "frequency" && Overlay.visible && showSpec && wavesurfer.spectrogram && myRefreshCount == refreshCount) {
            for (var channel = 0; channel < wavesurfer.backend.buffer.numberOfChannels; channel++) {
                var height = wavesurfer.spectrogram.canvas.height / wavesurfer.backend.buffer.numberOfChannels;
                var width = wavesurfer.spectrogram.canvas.width;
                var width2 = Overlay.values.length;
                var imageData = wavesurfer.spectrogram.canvas.getContext('2d').getImageData(0, channel * height, width, height);
                for (var idx = 0; idx < Overlay.values.length; idx++) {
                    if (Overlay.values[idx] && Overlay.values[idx][channel][0] >= SystemPreferences.frequencyRange[0] && Overlay.values[idx][channel][0] <= SystemPreferences.frequencyRange[1]) {
                        if (!Array.isArray(Overlay.values[idx][channel][1]))
                            Overlay.values[idx][channel][1] = Overlay.values[idx][channel][1].match(/\d+/g)
                        var color = Overlay.values[idx][channel][1];
                        for (var pos = 0; pos < color.length; pos++) {
                            color[pos] = parseInt(color[pos]);
                            if (color[pos] > 0 && color[pos] < 1)
                                color[pos] *= 255;
                        }
                        if (color.length < 3)
                            color = [color[0], color[0], color[0]]
                        if (color.length < 4)
                            color[3] = 255;
                        for (var pixelY = 0; pixelY < Math.ceil(height / (SystemPreferences.frequencyRange[1] - SystemPreferences.frequencyRange[0])); pixelY++) {
                            for (var pixelX = 0; pixelX < Math.ceil(width / width2); pixelX++) {
                                var redIndex = ((height - parseInt(pixelY + (Overlay.values[idx][channel][0] - SystemPreferences.frequencyRange[0]) / (SystemPreferences.frequencyRange[1] - SystemPreferences.frequencyRange[0]) * height)) * width + parseInt(idx * width / width2) + pixelX) * 4;
                                imageData.data[redIndex + 0] = color[0] * 255;
                                imageData.data[redIndex + 1] = color[1] * 255;
                                imageData.data[redIndex + 2] = color[2] * 255;
                                imageData.data[redIndex + 3] = color[3] * 255;
                            }
                        }
                    }
                }
                await wavesurfer.spectrogram.canvas.getContext('2d').putImageData(imageData, 0, channel * height);
            }
        }
    }
    updateOverlayList();
}