import { gray1, wavesurfer, getCookie, socket, windowResize, UserSettings, showListPlugins, setShowListPlugins } from "./script.js";
import { dragElement } from "./drag_n_drop.js";
import { selectedRegion } from "./annotation.js";
import { updateOverlays, updateLastSettings, initProcessing, sources, processingInfo } from "./processing.js";
import { updateDropDownMenuCheckboxes } from "./dropdown.js";

export var PluginParams = {};
export var PluginParamsStructured = {};

export function loadPlugin(data) {
    const connector = fetch('/loadPlugin', {
        method: "POST",
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(data),
        credentials: 'same-origin'
    })
        .then((response) => {
            if (response.ok) {
                return response.json();
            }
            else {
                socket._callbacks['$error'][0]({target: data.id, message: response.statusText})
                return null;
            }
            //if (response.ok) return response.json();
            //throw new Error(response.statusText);
         }) 
        .then((connector) => { return connector; })
        /*.catch(function(error) {
                wavesurfer.pause();
                document.getElementById(data.id + "_content").innerHTML = error;
                if (document.getElementById(data.id + "Apply"))
                    document.getElementById(data.id + "Apply").disabled = true;
                if (document.getElementById(data.id + "OK"))
                    document.getElementById(data.id + "OK").disabled = true;
                document.getElementById(data.id).setEnable(true);
            });*/        
    const init = async () => {
        var values = null;
        values = JSON.parse(await connector);
        if (values) {
            var divWindow = document.getElementById(data.id)
            divWindow.gui.container.style.height = document.getElementById(data.id + "_content").offsetHeight + "px";
            for (const [key, value] of Object.entries(values.params)) {
                PluginParams[data.id + "@" + key] = value;
                paramChangedManually(data.id + "@" + key, PluginParams[data.id + "@" + key], false);
            }
            if (values.hasOwnProperty("script") && values.script && values.script.length) {
                if (eval("typeof pluginJSObject_" + divWindow.id) !== 'function') {
                    var script = document.createElement('script');
                    var inlineScript = document.createTextNode("(async () => {await import('./static/JS/pluginHelper.js');})();\nclass pluginJSObject_" + divWindow.id + " {\n" + values["script"].replace(/function /gi, "") + "}");
                    script.appendChild(inlineScript); 
                    document.head.appendChild(script); 
                }
                divWindow.pluginJSObject = eval("new pluginJSObject_" + divWindow.id + "()");
            }
            divWindow.isServerSide = values["isServerSide"];
            divWindow.info = values["info"];
            if (divWindow.info)
                document.getElementById(divWindow.id + "_Info").style.display = "block";
            var imageIdxs = [];
            if (values.hasOwnProperty("controls") && values.controls && values.controls.length) {
                for (var idx = 0; idx < values.controls.length; idx++) {
                    if (values.controls[idx]["property"]) {
                        var paramstr = data.id + "@" + values.controls[idx]["property"];
                        if (divWindow.type == 'PluginType.insert' && values.controls[idx]["property"].toLowerCase() == "duration") {
                            divWindow.durationValueName = paramstr;
                            if (selectedRegion)
                                values.params[values.controls[idx].property] = selectedRegion.end - selectedRegion.start;
                        }
                        values.controls[idx]["object"] = PluginParams;
                        values.controls[idx]["property"] = paramstr;
                        if (values.controls[idx]["type"].toLowerCase() != "button") {
                            values.controls[idx]["onChange"] = function (val) {
                                paramChangedManually(this.property, val);
                            };
                        }
                        if (values.controls[idx]["type"].toLowerCase() == "image") {
                            imageIdxs.push(idx);
                            values.controls[idx]["image"] = values.controls[idx]["property"];
                            values.controls[idx]["property"] = "";
                        }
                    }
                    if (values.controls[idx]["type"].toLowerCase() == "image")
                        values.controls[idx]["type"] = "display"
                }
                if (divWindow.type == 'PluginType.insert' && !divWindow.hasOwnProperty("durationValueName"))
                    throw new Error("Parameter 'Duration' is necessary for PluginType.insert!");
                socket.emit('message', { settings: PluginParamsStructured });
                divWindow.gui.Register(values.controls);
                if (divWindow.hasOwnProperty("pluginJSObject") && typeof divWindow.pluginJSObject.init === "function")
                    divWindow.pluginJSObject.init(PluginParamsStructured[divWindow.id]["params"], values.controls);
            }
            else if (divWindow.hasOwnProperty("pluginJSObject") && typeof divWindow.pluginJSObject.init === "function")
                divWindow.pluginJSObject.init(PluginParamsStructured[divWindow.id]["params"], null);
            for (idx = 0; idx < imageIdxs.length; idx++) {
                var img = window.document.createElement('img');
                img.id = "image";
                img.style.width = "100%";
                img.style.height = (+Number(values.controls[imageIdxs[idx]].height.replace("px", "")) || 50) + "px";
                divWindow.gui.loadedComponents[imageIdxs[idx]].text.appendChild(img);
                divWindow.gui.loadedComponents[imageIdxs[idx]].isImage = true;
            }
            receiveParams(PluginParamsStructured);
            divWindow.setEnable(true);
        }
    };
    init();
}

function unloadPlugin(pluginId) {
    for (const [key, value] of Object.entries(PluginParams)) {
        if (key.startsWith(pluginId + "@"))
            delete PluginParams[key];
    }
    if (PluginParamsStructured && PluginParamsStructured.hasOwnProperty(pluginId))
        delete PluginParamsStructured[pluginId];
    const connector = fetch('/unloadPlugin', {
        method: "POST",
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ id: pluginId }),
        credentials: 'same-origin'
    })
    const remove = async () => {
        const data = await connector;
    };
    remove();
}

export async function loadPluginList(data) {
    const connector = fetch('/loadPluginList', {
        method: "GET",
        headers: { 'Content-Type': 'application/json' },
        credentials: 'same-origin'
    })
        .then((response) => response.json())
        .then((connector) => { return connector; })
    const load = async () => {
        const plugins = JSON.parse(await connector);
        var PluginName = "_ListPlugins"
        var divWindow = document.getElementById(PluginName);
        if (showListPlugins) {
            if (!divWindow) {
                createPopUpWindow(PluginName, "Plugin-List", false, false)
                divWindow = document.getElementById(PluginName);
                divWindow.setEnable(true);
                divWindow.onclose = function (event) {
                    setShowListPlugins(false);
                    closePlugin(this.id);
                }
                dockWindow(divWindow, "right");
                var divControls = document.getElementById(PluginName + "_controls");
                divControls.style.display = "hidden";
            }
            document.getElementById(PluginName + "_content").innerHTML = "";
        }
        var menuNode = document.getElementById("Plugins");
        menuNode.innerHTML = "";
        var a = null;
        if (plugins && plugins.length) {
            for (var idx = 0; idx < plugins.length; idx++) {
                a = window.document.createElement('a');
                a.id = plugins[idx].id + "_MenuLink";
                a.text = plugins[idx].name.replace("_", " ");
                a.PluginName = plugins[idx].id;
                a.DisplayName = plugins[idx].name;
                a.type = plugins[idx].type;
                a.isModal = plugins[idx].isModal || a.type == "PluginType.insert"
                a.isTemp = plugins[idx].isTemp
                a.style.pointerEvents = "none";
                a.style.color = gray1;

                if (a.isTemp) {
                    a.text = "[" + a.text + "]";
                }
                a.addEventListener("click", function (e) {
                    if (!document.getElementById(this.PluginName)) {
                        if (!this.isModal) {
                            this.style.pointerEvents = "none";
                            this.style.color = gray1;
                            if (document.getElementById(this.id + "_copy")) {
                                document.getElementById(this.id + "_copy").style.pointerEvents = this.style.pointerEvents;
                                document.getElementById(this.id + "_copy").style.color = this.style.color;
                            }
                        }
                        if (this.type == "PluginType.insert")
                            wavesurfer.pause();
                        document.getElementById("btnRecord").disabled = true;
                        var audiobuffer = wavesurfer.backend.buffer;
                        createPopUpWindow(this.PluginName, "PlugIn " + (document.getElementsByClassName("pluginwindow").length + 1) + ": " + this.PluginName.replace("_", " "), this.isModal, true)
                        loadPlugin({ id: this.PluginName, sampleRate: audiobuffer.sampleRate, channels: audiobuffer.numberOfChannels, signalLength: audiobuffer.duration });
                        var divWindow = document.getElementById(this.PluginName);
                        var divContent = document.getElementById(this.PluginName + "_content");
                        var divControls = document.getElementById(this.PluginName + "_controls");

                        divWindow.onclose = function (id) {
                            closePlugin(id);
                        }
                        divWindow.type = this.type;
                        divWindow.openLink = this;
                        divControls.style.display = "block";

                        var divControlButtons = window.document.createElement('div');
                        divControlButtons.id = divWindow.id + "PresetControls"
                        divControlButtons.className = 'controls';
                        divControlButtons.style.float = "left";
                        divControls.className = "controlsArea";
                        divControls.style.paddingLeft = "0px";
                        divControls.appendChild(divControlButtons);

                        var btn = null;
                        var datalist = window.document.createElement('datalist');
                        datalist.id = divWindow.id + "PresetList";
                        datalist.parent = divWindow.id;
                        divWindow.PresetList = JSON.parse(getCookie("PresetList_" + divWindow.id, "{}"));
                        Object.keys(divWindow.PresetList).forEach(function (key) {
                            var option = document.createElement('option');
                            option.value = key;
                            datalist.appendChild(option);
                        })
                        divControlButtons.appendChild(datalist);

                        var input = window.document.createElement('Input');
                        input.id = divWindow.id + "Presets";
                        input.className = 'btn subwindowcontrols_btn';
                        input.setAttribute("list", divWindow.id + "PresetList");
                        input.autocomplete = "off";
                        input.style.width = "150px";
                        input.style.color = "black";
                        input.style.float = "left";
                        input.style.textAlign = "left";
                        input.PluginName = this.PluginName;
                        input.addEventListener("click", function (e) {
                            this.select();
                        });
                        input.addEventListener("keydown", function (e) {
                            this.dispatchEvent(new Event("change"));
                        });
                        input.addEventListener("change", function (e) {
                            var divWindow = document.getElementById(this.id.replace("Presets", ""))
                            if (divWindow.PresetList && divWindow.PresetList[document.getElementById(divWindow.id + "Presets").value]) {
                                var value = {}
                                value[divWindow.id] = { "params": divWindow.PresetList[document.getElementById(divWindow.id + "Presets").value] };
                                receiveParams(value);
                                //PluginParamsStructured.hasChanges = false;
                                socket.emit('message', { settings: PluginParamsStructured });
                                document.getElementById(divWindow.id + "SavePreset").disabled = true;
                                document.getElementById(divWindow.id + "DeletePreset").disabled = false;
                            }
                            else
                                document.getElementById(divWindow.id + "DeletePreset").disabled = true;
                        });
                        divControlButtons.appendChild(input);

                        btn = window.document.createElement('button');
                        btn.id = divWindow.id + "SavePreset";
                        btn.innerHTML = "<i class='glyphicon glyphicon-save-file'></i>";
                        btn.className = 'btn btn-primary';
                        btn.disabled = true;
                        btn.addEventListener("click", function (e) {
                            var divWindow = document.getElementById(this.id.replace("SavePreset", ""))
                            if (document.getElementById(divWindow.id + "Presets").value != "") {
                                if (!divWindow.PresetList)
                                    divWindow.PresetList = {};
                                var List = Object.assign({}, PluginParamsStructured[divWindow.id]["params"]);
                                delete List.sampleRate;
                                delete List.channels;
                                delete List.signalLength;
                                divWindow.PresetList[document.getElementById(divWindow.id + "Presets").value] = List;
                                document.cookie = "PresetList_" + divWindow.id + "=" + JSON.stringify(divWindow.PresetList) + ";path=/; max-age=31536000";
                                document.getElementById(divWindow.id + "SavePreset").disabled = true;
                                var option = document.createElement('option');
                                option.value = document.getElementById(divWindow.id + "Presets").value;
                                document.getElementById(divWindow.id + "PresetList").appendChild(option);
                            }
                        });
                        divControlButtons.appendChild(btn);

                        btn = window.document.createElement('button');
                        btn.id = divWindow.id + "DeletePreset";
                        btn.innerHTML = "<i class='glyphicon glyphicon-remove'></i>";
                        btn.className = 'btn btn-primary';
                        btn.disabled = true;
                        btn.addEventListener("click", function (e) {
                            var divWindow = document.getElementById(this.id.replace("DeletePreset", ""))
                            if (document.getElementById(divWindow.id + "Presets").value != "" && divWindow.PresetList[document.getElementById(divWindow.id + "Presets").value]) {
                                delete divWindow.PresetList[document.getElementById(divWindow.id + "Presets").value];
                                document.cookie = "PresetList_" + divWindow.id + "=" + JSON.stringify(divWindow.PresetList) + ";path=/; max-age=31536000";
                                document.getElementById(divWindow.id + "SavePreset").disabled = true;
                                document.getElementById(divWindow.id + "DeletePreset").disabled = true;
                                document.getElementById(divWindow.id + "PresetList").remove(document.getElementById(divWindow.id + "Presets").value);
                                document.getElementById(divWindow.id + "Presets").value = "";
                            }
                        });
                        divControlButtons.appendChild(btn);

                        divControlButtons = window.document.createElement('div');
                        divControlButtons.className = 'controls';
                        divControlButtons.style.marginRight = "10px";
                        divControlButtons.style.float = "right";

                        divControls.appendChild(divControlButtons);

                        if (this.type == 'PluginType.persistent') { //  || this.type == 'PluginType.insert'
                            btn = window.document.createElement('button');
                            btn.id = divWindow.id + "Apply";
                            btn.innerText = "Apply";
                            btn.className = 'btn btn-primary subwindowcontrols_btn';
                            btn.PluginName = this.PluginName;
                            btn.addEventListener("click", onClick_Apply);
                            divControlButtons.appendChild(btn);
                        }
                        if (this.type == 'PluginType.persistent'|| this.type == 'PluginType.insert') {
                            btn = window.document.createElement('button');
                            btn.id = divWindow.id + "Cancel";
                            btn.innerText = "Cancel";
                            btn.className = 'btn btn-primary subwindowcontrols_btn';
                            btn.PluginName = this.PluginName;
                            btn.addEventListener("click", function (e) {
                                closePlugin(this.PluginName);
                            });
                            divControlButtons.appendChild(btn);
                        }
                        btn = window.document.createElement('button');
                        btn.id = divWindow.id + "OK";
                        btn.innerText = "OK";
                        btn.className = 'btn btn-primary subwindowcontrols_btn';
                        btn.PluginName = this.PluginName;
                        btn.addEventListener("click", onClick_OK);
                        divControlButtons.appendChild(btn);
                    }
                });
                if (document.getElementById(plugins[idx].id)) {
                    a.style.pointerEvents = "none";
                    a.style.color = gray1;
                }
                else {
                    a.style.pointerEvents = "";
                    a.style.color = "black";
                }
                menuNode.appendChild(a);
                if (showListPlugins) {
                    var copy = a.cloneNode(true);
                    copy.id += "_copy"
                    copy.style.display = "block";
                    copy.style.paddingLeft = "5px";
                    if (document.getElementById(plugins[idx].id)) {
                        copy.style.pointerEvents = "none";
                        copy.style.color = gray1;
                    }
                    else {
                        copy.style.pointerEvents = "";
                        copy.style.color = "white";
                    }    
                    copy.addEventListener("click", function (e) {
                        document.getElementById(this.id.replace("_copy", "")).click();
                    });
                    document.getElementById(PluginName + "_content").appendChild(copy);
                }
            }
            document.getElementById("PluginsMenu").style.pointerEvents = "";
        }
    };
    document.getElementById("PluginsMenu").style.pointerEvents = "none";
    await load();
    if (UserSettings.hasOwnProperty("isDeveloper") && UserSettings["isDeveloper"]) {
        var menuNode = document.getElementById("Plugins");
        var hr = window.document.createElement('hr');
        hr.style.cssText = "margin:5px; border: 1px solid #8F9092;"
        menuNode.appendChild(hr);
        var a = window.document.createElement('a');
        a.id = "importPlugin";
        a.text = "Import Plugin";
        a.addEventListener("click", async function (e) { await importPlugin(); });
        menuNode.appendChild(a);
    }
}

export async function onClick_OK(e) {
    var applyButton = document.getElementById(e.target.id.substring(0, e.target.id.length - 2) + "Apply")
    if (applyButton)
        applyButton.disabled = true;
    await initProcessing(this.PluginName, sources.WavesurferBuffer);
}

async function onClick_Apply(e) {
    var okButton = document.getElementById(e.target.id.substring(0, e.target.id.length - 5) + "OK")
    if (okButton)
        okButton.disabled = true;
    await initProcessing(this.PluginName, this.innerText == "Apply" ? sources.WavesurferBuffer : sources.StreamProcessor);
}

export async function closePlugin(pluginId) {
    var divWindow = document.getElementById(pluginId);
    if (divWindow) {
        if (divWindow.isModal) {
            var subwindows = document.getElementsByClassName("pluginwindow");
            for (var idx = 0; idx < subwindows.length; idx++) {
                subwindows[idx].setEnable(true);
            }
            if (!pluginId.startsWith("_"))
                wavesurfer.pause();
        }
        var offset = divWindow.getBoundingClientRect();
        var pos = [offset.left, offset.top, offset.width, offset.height]
        document.cookie = "PluginPos_" + pluginId + "=" + pos + ";path=/; max-age=31536000";
        if (divWindow && document.getElementById(pluginId + "_disable").style.visibility != "visible") {
            document.getElementById("hideWavesurfer").style.visibility = "hidden";
            unloadPlugin(pluginId);
            if (divWindow.openLink) {
                divWindow.openLink.style.pointerEvents = "";
                divWindow.openLink.style.color = "black";
                if (document.getElementById(divWindow.openLink.id + "_copy")) {
                    document.getElementById(divWindow.openLink.id + "_copy").style.pointerEvents = divWindow.openLink.style.pointerEvents;
                    document.getElementById(divWindow.openLink.id + "_copy").style.color = "white";
                }
            }
            divWindow.parentElement.removeChild(divWindow);
            for (const [key, value] of Object.entries(PluginParams)) {
                if (key.startsWith(pluginId + "@"))
                    delete PluginParams[key];
            }
        }
    }
    document.getElementById("btnRecord").disabled = (getPluginWindows().length != 0);
    updateDropDownMenuCheckboxes();
    if (divWindow && divWindow.hasOwnProperty("dock") && divWindow.dock)
        windowResize();
    await updateOverlays();
}

export function getPluginWindows() {
    var divWindows = Array.from(document.getElementsByClassName("pluginwindow"));
    return divWindows.filter(plugin => !plugin.id.startsWith("_") && !plugin.id.startsWith("PopUpWindow_"));
}

function paramChangedManually(key, value, sendToServer = true) {
    var keyParts = key.split("@")
    PluginParams[key] = value;
    var divWindow = document.getElementById(keyParts[0]);
    if (divWindow) {
        if (document.getElementById(divWindow.id).type == 'PluginType.persistent')
            document.getElementById(divWindow.id + "Apply").disabled = false;
        if (!PluginParamsStructured.hasOwnProperty("hasChanges"))
            PluginParamsStructured.hasChanges = false;
        if (!PluginParamsStructured.hasOwnProperty(divWindow.id) || !PluginParamsStructured[divWindow.id])
            PluginParamsStructured[divWindow.id] = { "params": {} };
        if (!PluginParamsStructured[divWindow.id]["params"].hasOwnProperty(keyParts[1]) || PluginParamsStructured[divWindow.id]["params"][keyParts[1]] != value) {
            PluginParamsStructured[divWindow.id]["params"][keyParts[1]] = value;
            PluginParamsStructured.hasChanges = true;
            updateLastSettings(divWindow.id, keyParts[1], value);
        }
        else
            sendToServer = false;
        for (var idx = 0; idx < divWindow.gui.loadedComponents.length; idx++) {
            if (divWindow.gui.loadedComponents[idx].binding && divWindow.gui.loadedComponents[idx].binding.property == key)
                divWindow.gui.loadedComponents[idx].SetEnabled(value != null)
        }
        if (sendToServer)
            socket.emit('message', { settings: PluginParamsStructured });
        document.getElementById(keyParts[0] + "SavePreset").disabled = false;
    }
}

export function receiveParams(values) {
    for (const [plugin, params] of Object.entries(values)) {
        var divWindow = document.getElementById(plugin);
        if (divWindow && params && params.hasOwnProperty("params")) {
            for (const [key, value] of Object.entries(params["params"])) {
                if (!PluginParams.hasOwnProperty(plugin + "@" + key))
                    PluginParams[plugin + "@" + key] = null;
                if (PluginParams[plugin + "@" + key] != value) {
                    PluginParamsStructured[plugin]["params"][key] = value;
                    PluginParams[plugin + "@" + key] = value;
                }
            }
            for (var idx = 0; idx < divWindow.gui.loadedComponents.length; idx++)
                if (divWindow.gui.loadedComponents[idx].binding && PluginParams.hasOwnProperty(divWindow.gui.loadedComponents[idx].binding.property)) {
                    divWindow.gui.loadedComponents[idx].SetEnabled(PluginParams[divWindow.gui.loadedComponents[idx].binding.property] != null);
                    divWindow.gui.loadedComponents[idx].SetValue(PluginParams[divWindow.gui.loadedComponents[idx].binding.property]);
                }
                else if (divWindow.gui.loadedComponents[idx].isImage) {
                    divWindow.gui.loadedComponents[idx].text.childNodes[0].src = PluginParams[divWindow.gui.loadedComponents[idx].opts.image];
                }
        }
    }
    //PluginParamsStructured.hasChanges = false;
    /*

    divWindow.gui.loadedComponents[imageIdxs[idx]].isImage
    var arrayBufferView = new Uint8Array(PluginParamsStructured["addNoise_Server"]['img'])
    var blob = new Blob( [ arrayBufferView ], { type: "image/jpeg" } );
    var urlCreator = window.URL || window.webkitURL;
    var imageUrl = urlCreator.createObjectURL( blob );
    var img = divWindow.gui.loadedComponents[2].text.children[0];
    img.src = imageUrl;
    */
}

export function createPopUpWindow(id, titel, isModal = false, gui = false) {
    if (isModal && document.getElementById("hideWavesurfer"))
        document.getElementById("hideWavesurfer").style.visibility = "visible";
    if (isModal && document.getElementsByClassName("pluginwindow")) {
        var subwindows = document.getElementsByClassName("pluginwindow");
        for (var idx = 0; idx < subwindows.length; idx++) {
            subwindows[idx].setEnable(false);
        }
    }
    var divWindow = window.document.createElement('div');
    divWindow.isModal = isModal;
    divWindow.classList.add("pluginwindow");
    divWindow.id = id;
    document.body.appendChild(divWindow);
    divWindow.onclose = function (event) {
        closePlugin(this.id);
    };
    divWindow.setEnable = async function (enable) {
        if (enable) {
            if (document.getElementById(this.id + "_progress"))
                divWindow.removeChild(document.getElementById(this.id + "_progress"));
            document.getElementById(this.id + "_disable").style.visibility = "hidden";
        }
        else
            document.getElementById(this.id + "_disable").style.visibility = "visible";
        if (document.getElementById(this.id).gui)
            document.getElementById(this.id).gui.panel.SetEnabled(enable);
        await new Promise(resolve => setTimeout(resolve, 0));
    }

    var divHeader = window.document.createElement('div');
    divHeader.id = id + "header";
    divHeader.classList.add("subwindowheader")
    divHeader.innerHTML = titel;
    divWindow.appendChild(divHeader);

    var a = window.document.createElement('a');
    a.id = id + "_closeLink";
    a.style.float = "right";
    a.style.color = "white";
    a.style.paddingRight = "5px";
    a.style.textDecoration = "none";
    a.innerHTML = "&#x2718";
    a.parent = id;
    a.onclick = function (event) {
        var divWindow = document.getElementById(this.parent);
        closePlugin(divWindow.id);
        if (divWindow.onclose)
            divWindow.onclose(this.parent);
    }
    divHeader.appendChild(a);

    a = window.document.createElement('a');
    a.id = id + "_Info";
    a.style.float = "right";
    a.style.color = "white";
    a.style.display = "none";
    a.style.textDecoration = "none";
    a.style.paddingTop = "1px";
    a.className = "glyphicon glyphicon-info-sign";
    a.style.paddingRight = "5px";
    a.parent = id;
    a.onclick = function (event) {
        var divParentWindow = document.getElementById(this.parent);
        var PluginName = "PluginInfo";
        createPopUpWindow(PluginName, "Plugin-Info: " + divParentWindow.id, true, false)
        var divWindow = document.getElementById(PluginName);
        var divContent = document.getElementById(PluginName + "_content");
        var divControls = document.getElementById(PluginName + "_controls");
        divControls.style.display = "none";
        divWindow.setEnable(true);
        divContent.innerHTML = divParentWindow.info;
        divContent.style.padding = "10px";
    }
    divHeader.appendChild(a);

    var divContent = window.document.createElement('div');
    divContent.id = id + "_content"
    divContent.classList.add("subwindowcontent")
    if (gui) {
        divWindow.gui = new guify({
            root: divContent,
            width: "100%",
            open: true,
            barMode: "none",
        });
        divWindow.gui.container.style.position = "relative";
        divWindow.gui.container.childNodes[0].style.overflowX = "hidden";
        divWindow.gui.container.childNodes[0].style.overflowY = "auto";
    }
    divWindow.appendChild(divContent);

    var divControls = window.document.createElement('div');
    divControls.id = id + "_controls"
    divControls.classList.add("subwindowcontrols")
    divControls.style.zIndex = 10;
    divControls.style.width = "100%";
    divWindow.appendChild(divControls);

    var positions = getCookie("PluginPos_" + divWindow.id);
    if (positions) {
        positions = positions.split(",");
        if (positions.length == 4) {
            divWindow.style.left = positions[0] + "px";
            divWindow.style.top = positions[1] + "px";
            divWindow.style.width = positions[2] + "px";
            divWindow.style.height = positions[3] + "px";
        }
    }
    else
        divWindow.style.height = "300px";

    var divWindowDisable = window.document.createElement('div');
    divWindowDisable.id = divWindow.id + "_disable";
    divWindowDisable.className = "hideWavesurfer";
    divWindow.appendChild(divWindowDisable);
    divWindow.setEnable(false);

    divWindow.style.visibility = "visible";
    divWindow.style.top = Math.max(Math.min(divWindow.offsetTop, window.innerHeight - divWindow.offsetHeight), 0) + "px";
    divWindow.style.left = Math.max(Math.min(divWindow.offsetLeft, window.innerWidth - divWindow.offsetWidth), 0) + "px";
    dragElement(divWindow);
    return divWindow;
}

export function dockWindow(divWindow, position) {
    if (divWindow) {
        var parent = document.getElementsByTagName("main")[0];
        if (position == "bottom")
            parent = document.getElementsByTagName("footer")[0];
        divWindow.dock = position;
        if (position == "right" || position == "bottom")
            parent.appendChild(divWindow);
        else if (position == "left")
            parent.insertBefore(divWindow, parent.children[0]);
        divWindow.style.position = "relative";
        divWindow.style.display = "flex";
        divWindow.style.height = "0px";
        divWindow.style.top = "0px";
        divWindow.style.left = "0px";
        divWindow.style.bottom = "0px";
        divWindow.style.right = "0px";
        divWindow.style.zIndex = 0;
        if (position == "left" || position == "right") {
            divWindow.style.minWidth = "350px";
            divWindow.style.width = "350px";
            divWindow.style.height = divWindow.parentElement.offsetHeight + "px";
        }
        if (position == "bottom") {
            divWindow.style.minHeight = "200px";
            divWindow.style.width = "350px";
            divWindow.style.height = "200px";
        }
        var divContent = document.getElementById(divWindow.id + "_content");
        divContent.style.overflowX = "hidden";
        divContent.style.overflowY = "auto";
        windowResize();
    }
}

async function importPlugin() {
    var input = document.createElement("input");
    input.type = "file";
    input.click();
    input.addEventListener('change', async ev => {
        let request = new XMLHttpRequest();
        let data = new FormData();
        for (var i = 0; i < input.files.length; i++) {
            if (input.files[i].name.toLowerCase().endsWith(".zip") || input.files[i].name.toLowerCase().endsWith(".py")) {
            if (input.files[i].hasOwnProperty("getAsFile"))
                data.append("plugin", input.files[i].getAsFile(), input.files[i].name);
            else
                data.append("plugin", input.files[i], input.files[i].name);
            }
        }
        if (data.getAll('plugin').length) {
            document.getElementById("hideWavesurfer").style.visibility = "visible";
            document.getElementById("progress-bar").style.display = 'block';
            await new Promise(resolve => setTimeout(resolve, 0));
            request.open('POST', '/importPlugin', true);
            request.upload.addEventListener('progress', function (e) {
            let percent_completed = (e.loaded / e.total) * 100;
            document.getElementById("progress-bar").querySelector('.progress-bar').style.width = Math.round(percent_completed) + '%';
            });
            request.addEventListener('load', async function (e) {
                await loadPluginList();
                document.getElementById("progress-bar").style.display = 'none';
                document.getElementById("hideWavesurfer").style.visibility = "hidden";
            });
            request.send(data);
        }
    }, false);
    return false;
}