import { addImportedAnnotation, deleteAnnotation, setDeletedRegions, setSelectedRegion } from "./annotation.js";
import { fitWindowSize } from "./dropdown.js";
import { IndexedDB } from "./IndexedDB.js";
import { DataSet, gray1, History, setAudioSignalHasChanges, setRefreshSpetrogram, wavesurfer } from "./script.js";

var historyMaxLen = 10;

export const HistoryType = {
    annotation: 1,
    audiodata: 2,
    annotationAndAudiodata: 3,
};

export async function undo() {
    if (document.getElementById("hideWavesurfer").style.visibility == "hidden") {
        document.getElementById("hideWavesurfer").style.visibility = "visible";
        await new Promise(resolve => setTimeout(resolve, 0));
        wavesurfer.pause();
        if (wavesurfer.backend.buffer.numberOfChannels && History.Idx >= 0 && document.getElementById("undo").style.pointerEvents == "") {
            var transaction = null;
            var Histories = null;
            var HistoryItem = null;
            if (IndexedDB) {
                transaction = IndexedDB.transaction("History", "readwrite");
                Histories = transaction.objectStore("History");
                HistoryItem = (await Histories.getAll(IDBKeyRange.only(History.Idx)))[0];
            }
            else
                HistoryItem = History.Items[History.Idx];
            var itemsCount = IndexedDB ? await Histories.count() : History.Items.length;
            if (History.Idx == itemsCount - 1) {
                await addToHistory(HistoryItem.HistoryType, "");
                History.Idx -= 1;
            }
            await updateHistory(History.Idx);
            History.Idx -= 1;
            await udpateHistoryMenu();
        }
        setSelectedRegion(null);
        document.getElementById("hideWavesurfer").style.visibility = "hidden";
    }
}

export async function redo() {
    if (document.getElementById("hideWavesurfer").style.visibility == "hidden") {
        document.getElementById("hideWavesurfer").style.visibility = "visible";
        await new Promise(resolve => setTimeout(resolve, 0));
        wavesurfer.pause();
        var transaction = null;
        var Histories = null;
        if (IndexedDB) {
            transaction = IndexedDB.transaction("History", "readwrite");
            Histories = transaction.objectStore("History");
        }
        var itemsCount = IndexedDB ? await Histories.count() : History.Items.length;
        if (wavesurfer.backend.buffer.numberOfChannels && History.Idx < itemsCount - 1 && document.getElementById("redo").style.pointerEvents == "") {
            History.Idx = Math.min(itemsCount - 1, (History.Idx + 2));
            await updateHistory(History.Idx);
            if (IndexedDB) {
                transaction = IndexedDB.transaction("History", "readwrite");
                Histories = transaction.objectStore("History");
            }
            itemsCount = IndexedDB ? await Histories.count() : History.Items.length;
            var HistoryItem = null;
            if (IndexedDB)
                HistoryItem = (await Histories.getAll(IDBKeyRange.only(itemsCount - 1)))[0];
            else
                HistoryItem = History.Items[itemsCount - 1];    
            if (History.Idx == itemsCount - 1 && HistoryItem.description == "") {
                if (IndexedDB)
                    await Histories.delete(itemsCount - 1);
                else
                    History.Items.pop();
                History.Idx = (IndexedDB ? await Histories.count() : History.Items.length) - 1;
            }
            else
                History.Idx--;
            await udpateHistoryMenu();
        }
        setSelectedRegion(null);
        document.getElementById("hideWavesurfer").style.visibility = "hidden";
    }
}

export async function clearHistory() {
    var transaction = null;
    var Histories = null;
    if (IndexedDB) {
        transaction = IndexedDB.transaction("History", "readwrite");
        Histories = transaction.objectStore("History");
        await Histories.clear()
    }
    else
        History.Items = [];
    History.Idx = -1;
    await udpateHistoryMenu();
}

export async function addToHistory(historyType, description) {
    var transaction = null;
    var Histories = null;
    if (IndexedDB) {
        transaction = IndexedDB.transaction("History", "readwrite");
        Histories = transaction.objectStore("History");
    }
    var itemsCount = IndexedDB ? await Histories.count() : History.Items.length;
    if (History.Idx != itemsCount - 1) {
        if (IndexedDB) {
            for (var item of await Histories.getAllKeys(IDBKeyRange.lowerBound(History.Idx + 1, false)))
                await Histories.delete(item);
        }
        else {
            for (var idx = itemsCount; idx > History.Idx + 1; idx--)
                History.Items.pop();
        }
    }
    itemsCount = IndexedDB ? await Histories.count() : History.Items.length;
    if (historyType == HistoryType.annotation || historyType == HistoryType.annotationAndAudiodata) {
        var tempDeleted = []
        for (const obj of DataSet.deletedRegions)
            tempDeleted.push(Object.assign({}, obj));
        var tempAnnotations = []
        Object.keys(wavesurfer.regions.list).forEach(element => {
            if (wavesurfer.regions.list[element].marker) {
                var item = {};
                item.start = new Date(wavesurfer.regions.list[element].marker.time * 1000).toISOString().substr(11, 12)
                item.end = new Date(wavesurfer.regions.list[element].marker.endTime * 1000).toISOString().substr(11, 12)
                item.id = wavesurfer.regions.list[element].id;
                item.value = wavesurfer.regions.list[element].markerLabel.textContent;
                item.tag_id = wavesurfer.regions.list[element].tag_id;
                item.hasChanges = false;
                if (wavesurfer.regions.list[element].hasOwnProperty("hasChanges"))
                    item.hasChanges = wavesurfer.regions.list[element].hasChanges;
                tempAnnotations.push(item);
            }
        });
        DataSet.annotations = [tempAnnotations, tempDeleted];
    }
    if (historyType == HistoryType.audiodata || historyType == HistoryType.annotationAndAudiodata) {
        DataSet.audiodata = [];
        for (var channel = 0; channel < wavesurfer.backend.buffer.numberOfChannels; channel++) {
            DataSet.audiodata.push(new Float32Array(wavesurfer.backend.buffer.length));
            wavesurfer.backend.buffer.copyFromChannel(DataSet.audiodata[channel], channel, 0);
        }
    }
    else if (DataSet.hasOwnProperty("audiodata"))
        delete DataSet.audiodata;
    DataSet.description = description;
    DataSet.HistoryType = historyType;
    DataSet.audioSignalHasChanges = setAudioSignalHasChanges(DataSet.audioSignalHasChanges);
    DataSet.id = itemsCount;
    if (IndexedDB)
        await Histories.add(Object.assign({}, DataSet));
    else
        History.Items.push(Object.assign({}, DataSet));
    if (DataSet.hasOwnProperty("audiodata"))
        delete DataSet.audiodata;
    if (DataSet.hasOwnProperty("annotations"))
        delete DataSet.annotations;
    delete DataSet.HistoryType;
    if (!IndexedDB) {
        var count = 0;
        for (var idx = 0; idx < History.Items.length; idx++)
            if (History.Items[idx].hasOwnProperty("audiodata") && History.Items[idx].audiodata.length)
                count++
        while (count >= historyMaxLen) {
            if (!(History.Items[idx].hasOwnProperty("audiodata") && History.Items[idx].audiodata.length))
                count--
            History.Items.shift();
        }
    }
    itemsCount = IndexedDB ? await Histories.count() : History.Items.length;
    History.Idx = itemsCount - 1;
    await udpateHistoryMenu();
}

async function updateHistory(idx) {
    var transaction = null;
    var Histories = null;
    if (IndexedDB) {
        transaction = IndexedDB.transaction("History", "readwrite");
        Histories = transaction.objectStore("History");
    }
    var itemsCount = IndexedDB ? await Histories.count() : History.Items.length;
    if (idx >= 0 && idx < itemsCount) {
        var blnDrawBuffer = false;
        var HistoryItem = null;
        if (IndexedDB) {
            HistoryItem = (await Histories.getAll(IDBKeyRange.only(idx)))[0];
        }
        else
            HistoryItem = History.Items[idx];
        if (HistoryItem.hasOwnProperty("annotations") && HistoryItem.annotations.length) {
            Object.keys(wavesurfer.regions.list).forEach(async element => {
                await deleteAnnotation(wavesurfer.regions.list[element], false);
            });
            Object.keys(HistoryItem.annotations[0]).forEach(async element => {
                var region = await addImportedAnnotation(HistoryItem.annotations[0][element]);
                region.hasChanges = HistoryItem.annotations[0][element].hasChanges;
            });
            setDeletedRegions(HistoryItem.annotations[1]);
        }
        if (HistoryItem.hasOwnProperty("audiodata")) {
            blnDrawBuffer = true;
            wavesurfer.backend.buffer = wavesurfer.backend.ac.createBuffer(
                HistoryItem.audiodata.length,
                HistoryItem.audiodata[0].length,
                wavesurfer.backend.buffer.sampleRate
            );
            for (var channel = 0; channel < wavesurfer.backend.buffer.numberOfChannels; channel++) {
                wavesurfer.backend.buffer.copyToChannel(HistoryItem.audiodata[channel], channel, 0);
            }
        }
        setAudioSignalHasChanges(HistoryItem.audioSignalHasChanges)
        if (blnDrawBuffer) {
            wavesurfer.drawBuffer();
            setRefreshSpetrogram(true);
            await fitWindowSize();
        }
        else
            wavesurfer.markers._updateMarkerPositions();
    }
}

async function udpateHistoryMenu() {
    var btnUndo = document.getElementById("undo")
    var btnRedo = document.getElementById("redo")
    var undoText = ""
    var redoText = ""
    var transaction = null;
    var Histories = null;
    if (IndexedDB) {
        transaction = IndexedDB.transaction("History", "readwrite");
        Histories = transaction.objectStore("History");
    }
    var itemsCount = IndexedDB ? await Histories.count() : History.Items.length;
    if (itemsCount > 0 && History.Idx >= 0 && History.Idx < itemsCount) {
        if (IndexedDB)
            undoText = ": <span style='color:#8F9092'>" + (await Histories.getAll(IDBKeyRange.only(History.Idx)))[0].description + "</span>";
        else
            undoText = ": <span style='color:#8F9092'>" + History.Items[History.Idx].description + "</span>";
        btnUndo.style.pointerEvents = "";
        btnUndo.style.color = "black";
    }
    else {
        btnUndo.style.pointerEvents = "none";
        btnUndo.style.color = gray1;
    }
    if (History.Idx >= -1 && itemsCount > 0 && History.Idx < itemsCount - 1) {
        btnRedo.style.pointerEvents = "";
        btnRedo.style.color = "black";
        if (IndexedDB) {
            redoText = ": <span style='color:#8F9092;'>" + (await Histories.getAll(IDBKeyRange.only(History.Idx + 1)))[0].description + "</span>";
        }
        else
            redoText = ": <span style='color:#8F9092;'>" + History.Items[History.Idx + 1].description + "</span>";
    }
    else {
        btnRedo.style.pointerEvents = "none";
        btnRedo.style.color = gray1;
    }
    btnUndo.innerHTML = "<span style='float: left; overflow: hidden; width: 130px'>Undo" + undoText + "</span><span style='float:right'>CTRL+Z</span>";
    btnRedo.innerHTML = "<span style='float: left; overflow: hidden; width: 130px'>Redo" + redoText + "</span><span style='float:right'>CTRL+Y</span>";
}
