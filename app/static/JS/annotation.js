import { wavesurfer, gray1, showListAnnotations, setLastClickTime, CtrlPressed, setAudioSignalHasChanges, DataSet, uuidEmit, UserSettings, setCurrentTime } from "./script.js"
import { showAnnotationList, closeDropdowns, updateShortLink } from "./dropdown.js"
import { toggleMenuOffHandler } from "./contextmenu.js"
import { addToHistory, HistoryType } from "./history.js";
import { Treeselect } from './treeselectjs/treeselectjs.mjs.js'

export var selectedRegion = null;
export function setSelectedRegion(value) { selectedRegion = value; }
var regionsParams = null;
export var  treeselect = null;
var treeselectOptions = null;
var lastValue = "";

export function setDeletedRegions(value) { DataSet.deletedRegions = value; }

function findNestedObj(entireObj, keyToFind, valToFind) {
    let foundObj;
    JSON.stringify(entireObj, (_, nestedValue) => {
      if (nestedValue && nestedValue[keyToFind] === valToFind) {
        foundObj = nestedValue;
      }
      return nestedValue;
    });
    return foundObj;
  };

const loadTagList = new XMLHttpRequest();
loadTagList.responseType = 'json';
loadTagList.open('POST', '/hierarchicalTagList');
loadTagList.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
loadTagList.onreadystatechange = async () => {
    if (!DataSet.tagList)
        DataSet.tagList = [];
    if (loadTagList.readyState == XMLHttpRequest.DONE && loadTagList.response) {
        const slot = document.createElement('div')
        slot.innerHTML = '<a class="treeselect-demo__slot" href="">Click!</a>'
        const domElement = document.getElementById("annotation");
        treeselectOptions = await loadTagList.response;
        
        treeselect = new Treeselect({
          parentHtmlContainer: domElement,
          value: null,
          options: treeselectOptions,
          listSlotHtmlComponent: slot,
          isSingleSelect: true,
          alwaysOpen: false,
          openLevel: Infinity,
          isGroupedValue: true,
          placeholder: "Select Annotation...",
          showTags: false,
          //nameChangeCallback: () => {console.log("TEST")},
        })
    
        treeselect.srcElement.addEventListener('input', async (e) => {
            var annotation = findNestedObj(treeselect.options, "value", e.detail)
            if (annotation) {
                if (!findNestedObj(treeselectOptions, "name", annotation.name))
                    treeselectOptions.unshift({id: null, name: annotation.name, children: null, deleted: null, parent_id: null, value: annotation.name})  
                treeselect.destroy();
                document.getElementById("annotation").style.visibility = "hidden";
                await addMarkerToRegion(selectedRegion, annotation)
                selectRegion(selectedRegion);
                await enableAnnotation(false);
            }
        });
    }
}

function treeselectKeyDown(e) {
    if (e.key === 'Escape' || e.key === 'Tab') {
        e.stopPropagation();
        document.getElementById("annotation").style.visibility = "hidden";
        treeselect.toggleOpenClose();
    }
}

function treeSearch(searchTree, value) {
    var returnTree = []
    for (var item of searchTree) {
        var children = null
        if (item.hasOwnProperty("children") && item.children)
            children = treeSearch(item.children, value);
        if (item.name.toLowerCase().includes(value.toLowerCase()) || (children && children.length)) {
            if (children && children.length)
                item.children = children;
            returnTree.push(item);
        }
    }
    return returnTree;
}

function treeselectKeyUp(e) {
    function existInOptionsTree(value, options = treeselectOptions) {
        var exist = false;
        for (var option of options) {
            if (option.hasOwnProperty("name") && value && option.name.toLowerCase() == value.toLowerCase())
                exist = true;
            if (option.hasOwnProperty("children") && option.children != null && option.children.length > 0)
                exist = exist || existInOptionsTree(value, option.children);
        }
        return exist;
    }            
    var value = document.getElementsByClassName("treeselect-input__edit")[0].value.trim();
    if (!existInOptionsTree(value) && value != lastValue) {
        if (value != "") {
            var options = JSON.parse(JSON.stringify(treeselectOptions))
            options.unshift({id: null, name: value, children: null, deleted: null, parent_id: null, value: ""})  
            treeselect.options = treeSearch(options, value);
        }
        else
            treeselect.options = treeselectOptions;
        treeselect.updateValue(value);
        treeselect.mount();
        treeselect.toggleOpenClose();
        treeselect.focus();
        document.getElementsByClassName("treeselect-input__edit")[0].addEventListener("keydown", treeselectKeyDown);
        document.getElementsByClassName("treeselect-input__edit")[0].addEventListener("keyup", treeselectKeyUp);
        document.getElementsByClassName("treeselect-input__edit")[0].value = value;
        lastValue = value;
    }
}

export async function loadAnnotations(data = {}) {
    const annotation = fetch('/getAnnotations', {
        method: "POST",
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(data),
        credentials: 'same-origin'
    })
        .then((response) => response.json())
        .then((annotation) => { return annotation; })
    const annotationLoaded = async () => {
        const items = JSON.parse(await annotation);
        if (items && items.length) {
            for (var idx = 0; idx < items.length; idx++)
                await addImportedAnnotation(items[idx]);
            loadAnnotations({id: items[items.length - 1].id });
        }
    };
    annotationLoaded();
}

export function initAnnotation() {
    loadTagList.send();

    wavesurfer.on('region-updated', function (region, e) {
        toggleMenuOffHandler();
        closeDropdowns();
        updateShortLink(region);
        if (region) {
            document.getElementById("CurrentTime").style.color = "#e30613";
            document.getElementById("CurrentTime").innerHTML = new Date((region.end - region.start) * 1000).toISOString().substr(11, 12);
        }
    });

    wavesurfer.on('region-click', function (region, e) {
        toggleMenuOffHandler();
        closeDropdowns();
        if (e.shiftKey) {
            if (e.target.tagName == "REGION" && region.shortLink && document.getElementById("_ListAnnotations") != null)
                region.shortLink.scrollIntoView()
            e.stopPropagation();
            wavesurfer.stop();
            Object.keys(wavesurfer.regions.list).forEach(element => {
                wavesurfer.regions.list[element].setLoop(false);
            });
            wavesurfer.seekAndCenter(region.start / wavesurfer.backend.buffer.duration);
            selectRegion(region);
            region.playLoop();
        }
    });

    wavesurfer.on('region-created', async function (region, e) {
        if (CtrlPressed == false && enableAnnotationWithoutCTRL == false)
            await enableAnnotation(false);
        else {
            region.color = wavesurfer.regions.params.color;
            region.channelIdx = -1;
            region.markerColor = region.color;
            if (region.regionHeight != "100%") {
                region.regionHeight = "100%"
                region.marginTop = '0px';
                region.style(region.element, {
                    height: region.regionHeight,
                    top: region.marginTop
                });
                region.element.style["z-index"] = 1;
                region.element.children[0].style.backgroundColor = gray1;
                region.element.children[1].style.backgroundColor = gray1;
            }
            selectedRegion = null;
        }
    });

    wavesurfer.on('region-dblclick', function (region, e) {
        if (UserSettings["allowAnnotationTags"]) {
            selectedRegion = region;
            if (e.target.tagName == "REGION" && region.shortLink && document.getElementById("_ListAnnotations") != null)
                region.shortLink.scrollIntoView();
            wavesurfer.seekAndCenter(region.start / wavesurfer.backend.buffer.duration);
            selectRegion(region);
        }
    });

    wavesurfer.on('region-update-end', async function (region) {
        toggleMenuOffHandler();
        if (region.marker) {
            await addToHistory(HistoryType.annotation, "Annotation moved");
            region.hasChanges = true;
            region.marker.time = region.start;
            region.marker.endTime = region.end;
            wavesurfer.markers._updateMarkerPositions();
        }
        setAudioSignalHasChanges(DataSet.audioSignalHasChanges);
        selectRegion(region, true);
        setCurrentTime(wavesurfer.getCurrentTime());
    });
    wavesurfer.minimap.drawRegions();
}

export function selectRegion(region, updateList = false) {
    if (region) {
        selectedRegion = region;
        if (selectedRegion.markerColor) {
            var colors = selectedRegion.markerColor.split(',');
            selectedRegion.color = selectedRegion.color.replace(colors[colors.length - 1], ".75)");
        }
        selectedRegion.resize = true;
        selectedRegion.drag = true;
        unselectRegions();
        selectedRegion._onRedraw();
        if (document.getElementById("_ListAnnotations")) {
            if (updateList)
                showAnnotationList();
            else
                updateShortLink(region);
        }
        if (showListAnnotations && region.shortLink)
            region.shortLink.scrollIntoView();
        selectedRegion.element.classList.add("task");
    }
}

export async function unselectRegions() {
    Object.keys(wavesurfer.regions.list).forEach(async element => {
        if (wavesurfer.regions.list[element] != selectedRegion) {
            var region = wavesurfer.regions.list[element];
            region.element.classList.remove("task");
            region.color = region.markerColor;
            if (region.marker) {
                region.resize = false;
                region.drag = false;
                region._onRedraw();
                updateShortLink(region);
            }
            else {
                await deleteAnnotation(region);
            }
        }
    });
    wavesurfer.minimap.renderRegions();
}

var enableAnnotationWithoutCTRL = true;
export async function enableAnnotation(enable) {
    enable = enable || enableAnnotationWithoutCTRL;
    if (enableAnnotationWithoutCTRL)
        regionsParams = wavesurfer.regions.params;
    if (enable && regionsParams) {
        wavesurfer.regions.enableDragSelection(regionsParams);
    }
    else {
        regionsParams = wavesurfer.regions.params;
        wavesurfer.regions.disableDragSelection();
    }
}

async function addMarkerToRegion(region, annotation, addHistory = true) {
    if (region) {
        var color = "rgba(255, 255, 255, 0.2)";
        if (annotation.id) {
            if (annotation.color.startsWith("rgb"))
                annotation.color = annotation.color.substr(annotation.color.indexOf("(") + 1, annotation.color.indexOf(")") - annotation.color.indexOf("(") - 1);
            color = "rgba(" + annotation.color + ", 0.2)";
        }
        region.markerColor = color;
    
        if (!annotation.id) 
            annotation.id = uuidEmit();
        if (annotation.id) {
            var marker = region.marker;
            if (!marker) {
                if (addHistory)
                    await addToHistory(HistoryType.annotation, "Annotation added");
                marker = wavesurfer.markers.add({
                    time: region.start,
                    label: annotation.name,
                    position: 'bottom',
                })
                marker.endTime = region.end;
                var markerLabelDiv = document.getElementsByTagName('marker')[document.getElementsByTagName('marker').length - 1].getElementsByClassName("marker-label")[0];
                region.markerLabel = markerLabelDiv.getElementsByTagName("span")[0];
                region.markerPin = markerLabelDiv.getElementsByTagName("svg")[0];
            }
            if (addHistory && region.markerLabel.textContent != annotation.name)
                await addToHistory(HistoryType.annotation, "Annotation Value changed");
            region.markerLabel.textContent = annotation.name;
            for (var idx = wavesurfer.markers.markers.length - 1; idx >= 0; idx--) {
                if (wavesurfer.markers.markers[idx] == marker) {
                    wavesurfer.markers.markers[idx].label = annotation.name;
                }
            }
            var colors = region.markerColor.split(',');
            region.resize = false;
            region.drag = false;
            region.color = color;
            region.markerColor = region.color;
            region.tag_id = annotation.id;
            if (annotation.hasOwnProperty("tag_id"))
                region.tag_id = annotation.tag_id;
            region.marker = marker;
            region.hasChanges = true;
            region.markerLabel.style.color = region.color.replace(colors[colors.length - 1], "0.9)")
            region.markerLabel.style.zIndex = 99;
            region.markerPin.getElementsByTagName("polygon")[0].style.fill = region.color.replace(colors[colors.length - 1], "1.0)");
            //region.markerPin.setAttribute("fill", region.color.replace(colors[colors.length - 1], "1.0)"));
            if (document.getElementById("_ListAnnotations"))
                showAnnotationList();
            setAudioSignalHasChanges(DataSet.audioSignalHasChanges);
            return region;
        }        
    }
}

export async function addImportedAnnotation(item) {
    var start = item.start.split(":");
    start = start[0] * 3600 + start[1] * 60 + (+start[2]);
    var end = item.end.split(":");
    end = end[0] * 3600 + end[1] * 60 + (+end[2]);
    var region = wavesurfer.regions.add({ start: start, end: end })
    region.resize = false;
    region.annotation_id = item.id;
    region.drag = false;
    region.element.style["z-index"] = 1;
    region.element.children[0].style.backgroundColor = gray1;
    region.element.children[1].style.backgroundColor = gray1;
    if (item.manual && !treeselectOptions.find(({ name }) => name === item.name))
        treeselectOptions.unshift({id: null, name: item.name, children: null, deleted: null, parent_id: null, value: item.id})  
    if (item.name)
        await addMarkerToRegion(region, item, false);
    region.hasChanges = false;
    region._onRedraw();
    setAudioSignalHasChanges(DataSet.audioSignalHasChanges);
    if (showListAnnotations)
        showAnnotationList();
    return region;
}

export function addAnnotation(region) {
    if (UserSettings["allowAnnotationTags"]) {
        if (region) {
            document.getElementById("annotation").style.right = "0px";
            document.getElementById("annotation").style.bottom = "0px";
            document.getElementById("annotation").style.visibility = "visible";
            treeselect.options = treeselectOptions;
            if (!region.tag_id)
                treeselect.value = [];
            else
                treeselect.value = region.tag_id;
            treeselect.mount();
            document.getElementsByClassName("treeselect-input__edit")[0].addEventListener("keydown", treeselectKeyDown);
            document.getElementsByClassName("treeselect-input__edit")[0].addEventListener("keyup", treeselectKeyUp);
            treeselect.toggleOpenClose();
            treeselect.focus();
        }
        if (showListAnnotations)
            showAnnotationList();
    }
}

export async function deleteAnnotation(region, addĤistory = true) {
    if (region && UserSettings["allowAnnotationTags"]) {
        if (region.marker) {
            if (addĤistory)
                await addToHistory(HistoryType.annotation, "Annotation deleted");
            for (var idx = wavesurfer.markers.markers.length - 1; idx >= 0; idx--) {
                if (wavesurfer.markers.markers[idx] == region.marker)
                    wavesurfer.markers.remove(idx);
            }
            wavesurfer.markers.remove(region.marker);
        }
        if (region.annotation_id) {
            DataSet.deletedRegions.push(region.annotation_id);
        }
        setAudioSignalHasChanges(DataSet.audioSignalHasChanges);
        setLastClickTime(null);
        region.remove();
    }
    if (showListAnnotations)
        showAnnotationList();
}

