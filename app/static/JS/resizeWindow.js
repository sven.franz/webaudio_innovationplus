// Source: https://jsfiddle.net/yckart/9L5b1bqf/
var handlesSize = 8;
var currentHandle = false;
var drag = false;
var currentWindow = false;

export function initResize() {
    window.addEventListener('mousedown', mouseDown, false);
    window.addEventListener('mouseup', mouseUp, false);
    window.addEventListener('mousemove', mouseMove, false);
}

function point(x, y) {
    return {
        x: x,
        y: y
    };
}

function dist(p1, p2) {
    return Math.sqrt((p2.x - p1.x) * (p2.x - p1.x) + (p2.y - p1.y) * (p2.y - p1.y));
}

function getHandle(mouse, canvas) {
    if (dist(mouse, point(canvas.offsetLeft, canvas.offsetTop)) <= handlesSize) return 'topleft';
    if (dist(mouse, point(canvas.offsetLeft + canvas.offsetWidth, canvas.offsetTop)) <= handlesSize) return 'topright';
    if (dist(mouse, point(canvas.offsetLeft, canvas.offsetTop + canvas.offsetHeight)) <= handlesSize) return 'bottomleft';
    if (dist(mouse, point(canvas.offsetLeft + canvas.offsetWidth, canvas.offsetTop + canvas.offsetHeight)) <= handlesSize) return 'bottomright';
    if (dist(mouse, point(canvas.offsetLeft + canvas.offsetWidth / 2, canvas.offsetTop)) <= handlesSize) return 'top';
    if (dist(mouse, point(canvas.offsetLeft, canvas.offsetTop + canvas.offsetHeight / 2)) <= handlesSize) return 'left';
    if (dist(mouse, point(canvas.offsetLeft + canvas.offsetWidth / 2, canvas.offsetTop + canvas.offsetHeight)) <= handlesSize) return 'bottom';
    if (dist(mouse, point(canvas.offsetLeft + canvas.offsetWidth, canvas.offsetTop + canvas.offsetHeight / 2)) <= handlesSize) return 'right';
    if (dist(mouse, point(canvas.offsetLeft + canvas.offsetWidth / 2, canvas.offsetTop + canvas.offsetHeight / 2)) <= handlesSize) return 'center';
    return false;
}

function mouseDown(e) {
    if (currentHandle && currentWindow) drag = true;
}

function mouseUp() {
    drag = false;
    currentHandle = false;
    if (currentWindow) {
        currentWindow.style.cursor = "";
        window.dispatchEvent(new Event('resize'));
    }
    currentWindow = false;
}

function mouseMove(e) {
    if (!drag) {
        var subwindows = false;
        ["subwindow", "pluginwindow"].forEach(element => {
            subwindows = document.getElementsByClassName(element);
            for (var idx = 0; idx < subwindows.length; idx++) {
                if ((!subwindows[idx].hasOwnProperty("dock") || !subwindows[idx].dock == null) && subwindows[idx].style.visibility != "hidden" && (!document.getElementById(subwindows[idx].id + "_disable") || document.getElementById(subwindows[idx].id + "_disable").style.visibility == "hidden")) {
                    currentHandle = getHandle(point(e.pageX, e.pageY), subwindows[idx]);
                    if (currentHandle) {
                        currentWindow = document.getElementById(subwindows[idx].id);
                        break;
                    }
                }
            }
        });
    }
    if (currentWindow) {
        switch (currentHandle) {
            case 'topleft':
                currentWindow.style.cursor = "nw-resize";
                break;
            case 'topright':
                currentWindow.style.cursor = "ne-resize";
                break;
            case 'bottomleft':
                currentWindow.style.cursor = "sw-resize";
                break;
            case 'bottomright':
                currentWindow.style.cursor = "se-resize";
                break;
            case 'top':
                currentWindow.style.cursor = "n-resize";
                break;
            case 'left':
                currentWindow.style.cursor = "w-resize";
                break;
            case 'bottom':
                currentWindow.style.cursor = "s-resize";
                break;
            case 'right':
                currentWindow.style.cursor = "e-resize";
                break;
            case 'center':
                currentWindow.style.cursor = "move";
                break;
            default:
                currentWindow.style.cursor = "";
        }
    }
    if (currentHandle && currentWindow && drag) {
        var mousePos = point(e.pageX, e.pageY);
        switch (currentHandle) {
            case 'topleft':
                currentWindow.style.width = currentWindow.offsetWidth + (currentWindow.offsetLeft - mousePos.x) + "px";
                currentWindow.style.height = currentWindow.offsetHeight + (currentWindow.offsetTop - mousePos.y) + "px";
                currentWindow.style.left = mousePos.x + "px";
                currentWindow.style.top = mousePos.y + "px";
                break;
            case 'topright':
                currentWindow.style.width = (mousePos.x - currentWindow.offsetLeft) + "px";
                currentWindow.style.height = currentWindow.offsetHeight + (currentWindow.offsetTop - mousePos.y) + "px";
                currentWindow.style.top = mousePos.y + "px";
                break;
            case 'bottomleft':
                currentWindow.style.width = currentWindow.offsetWidth + (currentWindow.offsetLeft - mousePos.x) + "px";
                currentWindow.style.left = mousePos.x + "px";
                currentWindow.style.height = (mousePos.y - currentWindow.offsetTop) + "px";
                break;
            case 'bottomright':
                currentWindow.style.width = (mousePos.x - currentWindow.offsetLeft) + "px";
                currentWindow.style.height = (mousePos.y - currentWindow.offsetTop) + "px";
                break;
            case 'top':
                currentWindow.style.height = (currentWindow.offsetTop - mousePos.y) + "px";
                currentWindow.style.top = mousePos.y + "px";
                break;
            case 'left':
                currentWindow.style.width = (currentWindow.offsetLeft - mousePos.x) + "px";
                currentWindow.style.left = mousePos.x + "px";
                break;
            case 'bottom':
                currentWindow.style.height = (mousePos.y - currentWindow.offsetTop) + "px";
                break;
            case 'right':
                currentWindow.style.width = (mousePos.x - currentWindow.offsetLeft) + "px";
                break;
            case 'center':
                currentWindow.style.left = (mousePos.x - currentWindow.offsetWidth / 2) + "px";
                currentWindow.style.top = (mousePos.y - currentWindow.offsetHeight / 2) + "px";
                break;
        }
    }
}
