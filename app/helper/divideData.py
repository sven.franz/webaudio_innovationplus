import os
import soundfile as sf
import numpy as np
import scipy.signal as sp
import io
import argparse

search_interval_start = 10
search_interval_stop = 15
min_len = 20  # all files below 20 minutes are not changed

def findCutPoints(data, fs, nr_of_new_parts, len_first_guess_min, one_block_ms=250, search_interval_min=1):
    ''' Usage: findCutPoints(data, fs, nr_of_new_parts, len_first_guess_min, one_block_ms = 250, search_interval_min = 1):
    This functions searches at counter*len_first_guess for a good cutting point (good means that the possibility of an
    important event is small) by minimizing the power in the search_interval and finally look for a smooth zero 
    crossing ==> no clicks at the cutting point.
    Input Parameters:
        data: a data vector (single channel)
        fs:   sampling rate
        nr_of_new_parts: the number of the new parts
        len_first_guess_min: the length of the new parts in minutes (nr_of_new_parts*nr_of_new_parts must be smaller than len(data))
        one_block_ms: block length to compute the power (in ms, default is 250ms)
        search_interval_min: the search area around len_first_guess_min +- search_interval_min 
    Output: List of cutpoints in samples including zero and len(data)
    '''

    one_block_samples = int(one_block_ms*0.001*fs)

    #search_interval_min = 1
    nr_of_search_blocks = int(np.round(search_interval_min*60*fs/one_block_samples))
    search_interval_samples = int(one_block_samples*nr_of_search_blocks)

    part_counter = 0
    cut_point_samples = np.zeros(nr_of_new_parts+1, dtype='int64')
    cut_point_samples[-1] = data.shape[0]
    for part_counter in range(1, nr_of_new_parts):
        # print(part_counter)
        start_val_min = len_first_guess_min * part_counter
        start_val_samples = int(start_val_min*60*fs)

        search_blocks = data[start_val_samples - search_interval_samples : start_val_samples + search_interval_samples, 0].reshape((nr_of_search_blocks*2, one_block_samples))

        power = np.mean(np.abs(search_blocks), 1)
        idx = np.argmin(power)
        cut_block = search_blocks[idx, :].A1

        # look out for the softest zero crossing
        sign_sig = cut_block[int(len(cut_block)/2):] * cut_block[int(len(cut_block)/2)-1:-1]
        sign_sig[sign_sig > 0] = -1.0
        zerocrossingindex = int(len(cut_block)/2) + np.argmax(sign_sig)

        cut_point_samples[part_counter] = (start_val_samples-search_interval_samples + idx*one_block_samples + zerocrossingindex)
    return cut_point_samples

def divide(*args):
    if type(args[0]) is dict:
        args = [argparse.Namespace(**args[0])]
    if (type(args) is tuple or type(args) is list) and type(args[0]) is argparse.Namespace:
        if hasattr(args[0], "data"):
            file = args[0].data
        elif hasattr(args[0], "filename"):
            file = args[0].filename
        fileInfo = sf.SoundFile(file)
        if hasattr(args[0], "data"):
            file.seek(0)
    blocks = []
    delay_values = None
    lastData = np.empty([0, fileInfo.channels])

    len_minutes = fileInfo.frames / fileInfo.samplerate / 60
    nr_of_new_parts = 1
    if len_minutes >= min_len:
        nr_of_new_parts = round(0.5*(len_minutes/search_interval_start+len_minutes/search_interval_stop))
    len_first_guess = round((fileInfo.frames / fileInfo.samplerate / 60) / nr_of_new_parts*60) / 60
    
    for data in sf.blocks(file, blocksize = fileInfo.samplerate * min_len * 60, overlap = 0):
        data = np.asmatrix(data)
        if data.shape[1] > data.shape[0]:
            data = np.transpose(data)
        data = np.concatenate([lastData, data])
        # compute the number of new parts and the approximate length
        len_minutes = data.shape[0] / fileInfo.samplerate / 60
        nr_of_new_parts = 1
        if len_minutes >= min_len:
            nr_of_new_parts = round(0.5*(len_minutes/search_interval_start+len_minutes/search_interval_stop))

        # lets start with a highpass (4th order butterworth)
        if type(args) is tuple and args[0].NoHighpass:
            cut_freq = 80
            sos = sp.butter(4, 2 * cut_freq / fileInfo.samplerate, 'highpass', output='sos')
            data, delay_values = sp.sosfilt(sos, data, zi = delay_values)

        #print(f'The input file will be splitted in {nr_of_new_parts} parts with an approximate length of {len_first_guess} minutes')
        # find minimum at the cut points (given by first guess) and aftrewards use a zero crossing for the final cut position
        cutpointslist = findCutPoints(data, fileInfo.samplerate, nr_of_new_parts, len_first_guess)
        for parts in range(nr_of_new_parts - 1):
            blocks.append(data[cutpointslist[parts]:cutpointslist[parts+1], :])
        lastData = data[cutpointslist[-2] : , :]
    if lastData.shape[0]:
        blocks.append(lastData)
    return blocks, fileInfo.samplerate

def divideFileAndStore(filename, parts, fs, outPath = None):
    path, filename = os.path.split(filename)
    filename_base, filename_extension = os.path.splitext(filename)
    if outPath == None:
        outPath = os.path.join(path, 'cuts')
    outPath = os.path.normpath(outPath)
    os.makedirs(outPath, exist_ok=True)
    for idx in range(len(parts)):
        outfilename = os.path.join(outPath, filename_base + '_' + str(idx) + filename_extension)
        sf.write(outfilename, parts[idx], fs)

if __name__ == "__main__":
    # parse the input parameter
    parser = argparse.ArgumentParser(description='Audiosignal cutting tool')
    parser.add_argument('filename', type=str, help='the filename of the long signal (only wav is supported at the moment)')
    parser.add_argument('--NoHighpass', action='store_false', help='Set if no highpass filter(default fc = 80 Hz)) is neded (default = False)')
    parser.add_argument('--outPath', type=str, help='''Set the output path, default is a new subdirectory 'cuts' ''')
    args = parser.parse_args()
    parts, fs = divide(args)
    divideFileAndStore(args.filename, parts, fs, args.outPath)
