import sys
import os
import json
SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))
import numpy as np
import numpy.fft as nf
import importlib
import soundfile as sf
from app.routes import executeMethodInSanbox, processAudiodata, app, session, default
from selenium import webdriver
from selenium.webdriver.common.by import By
from http.server import BaseHTTPRequestHandler, HTTPServer
import threading

PluginList = {}

class MyServer(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        if (self.path == "/"):
            file = "index.html"
            self.send_header("Content-type", "text/html")
        elif (self.path == '/tmp/Plugin.js'):
            file = "tmp/Plugin.js"
            self.send_header("Content-type", "text/javascript")
        else:
            file = (".." + self.path).replace("/", os.sep)
            if file.endswith(".js"):
                self.send_header("Content-type", "text/javascript")
            elif file.endswith(".css"):
                self.send_header("Content-type", "text/css")
            else:
                if not file.endswith("favicon.ico"):
                    print("\033[91mError: Error serving file'" + file + "'!\033[0m")
                return
        if (os.path.isfile(os.path.join(SCRIPT_DIR, file))):
            self.end_headers()
            with open(os.path.join(SCRIPT_DIR, file)) as f:
                fileData = ''.join(f.readlines())
            self.wfile.write(bytes(fileData, "utf-8"))
        else:
            print("\033[91mError: Error serving file'" + file + "'!\033[0m")

class PluginTester:

    def logger(self, value, nl = True):
        if nl:
            nl = "<br>"
        else:
            nl = ""
        driver.execute_script("var div = document.getElementById('logger'); div.innerHTML += '&nbsp;" + value + nl + "'; div.scrollTop = div.scrollHeight;")

    def __init__(self, audiofile, pluginname) -> None:
        pluginfile = os.path.join(SCRIPT_DIR, "..", "Plugins", pluginname + ".py")
        if not audiofile.startswith(os.sep):
            audiofile = os.path.join(SCRIPT_DIR, audiofile)
        if not os.path.isfile(audiofile):
            print("\033[91mError: Audiofile '" + audiofile + "' does not exist!\033[0m")
            return
        if not os.path.isfile(pluginfile):
            print("\033[91mError: Plugin '" + pluginname + ".py' does not exist in Folder '" + os.path.join("..", "Plugins") + "'!\033[0m")
            return
        self.pluginname = pluginname
        self.request_context = app.test_request_context('', data={})
        script = ""
        if os.path.isfile(pluginfile.replace(".py", ".js")):
            with open(pluginfile.replace(".py", ".js")) as f:
                script = ''.join(f.readlines())
        if not os.path.exists(os.path.join(SCRIPT_DIR, 'tmp')):
            os.mkdir(os.path.join(SCRIPT_DIR, 'tmp'))
        with open(os.path.join(SCRIPT_DIR, 'tmp/Plugin.js'), 'w') as fp:
            fp.write("%s\n" % ("class pluginJSObject_" + pluginname + "{\n" + script.replace("function", "") + "}; window.pluginJSObject_" + pluginname + " = pluginJSObject_" + pluginname + ";"))
        
        hostName = "localhost"
        serverPort = 8080
        webServer = HTTPServer((hostName, serverPort), MyServer)
        # webServer.socket = ssl.wrap_socket (webServer.socket, keyfile = os.path.join(SCRIPT_DIR, "../key.pem"), certfile = os.path.join(SCRIPT_DIR, '../cert.pem'), server_side=True)
        threading.Thread(target=webServer.serve_forever).start()
        driver.maximize_window()
        driver.get("http://" + hostName + ":" + str(serverPort))
        
        self.logger("Load Audiofile \\'" + audiofile + "\\'")
        self.audiofile = sf.SoundFile(audiofile)
        self.logger("Load Plugin \\'" + self.pluginname + "\\'")
        spec = importlib.util.spec_from_file_location(self.pluginname, pluginfile)
        module = importlib.util.module_from_spec(spec)        
        spec.loader.exec_module(module)
        sys.modules[self.pluginname] = module
        PluginList[self.pluginname] = {"user_ids" : [], "isTemp" : False}
        self.logger("Init Plugin")

        with self.request_context:
            data = {"id": self.pluginname, "sampleRate": self.audiofile.samplerate, "channels": self.audiofile.channels, "signalLength": self.audiofile.frames / self.audiofile.samplerate }
            self.plugin = executeMethodInSanbox(PluginList[self.pluginname], getattr(sys.modules[self.pluginname], self.pluginname), data)
            data = {"controls": None, "params": None, "script": None, "isServerSide": False}
            self.plugin.isTemp = False
            executeMethodInSanbox(self.plugin, self.plugin.updateParams)
            data["params"] = self.plugin.params
            data["controls"] = self.plugin.controls
            if hasattr(self.plugin, "process"):
                data["isServerSide"] = True
            session["Plugins"] = [{"object": self.plugin, "params": data["params"], "isTemp": False}]
            transferData, currentPlugins = processAudiodata({"Init": True})
            driver.execute_script("window.divWindows = new Array({id: '" + pluginname + "', pluginJSObject: new pluginJSObject_" + pluginname + "()});")
            driver.execute_script("window.DataSet = {Overlays: {}};")
            driver.execute_script("window.wavesurfer = {backend: {buffer: {numberOfChannels: " + str(self.audiofile.channels) + "}}};")

    def process(self):
        with self.request_context:
            audiodata = self.audiofile.read().tolist()
            idx = 0
            blocklen = 1024 * 50
            while idx < self.audiofile.frames:
                percentage = str(round(idx / self.audiofile.frames * 10000) / 100)
                if len(percentage.split(".")[1]) == 1:
                    percentage += "0"
                if len(percentage.split(".")[0]) == 1:
                    percentage = "0" + percentage
                self.logger("Processing " + percentage + " %...&nbsp;", False)
                currentBlock = np.array(audiodata[idx : min(self.audiofile.frames, idx + blocklen)]).transpose().tolist()
                data = {"data": currentBlock, "idx": 0, "PluginId": self.pluginname, "settings": self.plugin.params}
                self.logger("Server...", False)
                transferData, currentPlugins = processAudiodata(data)
                if transferData["data"]:
                    transferData["data"] = np.fromstring(transferData["data"], np.float32)
                    transferData["data"].shape = currentBlock.shape
                    currentBlock = transferData["data"]
                if transferData["settings"]:
                    self.plugin.params = transferData["settings"]
                if driver.execute_script("return divWindows[0].pluginJSObject.process ? true : false"):
                    self.logger("Client...", False)
                    driver.execute_script("window.PluginParamsStructured = {" + self.pluginname + ": {params: " + json.dumps(transferData["settings"], default= str) + "}}"); 
                    driver.execute_script("window.currentBlock = JSON.stringify(wola(divWindows, " + json.dumps(currentBlock) + ", { val: 0 }));")
                    tempBlock = driver.execute_script("return window.currentBlock")
                    self.plugin.params = driver.execute_script("return convertParams2Sendable(window.PluginParamsStructured." + self.pluginname + ".params)")

                    #self.plugin.params = driver.execute_script("return convertParams2Sendable(window.PluginParamsStructured.Spectrum.params)")
                    if len(tempBlock):
                        currentBlock = tempBlock
                self.logger("", True)
                audiodata[idx : min(self.audiofile.frames, idx + blocklen)] = np.array(data).transpose().tolist()
                idx += blocklen
            self.logger("Processing done")
            self.logger("Post-Processing", False)
            self.logger("Server...", False)
            transferData, currentPlugins = processAudiodata({"postProcess": True, "PluginId": self.pluginname, "settings": self.plugin.params})
            if driver.execute_script("return divWindows[0].pluginJSObject.postprocess ? true : false"):
                self.logger("Client...", False)
                driver.execute_script("divWindows[0].pluginJSObject.postprocess(" + json.dumps(transferData["data"][self.pluginname], default=default) + ", " + json.dumps(transferData["settings"][self.pluginname]["params"], default=default) + ", document.getElementById('" + self.pluginname + "_content'))"); 
            self.logger("", True)
            self.logger("Done!")



if __name__ == "__main__":
    """
    from selenium.webdriver.chrome.options import Options
    opts = Options()
    chromium_path = "/usr/bin/chromium-browser"
    opts.binary_location = chromium_path
    driver = webdriver.Chrome(options=opts)
    """
    driver = webdriver.Firefox()
    
    filename = "/home/sven/devel/InnovationPlus/webAudio/AudioFiles/tmp/audiomass-output.wav"
    #filename = os.path.join("..", "AudioFiles", "NordwindNah.wav")
    pluginname = "Spectrum"
    #pluginname = "addNoise_Server"
    pluginTester = PluginTester(filename, pluginname)
    
    pluginTester.process()