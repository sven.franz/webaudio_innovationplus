# Requirements
- The plugin file must be stored in the "Plugins" folder, containing the python plugin class.
- Filename must be same as Plugin Class Name + `.py`
- Plugin class must inherit from AudioPlugin. 

Minimal and necessary plugin structure example `Plugin.py`:

~~~~python
from app.AudioPlugin import AudioPlugin

class Plugin(AudioPlugin):
    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
~~~~

# Plugin Settings
Following Class variables controls the behaviour of the plugins:

- `name` (String, default: None)
- `isModal` (Boolean, default: False): GUI behaviour. If true the plugin popup window will be modal, the main window and other popups will be disabled but visible.
- `type` (PluginType-Enum, default: PluginType.loopthrough):
    - `PluginType.loopthrough`: Audio data will be passed to the process method of the plugin. Changes will be discarded! This type is usefull for analysis plugins.
    - `PluginType.persistent`: Changes of audio data in the process method will directly change the data of the audio file.
    - `PluginType.insert`: Audio data produced in the process method of this plugin will be inserted into the audio file at cursor position or replace audio data of an marked block.
- `domain` (Domain-Enum, default: Domain.Time): Possible Values: `Domain.Time` and `Domain.Frequency`
- `blockLen` (Integer, default: 1024)
- `fftSize` (Integer, default: 1024)
- `blockTime` (float, read only): gives you the start time of the current processed block.
- `timeDomainWithoutOverlap` (Boolean, default: False): in general audio will be processed with 50% overlap and hann-window, when your plugin is in time domain you can force no overlap and rect window by setting `timeDomainWithoutOverlap` to `true`.

# Plugins Parameters
Plugins Parameters have to be defiend and initialized in constructor `__init__` of the python plugin file by filling the `self.params` list, e.g. 
~~~~python
self.params["active"] = True
self.params["level"] = -10
~~~~
These parameters can be read and changed by the plugin code in each executed plugin method on client side as well as on server side. Additionally, it is possible to change the Parameters before or while executing the Plugin-Algorithm interactive with GUI control elements.

**Important:** The type of the variable is only limited on serializeable types (int, str, bool, etc.). Unserializeable objects (e.g. Numpy arrays) must be converted into serializeable data and deserialization must be performend in your plugin code. For Numpy arrays you can use methods like `.tobytes()` or `.tolist()` for serialization and `.fromstring()` or `.asarray()` for deserialization.

## Edit Callback
`updateParams`

## GUI Controled Parameters 
The plugin parameters could interactive modified on client side by GUI elements in the plugin popup window. Rendering these GUI elements is realized by using the [guify](https://github.com/colejd/guify) JS library. This library gives a simple way to build a GUI defined by JSON-Code and is therefore easy to use in combination with python.

Interactive GUI elements have to be defiend by the `self.controls` property in the constructor `__init__` of your Python plugin. All element types of [guify](https://github.com/colejd/guify) are possible and will automatically interact with your plugins params if the property of the control is set correctly. This property has to be identicall to an plugin params value key.

Two GUI elements, here checkbox and range, can be created as follows:
~~~~python
self.controls = [
    {"type": "checkbox", "label": "Active", "property": "active"},
    {"type": "range", "label": "Level", "min": -100, "max": 0, "step": 1, "property": "level"},
]
~~~~
with the displayed label text, the property for initeraction with the plugin params and additional parameters. Tis example will create the depicted controls:

<a href="docs/GUI_elements.png" target="_blank" style="padding:10px; background-color: white"><img src="docs/GUI_elements.png" width="300px"/></a>

Further information for the GUI elements can be found in the documentation of [guify](https://github.com/colejd/guify/blob/main/docs/api.md). 

# Processing Lifecicle
The processing lifecicle of any plugin (independently even from plugin type) is seperated in three sections. In each section all methods are optional on server side as well as on client side. Following a minimal example plugin with all impelented methods.

`Plugin.py`:
~~~~python
class Plugin(AudioPlugin):
    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
    def preprocess(self):
        pass
    def process(self, data):
        return data
    def postprocess(self):
        pass
~~~~

While a python file is absolutely necessary for the plugin. But for a client side browser internal data processing an optional javascript code can be developed. The javascript file has to fullfill following rules:
- An additional .js file in "Plugins" folder is necessarry
- Filename must be same as `Plugin_File_Name` + `.js`
- This JavaScript file can contain optional methods which will be called automatically by audio process

`Plugin.js`:
~~~~JavaScript
function preprocess(params, window) {}
function process(data, params, window) {return data; }
function postprocess(data, params, window) {}
~~~~

## Preprocess
- Serverside: `def preprocess(self)`
- Clientside: `function preprocess(params, window) {}`

## Process
- Serverside: `def process(self)`
- Clientside: `function process(data, params, window)`
<a href="docs/processing.png" target="_blank" style="float: right; padding:10px; background-color: white"><img src="docs/processing.png" width="300px"/></a>

## Postprocess
- Serverside: `def postprocess(self)`
- Clientside: `function postprocess(data, params, window)`


# PluginHelper Functionality and Functions

## Complex Spectral Values clientside
While spetral values are repesented as [NumPy](https://numpy.org/) complex value objects, there is no equivalent representation of a complex data type in javascript. Therefore, the JavaScript libraray [`Complex.js`](https://github.com/infusion/Complex.js/) is included to webAudio an can direct be used in plugins JavaScript functions. If your plugins domain is set to `Domain.Frequency` the passes data will be an array of complex data objects represented by this library conatining the spectrum of positive frequencies.  All in `Complex.js` implemented functions can be called within the plugin JavaScript code.

For documentation and examples see [https://github.com/infusion/Complex.js/](https://github.com/infusion/Complex.js/)

## PopUp windows
PopUp windows can easly created inside all optional JavaScript functions by calling `function popUp(title, modal = true, backgroundColor = "#ffffff")`. For updating window contant the handle to the PopUp can be stored in the passed window object. The handle will refer to the content HTML div container which can be modified using the [`JavaScript Document Object Model (DOM)`](https://wiki.selfhtml.org/wiki/JavaScript/DOM)  All further window interaction functionality will be performed will be available automatically.

Example to create a PopUp window within `postprocess` javascript function:
~~~~JavaScript
function postprocess(data, params, window) {
    window.popUp = popUp("WindowName");
}
~~~~

## `dygraphs` for plots
A fast and efficient way to plot data in a plugin content area is possible by using the included JavaScript libraray [`dygraphs`](https://dygraphs.com/), e.g. as follows:
~~~~JavaScript
window.graph = new Dygraph(
    destinationDomObject,
    data,
    {
        xlabel: 'Frequency / Hz',
        ylabel: 'Level / dB',
        legend: 'always',
        title: 'Spectrum',
        animatedZooms: true,
    });
~~~~
with the destinationDomObject to which the graph should be added (`var destinationDomObject = window` or `var destinationDomObject = window.popUp`) and data in `dygraphs` compatible data format. 


For documentation and examples see [https://dygraphs.com/](https://dygraphs.com/)

## `Matplotlib` for plots


For documentation and examples see [https://matplotlib.org/](https://matplotlib.org/)

# Plugin Documentation
Create a [Markdown](https://de.wikipedia.org/wiki/Markdown) file in the "Plugins" folder containing the Plugin documentation. Filename must be same as Plugin Class Name + `.md`. Alternatively, you can impelemnt a `self.info()` method in your plugin which returns the documentation as a [Markdown](https://de.wikipedia.org/wiki/Markdown) string.

Automatically, the Plugin pop-up window will have an `Info` Icon in the window title bar and shows your [Markdown](https://de.wikipedia.org/wiki/Markdown) as HTML in a popup window.

# Import Plugin for Testing

# Standalone Plugin-Tester
will be announced later