
import os
import glob
from app import app, socketio
import configparser

if __name__ == '__main__':
    restart = True
    while restart:
        restart = False
        for file in glob.glob(os.path.join('flask_session', '*'), recursive=True):
            os.remove(file)
        file = 'CONFIG.ini'
        config = configparser.ConfigParser()
        if not os.path.exists(file):
            config['WebAudio'] = {"Host": "127.0.0.1", "Port": 8080, "debug": False}
            with open(file, 'w') as configfile:
                config.write(configfile)
        config.read(file)
        host = "127.0.0.1"
        if config.has_option("WebAudio", "Host"):
            host = config["WebAudio"]["Host"]
        else:
            config.set('WebAudio','Host', host)
            with open(file, 'w') as configfile:
                config.write(configfile)
        try:
            socketio.run(app, host = host, ssl_context=('cert.pem', 'key.pem'), debug = (config["WebAudio"]["debug"].lower() == "true" or config["WebAudio"]["debug"] == "1"), port = int(config["WebAudio"]["Port"]))
        except SystemExit as e:
            restart = (config["WebAudio"]["debug"].lower() == "true" or config["WebAudio"]["debug"] == "1")
