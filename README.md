# webAudio-Server

## Virtual Environment

### Initializing Virtual Environment

To create a virtual environment, change to webaudio-Server root directory and run the venv module as a script with the directory path:
```
python3 -m venv webaudio-env
```
This will create the webaudio-env directory if it doesn’t exist, and also create directories inside it containing a copy of the Python interpreter and various supporting files.

Once you’ve created a virtual environment, you may activate it.

On Windows, run:
```
webaudio-env\Scripts\activate.bat
```

On Unix or MacOS, run:
```
source webaudio-env/bin/activate
```

Activating the virtual environment will change your shell’s prompt to show what virtual environment you’re using, and modify the environment so that running python will get you that particular version and installation of Python. For example:
```
$ source ~/envs/webaudio-env/bin/activate
(webaudio-env) $ python
Python 3.5.1 (default, May  6 2016, 10:59:36)
  ...
>>> import sys
>>> sys.path
['', '/usr/local/lib/python35.zip', ...,
'~/envs/webaudio-env/lib/python3.5/site-packages']
>>>
```
To deactivate a virtual environment, type:
```
deactivate
```
into the terminal.

See https://docs.python.org/3/tutorial/venv.html for more information about virtual environments.

### Install Requirements

You can easily install all neccesarry python packages for webaudio-Server by executing
```
pip install -r requirements.txt
```
e.g. in your virtual environment.

## Configuration

You can modfiy important settings in *CONFIG.ini* file and *SMTP_CONFIG.ini* (appears after first start of the server) 

## Usage

Start a webaudio-Server instance by calling *webaudioServer.py* as a script
```
python3 webaudioServer.py
```
If this python script is called the fist time, the underlying database and a necessary admin account will must be created. Therefore, input your data when asked:
```
Initializing Database
=====================
Create Admin Account:
 eMail (used as login): [YOUR EMAILLOGIN]
 Name: [YOUR NAME]
 Password: [YOUR PASSWORD]
 Password-Check: [YOUR PASSWORD]
```
Initializing procedure is finished or database was already created successfully before, a message like 
```
 * Serving Flask app 'app'
 * Debug mode: off
 * Running on https://127.0.0.1:8080
Press CTRL+C to quit
```
should be displayed. The webAudio-Server is running. Call the given link in your web browser (here: https://127.0.0.1:8080)

## Reinitialisation of Server Application
Important: all Settings and stored Data will get lost!
* Stop webAudio Server application
* Delete or Rename File *db.sqlite3* in Server root directory
* Start webAudio Server application

## usefull docs
- https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-i-hello-world
