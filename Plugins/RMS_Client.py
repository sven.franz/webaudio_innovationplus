from app.AudioPlugin import AudioPlugin, PluginType

class RMS_Client(AudioPlugin):
    name = "RMS_Client"
    type = PluginType.persistent
    blockLen = 1024

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.params["color"] = "rgb(255, 255, 255)"
        self.controls = [
                {"type": "select", "label": "Blocklen", "property": "blockLen", "options": [64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384]},
                {"type": "color", "label": "Color", "property": "color", "format": "rgb"}
            ]
