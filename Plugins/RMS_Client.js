function process(data, params, window) {
    let RMS = new Array(data.length);
    var PlotPoints = []
    for (var channel = 0; channel < data.length; channel++) {
        RMS[channel] = 0;
        for (var idx = 0; idx < data[channel].length; idx++)
            RMS[channel] += data[channel][idx] * data[channel][idx];
        PlotPoints.push(new TimePoint(Math.sqrt(RMS[channel] / data[channel].length), channel, params.color));
    }
    return PlotPoints;
}
