function preprocess(params, window) {
    params.spectrogram = [];
    for (var channel = 0; channel < params.channels; channel++) {
        params.spectrogram.push(new Float32Array(params.fftSize / 2 + 1))
        params.N = 0;
    }

    /*
    // Create PopUp Window
    var specPlotWin = popUp("SpectrumPlot");
    // Create Dygraph in PopUp Window
    specPlotWin.graph = new Dygraph(specPlotWin, arrayToDygraph(params.spectrogram));
    */
}

function process(data, params, window) {
    // https://github.com/infusion/Complex.js/
    params.N++;
    var channel = data.length;
    var idx;
    while (channel--) {
        idx = data[channel].length;
        while (idx--) {
            params.spectrogram[channel][idx] = Math.max(1, (params.N - 1)) * (params.spectrogram[channel][idx] + data[channel][idx].pow(2).abs()) / Math.max(1, (params.N - 1));
            var x = 1;
        }
    }
    
    /*
    // Update Plot Graph after each 100. Block
    if (params.N%100 == 0) 
        document.getElementById("PopUpWindow_" + "SpectrumPlot" + "_content").graph.updateOptions({'file': arrayToDygraph(params.spectrogram)});
    */
    return data;
}

function postprocess(data, params, window) {
    var myPopUp = popUp("Spectrum");
    //myPopUp.appendChild(blobToIdmage(data)); // plot image from python postprocess 
    var xTicks = [];
    for (var idx = 0; idx < params["spectrogram"][0].length; idx++)
        xTicks.push(idx / params["spectrogram"][0].length * params.sampleRate / 2);
    var data = arrayToDygraph(params["spectrogram"], xTicks);
    // see https://dygraphs.com/
    var g = new Dygraph(
        myPopUp,
        data,
        {
            ylabel: 'Level / dB',
            xlabel: 'Frequency / Hz',
            legend: 'always',
            title: 'Spectrum',
            animatedZooms: true,
        });
}