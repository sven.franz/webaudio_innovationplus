from app.AudioPlugin import Annotation, AudioPlugin, PluginType
import numpy as np


class Annotator(AudioPlugin):
    name = "Annotator"
    type = PluginType.persistent

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)

    def process(self, data):
        if self.blockTime["start"] >= 5 and self.blockTime["end"] <= 5.028:
                return Annotation("Sprache", "00:00:03", "00:00:05")