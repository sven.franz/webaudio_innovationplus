from app.AudioPlugin import AudioPlugin, Domain, PluginType


class addNoise_Client(AudioPlugin):
    type = PluginType.persistent

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.params["level"] = -10
        self.params["active"] = 1
        self.controls = [
            {"type": "checkbox", "label": "Active", "property": "active"},
            {"type": "range", "label": "Level", "min": -100,
             "max": 50, "step": 1, "property": "level"},
        ]

    def updateParams(self):
        if self.params["level"] == -100:
            self.params["active"] = None
        elif self.params["active"] == None:
            self.params["active"] = False
