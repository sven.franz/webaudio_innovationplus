from random import randint
from app.AudioPlugin import AudioPlugin, PluginType
import numpy as np
#import io
#from matplotlib.figure import Figure

class addNoise_Server(AudioPlugin):
    name = "addNoise Server"
    type = PluginType.persistent
    
    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.params["level"] = -10
        self.params["active"] = 1
        #self.params["img"] = ""
        self.controls = [
                {"type": "checkbox", "label": "Active", "property": "active"},
                {"type": "range", "label": "Level", "min": -100, "max": 0, "step": 1, "property": "level"},
                #{"type": "display", "label": "Level", "width": "200px", "height": "200px", "property": "level"},
                #{"type": "image", "label": "Image", "width": "200px", "height": "200px", "property": "img"},
            ]

    def updateParams(self):
        if self.params["active"] == False:
            self.params["level"] = None
        elif self.params["level"] == None:
            self.params["level"] = 0
        """
        if hasattr(self, "tempData"):
            fig = Figure(figsize=(4, 2))
            ax = fig.add_subplot(111)
            ax.plot(np.transpose(self.tempData))
            self.params["img"] = "static/tmp/test.png"
            fig.savefig("app/" + self.params["img"])
            self.params["img"] += "?" + str(randint(0, 10000000))
        """
            
    def process(self, data):
        if  self.params["active"]:
            data += np.power(10, self.params["level"] / 20) * np.random.normal(0, 1, size = data.shape)
        #self.tempData = data
        return data
