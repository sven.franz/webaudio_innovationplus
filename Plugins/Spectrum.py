from app.AudioPlugin import AudioPlugin, Domain, Figure, PluginType
import numpy as np

class Spectrum(AudioPlugin):
    name = "Spectrum"
    type = PluginType.loopthrough    
    domain = Domain.Frequency

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.controls = [
                {"type": "select", "label": "Blocklen", "property": "blockLen", "options": [64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384]},
            ]

    def postprocess(self):
        self.params["spectrogram"] = np.multiply(20, np.log10(np.abs(np.array(self.bytesToArray(self.params["spectrogram"]))) + np.finfo(np.float32).eps))

        """
        spec = self.bytesToArray(self.params["spectrogram"])
        fig = Figure()
        ax = fig.add_subplot(111)
        ax.plot(np.linspace(0, self.params["sampleRate"] / 2, spec.shape[1]), np.transpose(20*np.abs(np.log10(spec))))
        ax.set_xscale('log')
        ax.set_title("Spectrum")
        ax.set_xlabel("Frequency / Hz")
        ax.set_ylabel("Amplitude / dB")
        return fig.toBytes()
        """
