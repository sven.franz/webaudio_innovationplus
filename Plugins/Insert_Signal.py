import numpy as np

from app.AudioPlugin import AudioPlugin, PluginType

class Insert_Signal(AudioPlugin):
    name = "Insert_Signal"
    type = PluginType.insert

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.params["duration"] = 1
        self.params["frequency"] = None
        self.params["signal"] = "Silence"
        self.params["level"] = None
        self.controls = [
                {"type": "range", "label": "Duration / s", "min": 0, "max": 300, "step": 0.01 , "property": "duration"},
                {"type": "select", "label": "Signal", "property": "signal", "options": ['Silence', 'White Noise', 'Sine', 'Rectangular', 'Triangular', 'Sawtooth']},
                {"type": "range", "label": "Frequency / Hz", "min": 1, "max": self.params["sampleRate"] / 2, "step": 1, "property": "frequency"},
                {"type": "range", "label": "Level / dB FS", "min": -100, "max": 0, "step": 1, "property": "level"},
            ]
        self.samplesDelta = 0
        self.maxComponents = 20
        self.time = np.linspace(0, self.blockLen / self.params["sampleRate"], self.blockLen).astype(np.float32)
        self.time.shape = [1, self.blockLen]
        self.time = np.repeat(self.time, self.params["channels"], 0)

    def updateParams(self):
        if self.params["signal"] == "Silence":
            self.params["level"] = None
        elif self.params["signal"] != "Silence" and self.params["level"] == None:
            self.params["level"] = -100
        if self.params["signal"] in ["Silence", 'White Noise'] :
            self.params["frequency"] = None
        elif not self.params["signal"] in ["Silence", 'White Noise'] and self.params["frequency"] == None:
            self.params["frequency"] = 1

    def process(self, data):
        if self.params["signal"] == "Silence":
            data = np.zeros([self.params["channels"], self.blockLen])
        elif self.params["signal"] == "White Noise":
            data = np.power(10, self.params["level"] / 20.0) * np.random.normal(0, 1, size = [self.params["channels"], self.blockLen])
        elif self.params["signal"] == "Sine":
            data = np.power(10, self.params["level"] / 20.0) * np.sin(2 * np.pi * self.params["frequency"] * (self.time + (self.samplesDelta / self.params["sampleRate"])))
        elif self.params["signal"] == "Rectangular":
            component = 1
            while component < self.maxComponents and (2 * component - 1) * self.params["frequency"] < self.params["sampleRate"] / 2:
                data += 1 / (2 * component - 1) * np.sin((2 * component - 1) * 2.0 * np.pi * self.params["frequency"] * (self.time + (self.samplesDelta / self.params["sampleRate"])))
                component += 1
            data = np.power(10, self.params["level"] / 20.0) * 4.0 / np.pi * data
        elif self.params["signal"] == "Triangular":
            component = 1
            while component < self.maxComponents and (2 * component - 1) * self.params["frequency"] < self.params["sampleRate"] / 2:
                data += np.power(-1, component - 1) / (2 * component - 1)**2 * np.sin((2 * component - 1) * 2 * np.pi * self.params["frequency"] * (self.time + (self.samplesDelta / self.params["sampleRate"])))
                component += 1
            data = np.power(10, self.params["level"] / 20) * (8 / np.pi**2) * data
        elif self.params["signal"] == "Sawtooth":
            component = 1
            while component < self.maxComponents and component * self.params["frequency"] < self.params["sampleRate"] / 2:
                data +=  np.power(-1, component - 1) / component * np.sin(component * 2 * np.pi * self.params["frequency"] * (self.time + (self.samplesDelta / self.params["sampleRate"])))
                component += 1
            data = np.power(10, self.params["level"] / 20) * (-2 / np.pi * data)
        if self.params["frequency"] != None:
            self.samplesDelta += data.shape[1] / 2
        return data
