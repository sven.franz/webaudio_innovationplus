from app.AudioPlugin import AudioPlugin, FrequencyPoint, PluginType
import numpy as np

class Pitch(AudioPlugin):
    name = "Pitch"
    type = PluginType.persistent
    blockLen = 1024

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.params["f_min"] = 50
        self.params["f_max"] = 300
        self.params["color"] = "#ffffff"
        self.controls = [
                {"type": "select", "label": "Blocklen", "property": "blockLen", "options": [128, 512, 1024, 2048, 4096, 8192, 16384]},
                {"type": "color", "label": "Color", "property": "color", "format": "rgb"}
            ]

    def autocorr(self, x):
        result = np.correlate(x, x, mode='full')
        return result[int(np.floor(result.size/2)) : ]

    def process(self, data):
        PlotPoints = []
        for channel in range(data.shape[0]):
            corr = self.autocorr(data[channel, :])
            corr = corr / (np.max(np.abs(corr)) + np.finfo(float).eps)
            corr = corr[int(np.floor(self.params["sampleRate"] / self.params["f_max"])) : int(np.floor(self.params["sampleRate"] / self.params["f_min"]))]
            idx = np.argmax(corr)
            pitchFreq = self.params["sampleRate"] / (idx + self.params["sampleRate"] / self.params["f_max"])
            if corr[idx] > 0.5 and pitchFreq >= self.params["f_min"] and pitchFreq <= self.params["f_max"]:
                PlotPoints.append(FrequencyPoint(pitchFreq, channel, list(map(int, self.params["color"][4:-1].split(",")))))
            else:
                PlotPoints.append(FrequencyPoint(-1, channel, list(map(int, self.params["color"][4:-1].split(",")))))
        return PlotPoints

