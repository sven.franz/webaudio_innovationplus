from app.AudioPlugin import AudioPlugin, PluginType, TimePoint
import numpy as np

class RMS(AudioPlugin):
    name = "RMS"
    type = PluginType.persistent

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.params["color"] = "rgb(255, 255, 255)"
        self.controls = [
                {"type": "select", "label": "Blocklen", "property": "blockLen", "options": [64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384]},
                {"type": "color", "label": "Color", "property": "color", "format": "rgb"}
            ]

    def process(self, data):
        PlotPoints = []
        for channel in range(data.shape[0]):
            PlotPoints.append(TimePoint(np.sqrt(np.sum(np.square(data[channel, :])) / data.shape[1]), channel, list(map(int, self.params["color"][4:-1].split(",")))))
        return PlotPoints